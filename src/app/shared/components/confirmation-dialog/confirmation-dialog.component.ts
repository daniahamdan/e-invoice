import { Component, EventEmitter, Output } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-confirmation-dialog',
  templateUrl: './confirmation-dialog.component.html',
  styleUrls: ['./confirmation-dialog.component.css']
})
export class ConfirmationDialogComponent {

  @Output() deleteConfirmed = new EventEmitter();

  constructor(public bsModalRef: BsModalRef) { }

  closeModal(): void {
    this.bsModalRef.hide();
  }

  deleteItem(): void {
    this.deleteConfirmed.emit();
    this.closeModal();
  }

}
