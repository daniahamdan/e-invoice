import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TempInfoComponent } from './temp-info.component';

describe('TempInfoComponent', () => {
  let component: TempInfoComponent;
  let fixture: ComponentFixture<TempInfoComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [TempInfoComponent]
    });
    fixture = TestBed.createComponent(TempInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
