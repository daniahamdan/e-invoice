import { Pipe, PipeTransform, NgModule } from '@angular/core';

@Pipe({
  name: 'replace',
})
export class ReplacePipe implements PipeTransform {
  transform(input: any, pattern: any, replacement: any): any {
    if (!isString(input) || isUndefined(pattern) || isUndefined(replacement)) {
      return input;
    }

    return input.replace(pattern, replacement);
  }
}

@NgModule({
  declarations: [ReplacePipe],
  exports: [ReplacePipe],
})
export class NgReplacePipeModule { }
export function isString(value: any): value is string {
  return typeof value === 'string';
}
export function isUndefined(value: any): value is undefined {
  return typeof value === 'undefined';
}
