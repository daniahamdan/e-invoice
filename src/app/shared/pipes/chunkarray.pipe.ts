import { NgModule, Pipe, PipeTransform } from '@angular/core';


@Pipe({
  name: 'chunkarray'
})
export class ChunkArrayPipe implements PipeTransform {
  // result1 = typeof Array === 'string' ? Array.slice(2) : '';
 
   transform(array: any[], start: number, end: number): any[] {
    if (!Array.isArray(array)) {
      // Optionally, you can throw an error or handle this case as you see fit
     // console.error('ChunkArrayPipe: input is not an array');
      return [];
    }
   return array.slice(start, end + 1);
 
  }
























}

@NgModule({
    declarations: [ChunkArrayPipe],
    exports: [ChunkArrayPipe],
  })
  export class NgChunkArrayPipeModule { 
  
  }

