export interface PageResponse {
  PageSize: number,
  PageNumber: number,
}
