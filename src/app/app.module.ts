import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './app.component';
import { AuthComponent } from './components/auth/auth.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgbDatepickerModule, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AppRoutingModule } from './modules/app-routing.module';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';
import { HTTP_INTERCEPTORS, HttpClient, HttpClientModule } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { HomeComponent } from './components/home/home.component';
import { AuthInterceptorService } from './components/auth/auth-interceptor.service';
import { ErrorInterceptor } from './services/error/error.interceptor';
import { DatePipe, JsonPipe } from '@angular/common';
import { AppTranslateService } from './services/translate-service/translate.service';
import { BootstrapModule } from './modules/bootstrap.module';
import { MerchantComponent } from './components/merchant/merchant.component';
import { MerchantListComponent } from './components/merchant/components/merchant-list/merchant-list.component';
import { MerchantAddUpdateComponent } from './components/merchant/components/merchant-add-update/merchant-add-update.component';
import { MerchantNavMenuComponent } from './components/merchant/components/merchant-nav-menu/merchant-nav-menu.component';
import { MerchantViewComponent } from './components/merchant/components/dialogs/merchant-view/merchant-view.component';
import { MerchantSelectComponent } from './components/merchant/components/merchant-select/merchant-select.component';
import { LoaderComponent } from './shared/components/loader/loader.component';
import { PageNotFoundComponent } from './shared/components/page-not-found/page-not-found.component';
import { LoadingInterceptor } from './shared/components/loader/loading.interceptor';
import { ConfirmationDialogComponent } from './shared/components/confirmation-dialog/confirmation-dialog.component';
import { TempInfoComponent } from './shared/components/temp-info/temp-info.component';
import { NavMenuComponent } from './components/nav-menu/nav-menu.component';
import { UserProfileDialogComponent } from './components/user/components/dialogs/user-profile-dialog/user-profile-dialog.component';
import { BranchListComponent } from './components/branch/components/branch-list/branch-list.component';
import { BranchviewComponent } from './components/branch/components/dialogs/branchview/branchview.component';
import { BranchAddUpdateComponent } from './components/branch/components/branch-add-update/branch-add-update.component';
import { BranchComponent } from './components/branch/components/branch/branch.component';
import { NavBranchComponent } from './components/branch/components/nav-branch/nav-branch.component';
import { Terminal } from './components/terminal/components/models/terminal.model';
import { TerminalComponent } from './components/terminal/components/terminal/terminal.component';
import { TerminalAddUpdateComponent } from './components/terminal/components/terminal-add-update/terminal-add-update.component';
import { TerminalListComponent } from './components/terminal/components/terminal-list/terminal-list.component';
import { TerminalViewComponent } from './components/terminal/components/terminal-view/terminal-view.component';
import { NavTerminalComponent } from './components/terminal/components/nav-terminal/nav-terminal.component';
import { AccountSelectComponent } from './components/account/components/account-select/account-select.component';
import { AccountListComponent } from './components/account/components/account-list/account-list.component';
import { AccountAddUpdateComponent } from './components/account/components/account-add-update/account-add-update.component';
import { AccountNavMenuComponent } from './components/account/components/account-nav-menu/account-nav-menu.component';
import { AccountViewComponent } from './components/account/components/dialogs/account-view/account-view.component';
import { AccountComponent } from './components/account/account.component';
import { ChangeAccountDialogComponent } from './components/account/components/dialogs/change-account-dialog/change-account-dialog.component';
import { NgChunkPipeModule } from './shared/pipes/chunk.pipe';
import { NgReplacePipeModule } from './shared/pipes/replace.pipe';
import { PlanComponent } from './components/plan/components/plan/plan.component';
import { PlanListComponent } from './components/plan/components/plan-list/plan-list.component';
import { PlanAddUpdateComponent } from './components/plan/components/plan-add-update/plan-add-update.component';
import { PlanViewComponent } from './components/plan/components/plan-view/plan-view.component';
import { NavPlanComponent } from './components/plan/components/nav-plan/nav-plan.component';
import { InvoiceComponent } from './components/invoice/invoice.component';
import { InvoiceNavMenuComponent } from './components/invoice/components/invoice-nav-menu/invoice-nav-menu.component';
import { DatePickerComponent } from './components/date-time-picker/components/date-picker/date-picker.component';
import { DateTimePickerComponent } from './components/date-time-picker/components/datetime-range-picker/date-time-picker.component';
import { NavUserComponent } from './components/users/users/nav-user/nav-user.component';
import { UserAddupdateComponent } from './components/users/users/user-addupdate/user-addupdate.component';
import { UserViewComponent } from './components/users/users/user-view/user-view.component';
import { UserComponent } from './components/users/users/user/user.component';
import { UserListComponent } from './components/users/users/user-list/user-list.component';

import { NavEchartsComponent } from './echarts/nav-echarts/nav-echarts.component';
import * as echarts from 'echarts';
import { NgxEchartsModule } from 'ngx-echarts';
import { NgChunkArrayPipeModule } from './shared/pipes/chunkarray.pipe';

import { MonitorComponent } from './components/monitor/monitor/monitor.component';
import { MonitorlistComponent } from './components/monitor/monitorlist/monitorlist.component';
import { NavMonitorComponent } from './components/monitor/nav-monitor/nav-monitor.component';
import { MonitorviewComponent } from './components/monitor/monitorview/monitorview.component';
import { InvoiceMessagesComponent } from './components/invoice/components/dialogs/invoice-messages/invoice-messages.component';
import { EchartsComponent } from './echarts/echarts.component';
import { InvoiceQrcodeComponent } from './components/invoice/components/invoice-qrcode/invoice-qrcode.component';
import { ClipboardModule } from 'ngx-clipboard';
import { PointRecordsComponent } from './components/point-records/point-records.component';
import { PointRecordsListComponent } from './components/point-records/components/point-records-list/point-records-list.component';
import { PointRecordsAddComponent } from './components/point-records/components/point-records-add/point-records-add.component';
import { PointRecordsNavMenuComponent } from './components/point-records/components/point-records-nav-menu/point-records-nav-menu.component';
import { PointRecordsViewComponent } from './components/point-records/dialogs/point-records-view/point-records-view.component';
import { ViewChangepasswordComponent } from './components/changepassword/view-changepassword/view-changepassword.component';
import { BulkuploadListComponent } from './components/bulkupload-list/bulkupload-list.component';
import { NavBulkuploadComponent } from './components/bulkupload-list/nav-bulkupload/nav-bulkupload.component';
import { ExportedInvoicesComponent } from './components/exported-invoices/exported-invoices.component';
import { ExportedInvoicesNavMenuComponent } from './components/exported-invoices/exported-invoices-nav-menu/exported-invoices-nav-menu.component';
import { Directionality, BidiModule } from '@angular/cdk/bidi';

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
  declarations: [
    AppComponent,
    AuthComponent,
    HomeComponent,
    LoaderComponent,
    PageNotFoundComponent,
    MerchantComponent,
    MerchantListComponent,
    MerchantAddUpdateComponent,
    MerchantNavMenuComponent,
    MerchantViewComponent,
    MerchantSelectComponent,
    ConfirmationDialogComponent,
    TempInfoComponent,
    NavMenuComponent,
    UserProfileDialogComponent,
    BranchListComponent,
    BranchviewComponent,
    BranchAddUpdateComponent,
    BranchComponent,
    NavBranchComponent,
    TerminalComponent,
    TerminalAddUpdateComponent,
    TerminalListComponent,
    TerminalViewComponent,
    NavTerminalComponent,
    AccountSelectComponent,
    AccountListComponent,
    AccountAddUpdateComponent,
    AccountNavMenuComponent,
    AccountViewComponent,
    AccountComponent,
    ChangeAccountDialogComponent,
    PlanComponent,
    PlanListComponent,
    PlanAddUpdateComponent,
    PlanViewComponent,
    NavPlanComponent,
    InvoiceComponent,
    InvoiceNavMenuComponent,
    DatePickerComponent,
    DateTimePickerComponent,
    NavPlanComponent,
    UserComponent,
    UserAddupdateComponent,
    UserListComponent,
    UserViewComponent,
    NavUserComponent,
    EchartsComponent,
    NavEchartsComponent,
    MonitorComponent,
    MonitorlistComponent,
    NavMonitorComponent,
    MonitorviewComponent,
    InvoiceMessagesComponent,
    InvoiceQrcodeComponent,
    PointRecordsComponent,
    PointRecordsListComponent,
    PointRecordsAddComponent,
    PointRecordsNavMenuComponent,
    PointRecordsViewComponent,
    ViewChangepasswordComponent,
    BulkuploadListComponent,
    NavBulkuploadComponent,
    ExportedInvoicesComponent,
    ExportedInvoicesNavMenuComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    BidiModule,
    AppRoutingModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    NgxEchartsModule,
    NgxEchartsModule.forRoot({ echarts }),
    FontAwesomeModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    NgSelectModule,
    BootstrapModule,
    NgbModule,
    NgbDatepickerModule,
    JsonPipe,
    NgChunkPipeModule,
    NgChunkArrayPipeModule,
    
    NgReplacePipeModule,
    ClipboardModule,
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptorService,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: LoadingInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ErrorInterceptor,
      multi: true
    },
    { provide: Directionality, useValue: { value: 'rtl' } },
    DatePipe,
    AppTranslateService
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ],
  bootstrap: [AppComponent]
})
export class AppModule {


}
