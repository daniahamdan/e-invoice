import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export abstract class HttpService {
  private readonly apiUrl: string = environment.apiUrl;
  private readonly headers = { 'content-type': 'application/json' };

  constructor(private httpClient: HttpClient) { }

  public post<T>(url: string, data: any, header?: any): Observable<any> {
    return this.httpClient.post<any>(`${this.apiUrl}${url}`, data, {
      headers: header == null ? this.headers : header,
    });
  }

  public getbyId<T>(url: string, id: any, header?: any, responseType?: any) {
    return this.httpClient.get<any>(`${this.apiUrl}${url}`, {
      params: { id },
      headers: header,
      responseType: responseType,
    });

  }
  public getExport<T>(url: string) {
    return this.httpClient.get<any>(`${this.apiUrl}${url}`);
  }


  public get<T>(url: string, request?: any, headers?: any, responseType?: any) {
    return this.httpClient.get<any>(`${this.apiUrl}${url}`, {
      params: request,
      headers: headers,
      responseType: responseType,
    });
  }
  public update<T>(url: string, id: any, data: any, headers?: any) {
    return this.httpClient.put<any>(`${this.apiUrl}${url}`, data, {
      params: { id },
      headers: headers,
    });
  }
  public put<T>(url: string, data: any) {
    return this.httpClient.put<any>(`${this.apiUrl}${url}`, data, {
      headers: this.headers,
    });
  }
  public delete<T>(url: string, params: any, headers?: any) {
    return this.httpClient.delete<any>(`${this.apiUrl}${url}`, {
      params,
      headers: headers,
    });
  }
}
