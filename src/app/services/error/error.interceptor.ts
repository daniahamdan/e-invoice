import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpErrorResponse
} from '@angular/common/http';
import { catchError, Observable, retry, throwError } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import { ToastService } from '../toast-service/toast.service';

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {

  constructor(private toastService: ToastService,
    private translate: TranslateService) { }

  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    return next.handle(request)
      .pipe(
        retry(0),
        catchError((error: HttpErrorResponse) => {
          let errorMessage = error?.message;
          if (error.status === 400) {
            errorMessage = error?.error?.title;
            this.toastService.error(errorMessage);
          }
          else if (error.status === 401) {
            return throwError(errorMessage);
          }
          else if (error.status === 404) {
            const notFoundMessage = this.translate
              .instant('general.notFoundMessage');
            this.toastService.error(notFoundMessage);
          }
          else {
            let message = error?.error?.message;
            if (!!message) {
              this.toastService.error(message);
            }
            else {
              const errorMessage = this.translate
                .instant('general.generalError');
              this.toastService.error(errorMessage);
            }
          }
          console.log(errorMessage);
          return throwError(errorMessage);
        })
      )
  }
}
