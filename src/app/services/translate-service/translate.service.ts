import { DOCUMENT } from '@angular/common';
import { Inject, Injectable, Renderer2, RendererFactory2 } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { BehaviorSubject } from 'rxjs';
import { environment } from 'src/environments/environment';
@Injectable({
  providedIn: 'root'
})
export class AppTranslateService {
  private currentLang = new BehaviorSubject(environment.defaultLang);

  currentLangObservable = this.currentLang.asObservable();

  private renderer: Renderer2;
  constructor(private translateService: TranslateService,
    @Inject(DOCUMENT) private document: Document,
    private rendererFactory: RendererFactory2) {
    this.renderer = rendererFactory.createRenderer(null, null);
    const defaultLang = environment.defaultLang;
    translateService.defaultLang = defaultLang;
    translateService.use(defaultLang);
  }

  changeLanguage(lang: string) {
    this.currentLang.next(lang);
    this.translateService.defaultLang = lang;
    this.translateService.use(lang);
    localStorage.setItem('currentLang', lang);
    this.changeCss(lang);
  }

  private changeCss(lang: string) {
    let htmlTag = this.document.getElementsByTagName('html')[0] as HTMLHtmlElement;
    const link = this.renderer.createElement('link');
    link.setAttribute('rel', 'stylesheet');
    link.setAttribute('type', 'text/css');
    if (lang === 'ar-SA') {
      var linkTodelete = this.document.querySelector('link[href="assets/css/styles-en.css"]');
      if (!!linkTodelete) linkTodelete.remove();
      htmlTag.dir = 'rtl';
      htmlTag.lang = 'ar';
      link.setAttribute('href', 'assets/css/styles-ar.css');
      this.renderer.appendChild(document.head, link);
    }
    else {
      var linkTodelete = this.document.querySelector('link[href="assets/css/styles-ar.css"]');
      if (!!linkTodelete) linkTodelete.remove();

      htmlTag.dir = 'ltr';
      htmlTag.lang = 'en';
      link.setAttribute('href', 'assets/css/styles-en.css');
      this.renderer.appendChild(document.head, link);
    }
  }

}
