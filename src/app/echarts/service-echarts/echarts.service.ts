import { Injectable } from '@angular/core';
import { tap } from 'rxjs';
import { HttpService } from 'src/app/services/http.service';

@Injectable({
  providedIn: 'root'
})
export class EchartsService {

  constructor(private httpService: HttpService) { 

  }

  getAccountStatistics(accountId: string|null, data?: any) {
    const id = (accountId == null ? '' : accountId)
  
      const headers = { 'AccountId': id }
      return this.httpService.get("Dashboard/AccountStatistics", data, headers).pipe(
        tap((response: any) => { 
        }))
    }



    getInvoiceAmountPerType(accountId: string|null, data?: any) {
      const id = (accountId == null ? '' : accountId)
    
        const headers = { 'AccountId': id }
        return this.httpService.get("Dashboard/InvoiceAmountPerType", data, headers).pipe(
          tap((response: any) => { 
          }))
      }

      
      getInvoiceAmountPerStatus(accountId: string|null, data?: any) {
      const id = (accountId == null ? '' : accountId)
    
        const headers = { 'AccountId': id }
        return this.httpService.get("Dashboard/InvoiceAmountPerStatus", data, headers).pipe(
          tap((response: any) => { 
          }))
      }


      getTopBranchesInvoices(accountId: string|null, data?: any) {
        const id = (accountId == null ? '' : accountId)
      
          const headers = { 'AccountId': id }
          return this.httpService.get("Dashboard/TopBranchesInvoices", data, headers).pipe(
            tap((response: any) => { 
            }))
        }


        getHourlyInvoices(accountId: string|null, data?: any) {
          const id = (accountId == null ? '' : accountId)
            const headers = { 'AccountId': id }
            return this.httpService.get("Dashboard/HourlyInvoices", data, headers).pipe(
              tap((response: any) => { 
              }))
          }
        
      
    
  


  
}
