

export class Echarts {
  accountId?: string;
  merchantId?: string;
  terminalId?: string;
  branchId?: string;
  FromDate?: string;
  ToDate?: string;
  branchCount!: number;
  terminalCount!: number;
  invoiceCount!: number;
  totalSubAmount!: number;
  totalVatAmount!: number;
  totalAmount!: number;
  type!: InvoiceType;
  hour!: number;
  amount!: number;
  branchName!: string;
  name!: string;
  value!: number;
  status!: StatusData;
}



 export interface EchartsData{

  name: string;
   value: number;
   count:number
}

export interface EcharData{

  count: number;
   value: number;

}


export interface EchartsFilter {
  
  AccountId: string;
  MerchantId: string;
  TerminalId?: string;
  branchId?: string;
  FromDate?: string;
  ToDate?: string;
}


export interface InvoiceFilter {
  
  accountId?: string;
  terminalId?: string;
  branchId?: string;
  referencedId?: string;
  invoiceNumber?: number;
  sendDateFrom?: string;
  sendDateTo?: string;
  invoiceType?: InvoiceType;
  pageSize?: number;
  pageNumber?: number;
}

export interface EchartFilter{
    accountId?: string;
    terminalId?: string;
    branchId?: string;
    referencedId?: string;
    invoiceNumber?: number;
    sendDateFrom?: string;
    sendDateTo?: string;
    invoiceType?: InvoiceType;
    
  }

  export enum InvoiceType {
    StandardInvoice =1,
  
    StandardDebitNote = 2,
  
    StandardCreditNote = 3,
  
    SimplifiedInvoice = 4,
  
    SimplifiedCreditNote = 5,
  
    SimplifiedDebitNote = 6,
  }

  export enum StatusData {
 Pass,
 Error,
 Warning
  }