import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NavEchartsComponent } from './nav-echarts.component';

describe('NavEchartsComponent', () => {
  let component: NavEchartsComponent;
  let fixture: ComponentFixture<NavEchartsComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [NavEchartsComponent]
    });
    fixture = TestBed.createComponent(NavEchartsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
