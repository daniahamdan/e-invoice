import { DatePipe } from '@angular/common';
import { Component, EventEmitter, Output } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { ActivatedRoute, Params } from '@angular/router';
import { faCalendarDays } from '@fortawesome/free-solid-svg-icons';
import { Observable, BehaviorSubject, of } from 'rxjs';
import { BranchService } from 'src/app/components/branch/components/branch.service';
import { InvoiceType} from 'src/app/components/invoice/models/invoice.model';
import { TerminalService } from 'src/app/components/terminal/components/services-terminal/terminal.service';
import { IdName } from 'src/app/shared/interfaces/idName.interface';
import {  EchartsFilter } from '../models/echarts.model';

@Component({
  selector: 'app-nav-echarts',
  templateUrl: './nav-echarts.component.html',
  styleUrls: ['./nav-echarts.component.css']
})
export class NavEchartsComponent {
  calendarIcon = faCalendarDays;

  branches!: Observable<IdName[]>;
  terminals!: Observable<IdName[]>;
  filterForm!: FormGroup;

  accountId!: string;
  merchantId!: string;

  invoiceTypeKeys: string[] = [];
  invoiceType = InvoiceType;

  fromDateString: string | null = null;
  toDateString: string | null = null;
  isCleared = new BehaviorSubject(false);
  $isCleared = this.isCleared.asObservable();


  @Output("filterEcharts") filterEcharts: EventEmitter<any> = new EventEmitter();

  constructor(private datePipe: DatePipe,
    private terminalService: TerminalService,
    private branchService: BranchService,
    private route: ActivatedRoute) {
     
  }
  ngAfterViewInit(): void {
    this.route.params.subscribe(
      (params: Params) => {
        if (this.accountId != params['accountId']) {
          this.accountId = params['accountId'];
        }
        if (this.merchantId != params['merchantId']) {
          this.merchantId = params['merchantId'];
          this.clearForm();
          this.getBranches();
          this.getTerminals();
          this.isCleared.next(true);
        }
      }
    );
  }

  ngOnInit(): void {
    this.accountId = this.route.snapshot.params['accountId'];
    this.merchantId = this.route.snapshot.params['merchantId'];

    this.filterForm = new FormGroup({
      'selectedBranch': new FormControl(null),
      'TerminalId': new FormControl(null),
    });
    this.getBranches();
    this.getTerminals();
  }

  getBranches() {
    this.branchService.getBranchList(this.accountId, { PageSize: 1000, PageNumber: 1, MerchantId: this.merchantId })
      .subscribe(response => {
        if (!!response) {
          this.branches = of(response.data.map((item: any) => ({
            id: item.nameEn,
            name: item.id,
          }))).pipe();
        }
      });
  }
  getTerminals() {
    this.terminalService.getTerminalList(this.accountId, { PageSize: 1000, PageNumber: 1, MerchantId: this.merchantId })
      .subscribe(response => {
        if (!!response) {
          this.terminals = of(response.data.map((item: any) => ({
            id: item.nameEn,
            name: item.id,
          }))).pipe();
        }
      });
  }

  onSave() {
  
    const TerminalId = this.filterForm.get('TerminalId')?.value;
    const branchId = this.filterForm.get('selectedBranch')?.value;
    
    const data: EchartsFilter = { AccountId: this.accountId ,MerchantId:this.merchantId};
    if (!!branchId) {
      data.branchId = branchId;
    }
    if (!!TerminalId) {
      data.TerminalId = TerminalId;
    }

    if (!!this.fromDateString) {
      data.FromDate = this.fromDateString;
    }
    if (!!this.toDateString) {
      data.ToDate = this.toDateString;
    }
   
    this.filterEcharts.emit(data);
  }

  clearForm() {
    this.filterForm?.controls['TerminalId'].setValue(null);
    this.filterForm?.controls['selectedBranch'].setValue(null);
    this.fromDateString = null;
    this.toDateString = null;
  }
  emitClearFilter() {
    this.clearForm();
    this.isCleared.next(true);
    this.filterEcharts.emit({ accountId: this.accountId, merchantId: this.merchantId });
  }
  onDateChange($event: any) {
    if (!!$event)
      this.setRangeDate($event)
  }
  setRangeDate(date: string) {
    if (!!date) {
      const range = date.split(';');
      this.fromDateString = range[0];
      this.toDateString = range[1];
      this.onSave();
    }
  }

}
