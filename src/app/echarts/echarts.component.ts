import { Component } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { faEllipsisV, faDownload, faFilePdf, faQrcode, faFile } from '@fortawesome/free-solid-svg-icons';

import { InvoiceService } from '../components/invoice/invoice.service';
import { Invoice, InvoiceType } from '../components/invoice/models/invoice.model';
import { EcharData, EchartFilter, Echarts, EchartsData, EchartsFilter, StatusData } from './models/echarts.model';
import { EChartsOption, number } from 'echarts';
import * as echartsService from './service-echarts/echarts.service';
import { TranslateService } from '@ngx-translate/core';




@Component({
  selector: 'app-echarts',
  templateUrl: './echarts.component.html',
  styleUrls: ['./echarts.component.css']
})
export class EchartsComponent {
  actionIcon = faEllipsisV;
  downloadIcon = faDownload;
  pdfIcon = faFilePdf;
  qrIcon = faQrcode;
  xmlIcon = faFile;
  invoices: Invoice[] = [];
  isUpdate: boolean = false;
  itemCount: number = 0;
  addsuccessMessage!: string;
  currentFilter: EchartFilter | null = null;
  paginatorPageSize: number = 10;
  paginatorPageNumber: number = 1;
  accountId!: string;
  merchantId!: string;
  echarts: Echarts[] = [];
  dataItem: any = {};
  datatype: any = {};
  datastatus: any = {}
  EChartsOption: any;

  EChartOption: any;
  ECharOption: any;
  value: any;
  EChaOption: any;
   result: any;
  branchCount: any;
  chart: any = [];
  terminalCount: any;
  totalAmount: any;
  DATA_COUNT = 5;
  NUMBER_CFG = { count: this.DATA_COUNT, min: 0, max: 100 };
  invoiceCount: any;
  currentData: any;
  isLoading!: boolean;
  error!: string;
  data: any
  type!: InvoiceType;
  invoiceTypeKeys: string[];
  invo!: string;
  respo!: string;



  chartname!: string;




  // echartsData: [{name: string, value: number}]  ; 



  //  jsonResponse = '[{id": 1, "name": "Item 1"}, {"id": 2, "name": "Item 2"}]';
  // = angular.fromJson(this.jsonResponse);

  // Parse the JSON array string into a JSONArray
  //JSONArray jsonArray = new this.JSONArray(this.jsonArrayString);

  // Enum: typeof InvoiceType  = InvoiceType ;



  //public myArray = Object.values(InvoiceType).map(item => String(item));

  Pass!: StatusData;
  colors: any;


 




  constructor(
    private invoiceService: InvoiceService,
    public router: Router,
    public route: ActivatedRoute,

    private echartsService: echartsService.EchartsService,

    public translate: TranslateService,
   

  ) {

    this.invoiceTypeKeys = Object.keys(InvoiceType)
    // this.invo=Object.keys(InvoiceType)[Object.values(InvoiceType).indexOf(this.type)]
    //console.log(Object.keys(InvoiceType))
    // Object.values(this.invoiceTypeKeys).map(InvoiceType=> String(InvoiceType));
    Object.keys(InvoiceType)[Object.values(InvoiceType).indexOf(this.type)]

    
  }



  ngOnInit() {

    this.accountId = this.route.snapshot.params['accountId'];
    this.merchantId = this.route.snapshot.params['merchantId'];
   


  }


  echartsOption: EChartsOption = {

    tooltip: {
      trigger: 'item'
    },
    //  legend: {
    //   top: '5%',
    // left: 'center'
    // },
    series: [
      {
        name: 'Fuel type',
        type: 'pie',
        radius: ['40%', '70%'],
        avoidLabelOverlap: true,
        label: {
          show: false,
          position: 'center'
        },
        emphasis: {
          label: {
            show: false,
            fontSize: 40,
            fontWeight: 'bold'
          }
        },

        labelLine: {
          show: false
        },
        data: [
          { value: 665, name: 'Diesal:665 , Total transaction:22' },
          { value: 509.26, name: 'Gaoline91: 509.26 , Total transaction:20' },
          { value: 755.77, name: 'Gaoline95: 755.77 , Total transaction:32 ' },

        ]
      }
    ]

  };
 
  


  drow(echartdata: any) {
   
    this.EChartsOption =

    {
    

      tooltip: {


        trigger: 'item',

        formatter: function (params : any) {
        
          return ` 
        
             ${params.data.name}<br />
           ${params.data.value} (${params.percent}%)<br />
                  count:  ${params.data.count} 
                  `;
        }
      },

      series: [
        {
          name: '',
          type: 'pie',
          radius: ['30%', '60%'],
          avoidLabelOverlap: true,

          label: {
            show: true,
            // position: 'center',
            formatter(param: { name: string; percent: number; value: number }) {
              // correct the percentage
              return param.name+":"+ param.value + '(' + param.percent + '%)';
            }

          },

          emphasis: {
            label: {
              show: true,
              fontSize: 20,
              fontWeight: 'bold',

            }
          },

          labelLine: {
            show: true
          },
          data: echartdata
        }
      ]

    };




  }


  drows(echartdata: any) {
    
    this.EChaOption =

    {

      tooltip: {
        trigger: 'item',
        formatter: function (params: {
          myArray: any; seriesName: any; name: any; data: { count: any; name: any; value: any; }; percent: any;
        }) {
          return `
          ${params.data.name}: ${params.data.value} (${params.percent}%)<br />
                  count: ${params.data.count}
                  `;
        },

        // itemStyle: { 
        //  color: (seriesIndex: { name: string | number; }) => this.colors[seriesIndex.name] 
        // color: (seriesIndex:StatusData) =>  this.getColor(seriesIndex)
        // color:this.getColor
        // },
      },

      series: [
        {

          name: '',
          type: 'pie',
          radius: ['30%', '60%'],
          avoidLabelOverlap: true,
          color: ['red', 'green', 'yellow'],
          label: {
            show: true,
            //position: 'center',
            formatter(param: { name: string; percent: number; value: number }) {
              // correct the percentage
              return param.name+":"+ param.value + ' (' + param.percent + '%)';
            }
          },
          emphasis: {
            label: {
              show: true,
              fontSize: 20,
              fontWeight: 'bold'
            }
          },
          labelLine: {
            show: true
          },
          data: echartdata
        }
      ]

    };




  }



  create(chartdata: any) {


    for (let chart of chartdata) {

     // console.log(chart);
      this.chartname = chart.name;
    }

    this.EChartOption =
    {

      tooltip: {

        trigger: 'item',
        formatter: function (params: { seriesName: any; name: any; data: { count: any; name: any; value: any; }; percent: any; }) {

          return `${Object.keys(InvoiceType)[Object.values(InvoiceType).indexOf(params.data.count)]}<br />
          ${params.data.name}: ${params.data.value}
          `;
        }



      },



      xAxis: {


        type: '',
        data: [this.chartname]

      },
      yAxis: {
        type: ''
      },
      series: [
        {
          data: chartdata,
          type: 'bar'
        }

      ]


    };




  }



  put(chardata: any) {

    this.ECharOption =
    {
      tooltip: {
        trigger: 'item',
        formatter: function (params: { seriesName: any; name: any; data: { count: any; name: any; value: any; }; percent: any; }) {

          return `${params.data.count}<br />
                 ${params.data.value}
          `;
        }
      },

      xAxis: {
        type: 'category',
        data: ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '15', '16', '17', '18', '19', '20', '21', '22', '23']

      },
      yAxis: {
        type: 'value'
      },
      series: [
        {
          data: chardata,
          type: 'bar'
        }

      ]


    };




  }











  ngAfterViewInit() {
    this.route.params.subscribe((params: Params) => {
      if (this.accountId != params['accountId']) {
        this.accountId = params['accountId'];
      }
      if (this.merchantId != params['merchantId']) {
        this.merchantId = params['merchantId'];
      }

    this.getAccountStatistics()
    this.getInvoiceAmountPerType();
    this.getTopBranchesInvoices();
    this.getHourlyInvoices();
    this.getInvoiceAmountPerStatus();

    });
  }





  getAccountStatistics(echartsData?: any) {
    const data = this.setListRequestData(echartsData)
    this.echartsService
      .getAccountStatistics(this.accountId, data)
      .subscribe((echartsData) => {

        this.echarts = echartsData.data;
        this.itemCount = echartsData.totalCount;
        this.dataItem = echartsData;
      });


  }






  getHourlyInvoices(echartsData?: any) {

    const data = this.setListRequestData(echartsData)
  

    this.echartsService
      .getHourlyInvoices(this.accountId, data)
      .subscribe((response: any[]) => {
        const chardata: EcharData[] = []

        response.map(item => {
          chardata.push({
            count: item.hour,
            value: item.amount,

          });
        })

        this.put(chardata);
      });



  }
;

  getInvoiceAmountPerType(echartsData?: any) {

    const data = this.setListRequestData(echartsData)
    this.echartsService
      .getInvoiceAmountPerType(this.accountId, data)
      .subscribe((response: any[]) => {
        const echartdata: EchartsData[] = []
      
        response.map(item => {
        
          var dania = this.translate.instant(
            Object.keys(InvoiceType)[Object.values(InvoiceType).indexOf(item.type)].toLowerCase(),
          )
          
          echartdata.push({
            name: this.translate.instant(
              "invoice." + Object.keys(InvoiceType)[Object.values(InvoiceType).indexOf(item.type)].toLowerCase(),
            ),
            value: item.totalAmount ,
            count: item.invoiceCount ,
          });
          this.datatype = echartdata
        })
        this.drow(echartdata);
      });
  }


  getInvoiceAmountPerStatus(statusData?: any) {

    const data = this.setListRequestData(statusData)
    this.echartsService
      .getInvoiceAmountPerStatus(this.accountId, data)
      .subscribe((response: any[]) => {
        const echartdata: EchartsData[] = []
     
        response.map(item => {
        //  var curr = formatNumber( item.totalAmount,this.locale, 
         //   '2.0-1'); 
          echartdata.push({
            name: item.status,
            value:  item.totalAmount ,
            
            count: item.invoiceCount,
          });
        
          this.datastatus = echartdata
        })

        this.drows(echartdata);

        //this.datastatus=echartdata;
      });

  }

  //public getColor(status:StatusData ): string{
  // debugger
  // return status ==  pass? "red" : "blue";
  // }




  getTopBranchesInvoices(echartsData?: any) {
    const data = this.setListRequestData(echartsData)
 
    this.echartsService
      .getTopBranchesInvoices(this.accountId, data)
      .subscribe((response: any[]) => {
        const chartdata: EchartsData[] = []

        response.map(item => {

          chartdata.push({
            name: item.branchName,
            count: item.type,
            value: item.totalAmount,
          });


          this.create(chartdata);

        });


      })
  }


  setListRequestData(echartsData?: EchartsFilter | null) {
    if (!!echartsData) {
      echartsData.AccountId = this.accountId;
      echartsData.MerchantId = this.merchantId;

      return echartsData;
    } else {
      return {
        MerchantId: this.merchantId,
        AccountId: this.accountId

      };
    }
  }



  applyEchartFilter(filterData?: any) {
    this.paginatorPageNumber = 1;
    this.paginatorPageSize = 10;
    this.currentFilter = filterData;
    this.getAccountStatistics(filterData);
    this.getInvoiceAmountPerType(filterData);
    this.getTopBranchesInvoices(filterData);
    this.getHourlyInvoices(filterData);
    this.getInvoiceAmountPerStatus(filterData);
  }




  preventCellClick($event: any) {
    $event.stopPropagation();
  }

}
