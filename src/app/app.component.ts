import { Component, ElementRef, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { environment } from 'src/environments/environment';
import { AppTranslateService } from './services/translate-service/translate.service';
import { AuthService } from './components/auth/auth.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],

})
export class AppComponent implements OnInit{
  
  title = 'E-Invoice';



  languages: string[] = ['en-US', 'ar-SA'];
  defaultLang: string = environment.defaultLang;
  

  constructor(private translate: TranslateService,
    private appTranslateService: AppTranslateService,
    private authService: AuthService) {
      
   
  }



  ngOnInit(): void {
    this.authService.autoLogin();
     
    const currentLang = localStorage.getItem('currentLang');
    const lang = ((currentLang == 'undefined' || currentLang == null) ? this.defaultLang : currentLang);
    this.onLangChange(lang);

  }
  onLangChange(lang: any) {
    this.appTranslateService.changeLanguage(lang);
  }
}


