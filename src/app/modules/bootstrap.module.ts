import { NgModule } from '@angular/core';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { ModalModule } from 'ngx-bootstrap/modal';
import { ToastrModule } from 'ngx-toastr';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { TabsModule } from 'ngx-bootstrap/tabs';


@NgModule({
  declarations: [],
  imports: [
    ModalModule.forRoot(),
    PaginationModule.forRoot(),
    ToastrModule.forRoot({
      positionClass: 'toast-bottom-left',
      timeOut: 5000,
      preventDuplicates: true,
      closeButton: true
    }),
    BsDatepickerModule.forRoot(),
    TabsModule
  ],
  exports: [
    ModalModule,
    PaginationModule,
    ToastrModule,
    BsDatepickerModule,
    TabsModule
  ]
})
export class BootstrapModule { }
