import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthComponent } from '../components/auth/auth.component';
import { HomeComponent } from '../components/home/home.component';
import { AuthGuard } from '../components/auth/auth.guard';
import { PageNotFoundComponent } from '../shared/components/page-not-found/page-not-found.component';
import { MerchantSelectComponent } from '../components/merchant/components/merchant-select/merchant-select.component';
import { UserRole } from '../components/auth/models/user.model';
import { MerchantComponent } from '../components/merchant/merchant.component';
import { MerchantListComponent } from '../components/merchant/components/merchant-list/merchant-list.component';
import { MerchantAddUpdateComponent } from '../components/merchant/components/merchant-add-update/merchant-add-update.component';
import { BranchComponent } from '../components/branch/components/branch/branch.component';
import { BranchListComponent } from '../components/branch/components/branch-list/branch-list.component';
import { BranchAddUpdateComponent } from '../components/branch/components/branch-add-update/branch-add-update.component';
import { TerminalComponent } from '../components/terminal/components/terminal/terminal.component';
import { TerminalListComponent } from '../components/terminal/components/terminal-list/terminal-list.component';
import { TerminalAddUpdateComponent } from '../components/terminal/components/terminal-add-update/terminal-add-update.component';
import { AccountSelectComponent } from '../components/account/components/account-select/account-select.component';
import { AccountComponent } from '../components/account/account.component';
import { AccountListComponent } from '../components/account/components/account-list/account-list.component';
import { AccountAddUpdateComponent } from '../components/account/components/account-add-update/account-add-update.component';
import { PlanComponent } from '../components/plan/components/plan/plan.component';
import { PlanListComponent } from '../components/plan/components/plan-list/plan-list.component';
import { PlanAddUpdateComponent } from '../components/plan/components/plan-add-update/plan-add-update.component';
import { InvoiceComponent } from '../components/invoice/invoice.component';
import { UserAddupdateComponent } from '../components/users/users/user-addupdate/user-addupdate.component';
import { UserComponent } from '../components/users/users/user/user.component';
import { UserListComponent } from '../components/users/users/user-list/user-list.component';
import { EchartsComponent } from '../echarts/echarts.component';
import { MonitorComponent } from '../components/monitor/monitor/monitor.component';
import { MonitorlistComponent } from '../components/monitor/monitorlist/monitorlist.component';
import { PointRecordsComponent } from '../components/point-records/point-records.component';
import { PointRecordsListComponent } from '../components/point-records/components/point-records-list/point-records-list.component';
import { PointRecordsAddComponent } from '../components/point-records/components/point-records-add/point-records-add.component';
import { BulkuploadListComponent } from '../components/bulkupload-list/bulkupload-list.component';
import { ExportedInvoicesComponent } from '../components/exported-invoices/exported-invoices.component';




const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
    canActivate: [AuthGuard],
    canActivateChild: [AuthGuard],
    children: [

      {
        path: 'bulkupload/:accountId/:merchantId',
        component: BulkuploadListComponent,
        data: {
          role: [UserRole.SystemAdmin,UserRole.AccountAdmin,UserRole.MerchantAdmin],
        },
      },
      {
        path: 'echarts/:accountId/:merchantId',
        component: EchartsComponent,
        data: {
          role: [UserRole.SystemAdmin,UserRole.AccountAdmin,UserRole.MerchantAdmin],
        },
      },

      {
        path: 'invoice/:accountId/:merchantId',
        component: InvoiceComponent,
        data: {
          role: [UserRole.SystemAdmin, UserRole.AccountAdmin, UserRole.MerchantAdmin],
        },
      },



      {
        path: 'monitor',
        component: MonitorComponent,
        data: {
          role: [UserRole.SystemAdmin],
        },
        children: [
          {
            path: 'monitor-list/:accountId/:merchantId',
            component: MonitorlistComponent,
            data: {
              role: [UserRole.SystemAdmin],
            },
          },

        ],
      },




      {
        path: 'merchant',
        component: MerchantComponent,
        data: {
          role: [UserRole.SystemAdmin, UserRole.AccountAdmin],
        },
        children: [
          {
            path: 'merchant-list/:accountId',
            component: MerchantListComponent,
            data: {
              role: [UserRole.SystemAdmin, UserRole.AccountAdmin],
            },
          },
          {
            path: 'merchant-add/:accountId',
            component: MerchantAddUpdateComponent,
            data: {
              role: [UserRole.SystemAdmin, UserRole.AccountAdmin],
            },
          },
        ],
      },

      {
        path: 'branch',
        component: BranchComponent,
        data: {
          role: [UserRole.SystemAdmin, UserRole.AccountAdmin, UserRole.MerchantAdmin],
        },
        children: [
          {
            path: 'branch-list/:accountId/:merchantId',
            component: BranchListComponent,
            data: {
              role: [UserRole.SystemAdmin, UserRole.AccountAdmin, UserRole.MerchantAdmin],
            },
          },
          {
            path: 'branch-add/:accountId/:merchantId',
            component: BranchAddUpdateComponent,
            data: {
              role: [UserRole.SystemAdmin, UserRole.AccountAdmin, UserRole.MerchantAdmin],
            },
          },
        ],
      },

      {
        path: 'terminal',
        component: TerminalComponent,
        data: {
          role: [UserRole.SystemAdmin, UserRole.AccountAdmin, UserRole.MerchantAdmin],
        },
        children: [
          {
            path: 'terminal-list/:accountId/:merchantId',
            component: TerminalListComponent,
            data: {
              role: [UserRole.SystemAdmin, UserRole.AccountAdmin, UserRole.MerchantAdmin],
            },
          },
          {
            path: 'terminal-add/:accountId/:merchantId',
            component: TerminalAddUpdateComponent,
            data: {
              role: [UserRole.SystemAdmin, UserRole.AccountAdmin, UserRole.MerchantAdmin],
            },
          },
        ],
      },

      {
        path: 'account',
        component: AccountComponent,
        data: {
          role: [UserRole.SystemAdmin],
        },
        children: [
          {
            path: 'account-list/:accountId',
            component: AccountListComponent,
            data: {
              role: [UserRole.SystemAdmin],
            },
          },
          {
            path: 'account-add/:accountId',
            component: AccountAddUpdateComponent,
            data: {
              role: [UserRole.SystemAdmin],
            },
          },
        ],
      },




      {
        path: 'plan',
        component: PlanComponent,
        data: {
          role: [UserRole.SystemAdmin],
        },
        children: [
          {
            path: 'plan-list/:accountId',
            component: PlanListComponent,
            data: {
              role: [UserRole.SystemAdmin],
            },
          },
          {
            path: 'plan-add/:accountId',
            component: PlanAddUpdateComponent,
            data: {
              role: [UserRole.SystemAdmin],
            },
          },
        ],
      },

      {
        path: 'user',
        component: UserComponent,
        data: {
          role: [UserRole.SystemAdmin, UserRole.AccountAdmin, UserRole.MerchantAdmin]
        },
        children: [
          {
            path: 'user-list/:accountId/:merchantId', component: UserListComponent,
            data: {
              role: [UserRole.SystemAdmin, UserRole.AccountAdmin, UserRole.MerchantAdmin]
            }
          },
          {
            path: 'user-add/:accountId/:merchantId', component: UserAddupdateComponent,
            data: {
              role: [UserRole.SystemAdmin, UserRole.AccountAdmin, UserRole.MerchantAdmin]
            }
          }
        ]
      },
      {
        path: 'balance',
        component: PointRecordsComponent,
        data: {
          role: [UserRole.SystemAdmin],
        },
        children: [
          {
            path: 'balance-list/:accountId/:merchantId',
            component: PointRecordsListComponent,
            data: {
              role: [UserRole.SystemAdmin],
            },
          },
          {
            path: 'balance-add/:accountId/:merchantId',
            component: PointRecordsAddComponent,
            data: {
              role: [UserRole.SystemAdmin],
            },
          },
        ]
      },
      {
        path: 'exportedInvoices/:accountId/:merchantId',
        component: ExportedInvoicesComponent,
        data: {
          role: [UserRole.SystemAdmin],
        }
      },
    ],
  },
  {
    path: 'merchants/:accountId',
    component: MerchantSelectComponent,
    canActivate: [AuthGuard],
    data: {
      role: [UserRole.SystemAdmin, UserRole.AccountAdmin],
    },
  },
  {
    path: 'accounts',
    component: AccountSelectComponent,
    canActivate: [AuthGuard],
    data: {
      role: [UserRole.SystemAdmin],
    },
  },
  { path: 'login', component: AuthComponent },
  { path: '**', component: PageNotFoundComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule { }
