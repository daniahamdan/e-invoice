export interface PointRecords {
    account: string
    merchant: string
    pointsDate:string
    expiryDate:string
    balance: number
  }
export interface PointRecordsCreate {
    accountId: string
    merchantId: string
    expiryDate:string
    balance: number
    
  }
  export interface PointRecordsFilter {
    accountId?: string;
    merchantId?: string;
    pageSize?: number;
    pageNumber?: number;
    expiryDate?:string
  }