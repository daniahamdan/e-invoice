import { AfterViewInit, Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Params } from '@angular/router';
import { PointRecordsFilter } from '../../models/point-records.model';
import { BehaviorSubject } from 'rxjs';

@Component({
  selector: 'app-point-records-nav-menu',
  templateUrl: './point-records-nav-menu.component.html',
  styleUrls: ['./point-records-nav-menu.component.css']
})
export class PointRecordsNavMenuComponent implements OnInit, AfterViewInit {
  filterForm!: FormGroup;
  merchantId!: string;
  accountId!: string;

  isCleared = new BehaviorSubject(false);
  $isCleared = this.isCleared.asObservable();

  dateString: string | null = null;
  @Output("filterPoints") filterPoints: EventEmitter<any> = new EventEmitter();
  constructor(
    private route: ActivatedRoute
  ) {
  }
  ngAfterViewInit(): void {
    this.route.params.subscribe((params: Params) => {
      if (this.merchantId != params['merchantId']) {
        this.merchantId = params['merchantId'];
      }
      if (this.accountId != params['accountId']) {
        this.accountId = params['accountId'];
      }
      this.clearForm();
    });
  }

  ngOnInit(): void {
    this.merchantId = this.route.snapshot.params['merchantId'];
    this.accountId = this.route.snapshot.params['accountId'];

  }
  clearForm() {
    this.isCleared.next(true);
    this.filterPoints.emit({ accountId: this.accountId, merchantId: this.merchantId });
  }
  onSave(){
    const data: PointRecordsFilter = this.filterForm.getRawValue();
    data.accountId = this.accountId;
    data.merchantId = this.merchantId;
    this.filterPoints.emit(data);
  }
  onDateChange($event: any) {
    if (!!$event) {
      const data: PointRecordsFilter = {
        accountId : this.accountId,
        merchantId : this.merchantId,
        expiryDate : $event
      };
    this.filterPoints.emit(data);
    }
  }
}
