import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { PointRecords, PointRecordsFilter } from '../../models/point-records.model';
import { PageChangedEvent } from 'ngx-bootstrap/pagination';
import { PointRecordsService } from '../../point-records.service';

@Component({
  selector: 'app-point-records-list',
  templateUrl: './point-records-list.component.html',
  styleUrls: ['./point-records-list.component.css']
})
export class PointRecordsListComponent implements OnInit {
  merchantId!: string;
  accountId!: string;
  records: PointRecords[] = [];
  currentFilter: PointRecordsFilter | null = null;
  paginatorPageSize: number = 10;
  paginatorPageNumber: number = 1;
  itemCount: number = 0;

  constructor(private route:ActivatedRoute, private pointService : PointRecordsService) {
    
  }
  ngOnInit(): void {
    this.merchantId = this.route.snapshot.params['merchantId'];
    this.accountId = this.route.snapshot.params['accountId'];
  }
  ngAfterViewInit() {
    this.route.params.subscribe((params: Params) => {
      if (this.accountId != params['accountId']) {
        this.accountId = params['accountId'];
      }
      if (this.merchantId != params['merchantId']) {
        this.merchantId = params['merchantId'];
      }
      this.getPointList();
    });
  }
  getPointList(pointData?: any) {
    const data = this.setListRequestData(pointData);
    this.pointService
      .getPointList(this.accountId, data)
      .subscribe((pointData) => {
        this.records = pointData.data;
        this.itemCount = pointData.totalCount;
      });
  }
  reloadPointTable(filterData?: any) {
    this.getPointList(filterData);
  }
  setListRequestData(pointData?: PointRecordsFilter | null) {
    if (!!pointData) {
      pointData.pageSize = this.paginatorPageSize;
      pointData.pageNumber = this.paginatorPageNumber;
      return pointData;
    } else {
      return {
        AccountId: this.accountId,
        MerchantId: this.merchantId,
        PageSize: this.paginatorPageSize,
        PageNumber: this.paginatorPageNumber,
      };
    }
  }

  applyPointFilter(filterData?: any) {
    this.paginatorPageNumber = 1;
    this.paginatorPageSize = 10;
    this.currentFilter = filterData;
    this.getPointList(filterData);
  }

  preventCellClick($event: any) {
    $event.stopPropagation();
  }

  pageChanged(event: PageChangedEvent): void {
    this.paginatorPageNumber = event.page;
    this.paginatorPageSize = event.itemsPerPage;
    this.reloadPointTable(this.currentFilter);
  }
}
