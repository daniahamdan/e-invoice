import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { PointRecordsService } from '../../point-records.service';
import { PointRecordsCreate } from '../../models/point-records.model';

@Component({
  selector: 'app-point-records-add',
  templateUrl: './point-records-add.component.html',
  styleUrls: ['./point-records-add.component.css']
})
export class PointRecordsAddComponent implements OnInit {
  accountId!: string;
  merchantId!: string;
  pointForm!: FormGroup;
  submitted: boolean = false;
  constructor(private route: ActivatedRoute,
    private router: Router,
    private pointService: PointRecordsService) {
    
  }

  ngOnInit(): void {
    this.merchantId = this.route.snapshot.params['merchantId'];
    this.accountId = this.route.snapshot.params['accountId'];

    this.pointForm = new FormGroup({
      'expiryDate': new FormControl(null, [Validators.required]),
      'balance': new FormControl(null, [Validators.required]),
    })
  }

  onSave() {
    this.submitted = true;
    if (!this.pointForm.valid) {
      this.pointForm.markAllAsTouched();
      return;
    }
    var data: PointRecordsCreate = this.pointForm.getRawValue();
    data.accountId = this.accountId;
    data.merchantId = this.merchantId;
    this.addPoint(data);
  }
  addPoint(data: any) {
    this.pointService
      .postPoint(this.accountId, data)
      .subscribe(response => {
        if (!!response) {
          this.router.navigate(['balance/balance-list', this.accountId,this.merchantId]);
        }
      });
  }
}
