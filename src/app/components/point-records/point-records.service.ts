import { Injectable } from '@angular/core';
import { tap } from 'rxjs';
import { HttpService } from 'src/app/services/http.service';

@Injectable({
  providedIn: 'root'
})
export class PointRecordsService {

  constructor(private httpService: HttpService) { }

  postPoint(accountId: string | null, data?: any) {
    const id = (accountId == null ? '' : accountId)

    const headers = { 'AccountId': id }
    return this.httpService.post("BalanceHistory", data, headers).pipe(
      tap(response => {
      }))
  }
  getPointList(accountId: string | null, data?: any) {
    const id = (accountId == null ? '' : accountId)

    const headers = { 'AccountId': id }
    return this.httpService.get("BalanceHistory/List", data, headers).pipe(
      tap(response => {
      }))
  }
}
