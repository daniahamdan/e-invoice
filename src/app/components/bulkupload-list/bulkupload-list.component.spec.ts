import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BulkuploadListComponent } from './bulkupload-list.component';

describe('BulkuploadListComponent', () => {
  let component: BulkuploadListComponent;
  let fixture: ComponentFixture<BulkuploadListComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [BulkuploadListComponent]
    });
    fixture = TestBed.createComponent(BulkuploadListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
