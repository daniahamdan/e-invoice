export class Bulkupload {
    username!: string;
    terminalName!: string;
    id!: string;
    invoiceCount!: number;
    successCount!: number;
    failedCount!: number;
    status!: InvoiceBulkUploadStatus;
    bulkuploadStatusString!: string;
    bulkuploadString!: string;


}


export interface BulkuploadFilter {
    accountId?: string;
    terminalId?: string;
    merchantId?: string;
    FromDate?: string;
    ToDate?: string;
    status?: InvoiceBulkUploadStatus;
    PageSize?: number;
    PageNumber?: number;
}




export enum InvoiceBulkUploadStatus {

    Pending = 0,
    Processing = 1,
    Suceeded = 2,
    Failed = 3,
}

