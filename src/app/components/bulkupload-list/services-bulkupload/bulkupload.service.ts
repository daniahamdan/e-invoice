import { Injectable } from '@angular/core';
import { tap } from 'rxjs';
import { HttpService } from 'src/app/services/http.service';

@Injectable({
  providedIn: 'root'
})
export class BulkuploadService {

  constructor(private httpService: HttpService){ 

  }


  getBulkUploadList(accountId: string|null, data?: any) {
    const id = (accountId == null ? '' : accountId)
  
      const headers = { 'AccountId': id }
      return this.httpService.get("InvoiceBulkUpload/List", data, headers).pipe(
        tap((response: any) => { 
        }))
    }
}
