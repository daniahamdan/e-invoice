import { TestBed } from '@angular/core/testing';

import { BulkuploadService } from './bulkupload.service';

describe('BulkuploadService', () => {
  let service: BulkuploadService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(BulkuploadService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
