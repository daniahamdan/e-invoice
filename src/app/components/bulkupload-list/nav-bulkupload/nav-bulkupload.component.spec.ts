import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NavBulkuploadComponent } from './nav-bulkupload.component';

describe('NavBulkuploadComponent', () => {
  let component: NavBulkuploadComponent;
  let fixture: ComponentFixture<NavBulkuploadComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [NavBulkuploadComponent]
    });
    fixture = TestBed.createComponent(NavBulkuploadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
