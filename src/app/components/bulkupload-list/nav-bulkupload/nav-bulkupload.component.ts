import { DatePipe } from '@angular/common';
import { Component, EventEmitter, Output } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Params } from '@angular/router';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { IdName } from 'src/app/shared/interfaces/idName.interface';
import { TerminalService } from '../../terminal/components/services-terminal/terminal.service';
import { BulkuploadFilter, InvoiceBulkUploadStatus } from '../models/bulkupload.model';

@Component({
  selector: 'app-nav-bulkupload',
  templateUrl: './nav-bulkupload.component.html',
  styleUrls: ['./nav-bulkupload.component.css']
})
export class NavBulkuploadComponent {
  terminals!: Observable<IdName[]>;
  filterForm!: FormGroup;
  accountId!: string;
  merchantId!: string;
  fromDateString: string | null = null;
  toDateString: string | null = null;
  isCleared = new BehaviorSubject(false);
  $isCleared = this.isCleared.asObservable();
  invoiceTypeKeys: string[] = [];
  bulkuploadStatusKeys: string[] = [];
  StatusKeys: string[] = [];
  bulkuploadStatus = InvoiceBulkUploadStatus;

  @Output("filterBulkupload") filterBulkupload: EventEmitter<any> = new EventEmitter();
  translate: any;
  toastService: any;
  filterMonitors: any;




  constructor(private datePipe: DatePipe,
    private terminalService: TerminalService,
    private route: ActivatedRoute) {
  
   this.bulkuploadStatusKeys = Object.keys(this.bulkuploadStatus).filter(f => !isNaN(Number(f)));
  }




  ngAfterViewInit(): void {
    this.route.params.subscribe(
      (params: Params) => {
        if (this.accountId != params['accountId']) {
          this.accountId = params['accountId'];
        }
        if (this.merchantId != params['merchantId']) {
          this.merchantId = params['merchantId'];
          this.clearForm();
          this.getTerminals();
          this.isCleared.next(true);
        }
      }
    );
  }


  ngOnInit(): void {
    this.accountId = this.route.snapshot.params['accountId'];
    this.merchantId = this.route.snapshot.params['merchantId'];

    this.filterForm = new FormGroup({
      'selectedTerminal': new FormControl(null),
      'bulkuploadStatus': new FormControl(null),
    });

    this.getTerminals();
  }


  onSave() {
    
   const terminalId = this.filterForm.get('selectedTerminal')?.value;
    const bulkuploadStatus = this.filterForm.get('bulkuploadStatus')?.value;
    const data: BulkuploadFilter = { accountId: this.accountId, merchantId:this.merchantId };
    if (!!terminalId) {
      data.terminalId = terminalId;
    }
    if (!!this.fromDateString) {
      data.FromDate = this.fromDateString;
    }
    if (!!this.toDateString) {
      data.ToDate = this.toDateString;
    }
  

    if (!!bulkuploadStatus) {
      data.status =  bulkuploadStatus;
    }

    this.filterBulkupload.emit(data);
  }


  getTerminals() {
    this.terminalService.getTerminalList(this.accountId, { PageSize: 1000, PageNumber: 1, MerchantId: this.merchantId })
      .subscribe(response => {
        if (!!response) {
          this.terminals = of(response.data.map((item: any) => ({
            id: item.id,
            name: item.nameEn,
          }))).pipe();
        }
      });
  }





  clearForm() {
    this.filterForm?.controls['selectedTerminal'].setValue(null);
    this.filterForm?.controls['bulkuploadStatus'].setValue(null);
    this.fromDateString = null;
    this.toDateString = null;
  }
  emitClearFilter() {
    this.clearForm();
    this.isCleared.next(true);
    this.filterBulkupload.emit({ accountId: this.accountId, merchantId: this.merchantId });
  }
  onDateChange($event: any) {
    if (!!$event)
      this.setRangeDate($event)
  }

  setRangeDate(date: string) {
    if (!!date) {
      const range = date.split(';');
      this.fromDateString = range[0];
      this.toDateString = range[1];
      this.onSave();
    }
  }





}
