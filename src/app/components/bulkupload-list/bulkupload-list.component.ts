import { Component } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { BsModalService } from 'ngx-bootstrap/modal';
import { PageChangedEvent } from 'ngx-bootstrap/pagination';
import { Bulkupload, BulkuploadFilter, InvoiceBulkUploadStatus } from './models/bulkupload.model';
import { BulkuploadService } from './services-bulkupload/bulkupload.service';

@Component({
  selector: 'app-bulkupload-list',
  templateUrl: './bulkupload-list.component.html',
  styleUrls: ['./bulkupload-list.component.css']
})
export class BulkuploadListComponent {



  accountId: string | null = '';
  merchantId: string | null = '';
 
  bulkuploads: Bulkupload[] = [];
  paginatorPageSize: number = 10;
  paginatorPageNumber: number = 1;
  itemCount: any;
  currentFilter: BulkuploadFilter | null = null;
  getScreenWidth!: number;
  tableMaxSize: number=10;
  getScreenHeight!: number;
 

  constructor(
    private bulkuploadService: BulkuploadService,
    public router: Router,
    public route: ActivatedRoute,
    private modalService: BsModalService
  ) { }

  ngOnInit(): void {
  
    this.accountId = this.route.snapshot.params['accountId'];
    this.merchantId = this.route.snapshot.params['merchantId'];
    this.getScreenWidth = window.innerWidth;
    this.getScreenHeight = window.innerHeight;
    if(this.getScreenWidth < 1000){
      this.tableMaxSize = 3;
    }
  }


  ngAfterViewInit() {
    this.route.params.subscribe((params: Params) => {
      if (this.accountId != params['accountId']) {
        this.accountId = params['accountId'];
      }
      if (this.merchantId != params['merchantId']) {
        this.merchantId = params['merchantId'];
      }
   
     this.getbulkuploadList();
    });


   
  }


  getbulkuploadList(bulkuploadData?: any) {
    const data = this.setListRequestData(bulkuploadData);
    this.bulkuploadService
      .getBulkUploadList(this.accountId, data)
      .subscribe((bulkuploadData) => {
        this.bulkuploads = bulkuploadData.data;
        this.itemCount =  bulkuploadData.totalCount;
        console.log(this.bulkuploads);
        this.bulkuploads.map((i) => {
          i.bulkuploadString = InvoiceBulkUploadStatus[i.status];
        });
   
      });
  }



  reloadbulkuploadTable(filterData?: any) {
    this.getbulkuploadList(filterData);
  }
  setListRequestData(bulkuploadData?: BulkuploadFilter  | null) {
    if (!!bulkuploadData) {
      bulkuploadData.PageSize = this.paginatorPageSize;
      bulkuploadData.PageNumber = this.paginatorPageNumber;
      return bulkuploadData;
    } else {
      return {
        AccountId: this.accountId,
        MerchantId: this.merchantId,
        PageSize: this.paginatorPageSize,
        PageNumber: this.paginatorPageNumber,
      };
    }
  }


  applyBulkuploadFilter(filterData?: any) {
    this.paginatorPageNumber = 1;
    this.paginatorPageSize = 10;
    this.currentFilter = filterData;
    this.getbulkuploadList(filterData);
  }

  preventCellClick($event: any) {
    $event.stopPropagation();
  }

  pageChanged(event: PageChangedEvent): void {
    this.paginatorPageNumber = event.page;
    this.paginatorPageSize = event.itemsPerPage;
    this.reloadbulkuploadTable(this.currentFilter);
  }
  
}
