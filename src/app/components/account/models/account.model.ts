export interface Account {
    id: string;
    adminPassword:string;
    name:string;
   statusString: string;
    status: AccountStatus;
    clientId:string;
    clientSecret:string;

}

export enum AccountStatus {
    Active = 0,
  
    InActive = 1,
  }


  export interface AccountPlan {
    name:string;
  }


  export interface AccountCreateUpdate {
 
    name: string;
    adminPassword:string;
    status: AccountStatus;
    clientId:string;
    clientSecret:string;
  }


  export interface AccountFilter {
    //  accountId: string;
     
      pageSize?: number;
      pageNumber?: number;
      searchPattern?: string;
      status?: AccountStatus;
    }
  
  