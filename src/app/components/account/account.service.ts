import { HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, tap } from 'rxjs';
import { HttpService } from 'src/app/services/http.service';

@Injectable({
  providedIn: 'root'
})
export class AccountService {

  private newAccountData = new BehaviorSubject(false);

  newAccountObservable = this.newAccountData.asObservable();

  constructor(private httpService: HttpService) { }

  emitNewAccountData() {
    this.newAccountData.next(true);
  }

  getAccountList(data?: any) {
      return this.httpService.get("Account/List", data).pipe(
        tap(response => { 
        }))
    }

    getAccount(id: string) {
      return this.httpService.getbyId("Account", id).pipe(
        tap(response => {
        }))
    }


    postAccount(accountId: string|null, data: any) {
      const id = (accountId == null ? '' : accountId)
  
      const headers = { 'AccountId': id }
      return this.httpService.post("Account", data, headers).pipe(
        tap(response => {
         this.emitNewAccountData();
        }));
    }



    updateAccount(id: string, data: any) {
      return this.httpService.update("Account", id, data).pipe(
        tap(response => {
          this.emitNewAccountData();
        }));
    }


    deleteAccount(id: string) {
      const param = new HttpParams()
        .set('id', id);
      return this.httpService.delete("Account", param).pipe(
        tap(response => {
          this.emitNewAccountData();
        }));
    }

    getPlans(accountId: string|null, data?: any){
      const id = (accountId == null ? '' : accountId)
  
      const headers = { 'AccountId': id }
      return this.httpService.get("Plan/List", data, headers).pipe(
        tap(response => { 
        }))
    }
    
  


  

}
