import { Component, Input } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { ClipboardService } from 'ngx-clipboard';
import { AccountStatus } from '../../../models/account.model';
import { faCopy } from '@fortawesome/free-solid-svg-icons';
import { Directionality } from '@angular/cdk/bidi';

@Component({
  selector: 'app-account-view',
  templateUrl: './account-view.component.html',
  styleUrls: ['./account-view.component.css']
})
export class AccountViewComponent {
  [x: string]: any;
  statusKeys: string[] = [];
  status = AccountStatus;
  currentData: any;
  // clientId!: string;
 // readonly : number;
  value: any
  copyIcon = faCopy;

  @Input() data: any;

  btn1text!: string;

  constructor(public bsModalRef: BsModalRef, private clipboardService: ClipboardService,private directionality: Directionality) {
    this.statusKeys = Object.keys(this.status).filter((f) => !isNaN(Number(f)));
  }
  ngOnInit(): void {
    this.currentData = this.data;
    //  this.currentData = this.clientId;
    // this.clientId="";
  }


 

  switchToRTL() {
 
    //this.directionality.value='rtl';
  }

  switchToLTR() {
   // this.directionality.setValue ('ltr');
  }



  copyContent() {
    // debugger
    // const clientId = +this.fillAccountForm?.value.clientId ;
    //  let inputVal = document.getElementsByClassName("clientId");
    this.clipboardService.copyFromContent(this.currentData.clientId);
  }

  copyContentt() {
    // debugger
    // const clientId = +this.fillAccountForm?.value.clientId ;
    //  let inputVal = document.getElementsByClassName("clientId");
    this.clipboardService.copyFromContent(this.currentData.clientSecret);
  }

  copyText() {
    // debugger
    navigator.clipboard.writeText(this.data);
  }





}
