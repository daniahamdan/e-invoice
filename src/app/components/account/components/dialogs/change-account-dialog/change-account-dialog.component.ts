import { Component, EventEmitter, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { Observable, of } from 'rxjs';
import { AccountService } from '../../../account.service';
import { AuthService } from 'src/app/components/auth/auth.service';
import { IdName } from 'src/app/shared/interfaces/idName.interface';
import { MerchantService } from 'src/app/components/merchant/merchant.service';

@Component({
  selector: 'app-change-account-dialog',
  templateUrl: './change-account-dialog.component.html',
  styleUrls: ['./change-account-dialog.component.css'],
})
export class ChangeAccountDialogComponent implements OnInit {
  accountName: string = '';
  accounts!: Observable<IdName[]>;
  accountId: string | null = null;
  selectedAccount!: string;

  merchantName: string = '';
  merchants!: Observable<IdName[]>;
  merchantId: string | null = null;
  selectedMerchant!: string;

  isSystemAdmin: boolean = false;
  isAccountAdmin: boolean = false;

  public selectedAccountEvent: EventEmitter<any> = new EventEmitter();
  public selectedMerchantEvent: EventEmitter<any> = new EventEmitter();
  constructor(
    private accountService: AccountService,
    private merchantService: MerchantService,
    private authService: AuthService,
    private router: Router,
    private bsModalRef: BsModalRef
  ) {}
  ngOnInit(): void {
    this.isSystemAdmin = this.authService.getLoggedInUser()?.userRole == 1;
    this.isAccountAdmin = this.authService.getLoggedInUser()?.userRole == 2;
    this.getAccountId();
    this.getMerchantId();
    if (this.isSystemAdmin) {
      this.getAccountList();
      this.getMerchantList(this.accountId);
      this.accountService.newAccountObservable.subscribe((response) => {
        if (response) {
          this.getAccountList();
        }
      });
    } else if (this.isSystemAdmin || this.isAccountAdmin) {
      this.getMerchantList(this.accountId);
      this.merchantService.newMerchantObservable.subscribe((response) => {
        if (response) {
          this.getMerchantList(this.accountId);
        }
      });
    }
  }

  getMerchantId() {
    const userMerchantId = this.authService.getLoggedInUser()?.merchantId;
    const selectedMerchantId = localStorage.getItem(
      'currentSelectedMerchantId'
    );
    if (!this.isSystemAdmin) {
      if (!!userMerchantId) {
        this.merchantId = userMerchantId;
      }
    } else {
      this.merchantId = selectedMerchantId;
    }
  }
  getAccountId() {
    const userAccountId = this.authService.getLoggedInUser()?.accountId;
    const selectedAccountId = localStorage.getItem('currentSelectedAccountId');
    if (!this.isSystemAdmin) {
      if (!!userAccountId) {
        this.accountId = userAccountId;
      }
    } else {
      this.accountId = selectedAccountId;
    }
  }

  getAccountList() {
    this.accountService
      .getAccountList({ PageSize: 1000, PageNumber: 1 })
      .subscribe((response) => {
        if (!!response) {
          this.accountName = response.data.find(
            (x: IdName) => x.id === this.accountId
          )?.name;
          this.accounts = of(
            response.data.map((item: IdName) => ({
              id: item.id,
              name: item.name,
            }))
          ).pipe();
        }
      });
  }
  getMerchantList(accountId: string | null) {
    this.merchantService
      .getMerchantList(accountId, {
        PageSize: 1000,
        PageNumber: 1,
        accountId: accountId,
      })
      .subscribe((response) => {
        if (!!response) {
          this.merchantName = response.data.find(
            (x: IdName) => x.id === this.merchantId
          )?.nameEn;
          this.merchants = of(
            response.data.map((item: any) => ({
              id: item.id,
              name: item.nameEn,
            }))
          ).pipe();
        }
      });
  }
  onAccountSelect(accountId: string) {
    this.selectedAccount = accountId;
    this.getMerchantList(accountId);
  }
  onMerchantSelect(merchantId: string) {
    this.selectedMerchant = merchantId;
  }
  onAccountChange() {
    if (this.isSystemAdmin) {
      this.updateSysAdminAccount();
    } else if (this.isAccountAdmin) {
      this.updateAdminMerchant();
    }
    this.removeModalAttr();
  }
  updateSysAdminAccount() {
    if (!!this.selectedAccount && !!this.selectedMerchant) {
      const currentAccountIdParam = this.updateCurrentAccountId();
      this.accountId = this.selectedAccount;
      const currentMerchantIdParam = this.updateCurrentMerchantId();
      this.merchantId = this.selectedMerchant;

      const currentUrl = this.router.url;
      const accountIndex = currentUrl.indexOf(
        currentAccountIdParam == null ? '' : currentAccountIdParam
      );
      const merchantIndex = currentUrl.indexOf(
        currentMerchantIdParam == null ? '' : currentMerchantIdParam
      );

      if (accountIndex > -1 || merchantIndex > -1) {
        const currentAccountId = currentUrl.substring(
          accountIndex,
          accountIndex + this.accountId.length
        );
        const currentMerchantId = currentUrl.substring(
          merchantIndex,
          merchantIndex + this.merchantId.length
        );

        let newUrl = currentUrl
          .replace(currentAccountId, this.selectedAccount);
          if(merchantIndex > 0){
            newUrl = newUrl.replace(currentMerchantId, this.selectedMerchant);
          }

        this.router.navigateByUrl(newUrl);
        this.accounts.subscribe((items: any) => {
          this.accountName = items.find(
            (x: IdName) => x.id === this.accountId
          ).name;
        });
        this.emitNewMerchant();
        this.selectedAccountEvent.emit({
          id: this.selectedAccount,
          name: this.accountName,
        });
      }
    }
  }
  updateAdminMerchant() {
    if (!!this.selectedMerchant) {
      const currentMerchantIdParam = this.updateCurrentMerchantId();
      this.merchantId = this.selectedMerchant;
      const currentUrl = this.router.url;
      const index = currentUrl.indexOf(
        currentMerchantIdParam == null ? '' : currentMerchantIdParam
      );
      if (index > -1) {
        const currentMerchantId = currentUrl.substring(
          index,
          index + this.merchantId.length
        );

        const newUrl = currentUrl.replace(
          currentMerchantId,
          this.selectedMerchant
        );

        this.router.navigateByUrl(newUrl);
        this.merchants.subscribe((items: any) => {
          this.merchantName = items.find(
            (x: IdName) => x.id === this.merchantId
          ).name;
        });
        this.selectedMerchantEvent.emit({
          id: this.selectedMerchant,
          name: this.merchantName,
        });
      }
    }
  }
  emitNewMerchant() {
    this.merchants.subscribe((items: any) => {
      this.merchantName = items.find(
        (x: IdName) => x.id === this.merchantId
      ).name;
    });
    this.selectedMerchantEvent.emit({
      id: this.selectedMerchant,
      name: this.merchantName,
    });
  }
  updateCurrentAccountId() {
    const currentAccountIdParam = localStorage.getItem(
      'currentSelectedAccountId'
    );
    localStorage.removeItem('currentSelectedAccountId');
    localStorage.setItem('currentSelectedAccountId', this.selectedAccount);
    return currentAccountIdParam;
  }
  updateCurrentMerchantId() {
    const currentMerchantIdParam = localStorage.getItem(
      'currentSelectedMerchantId'
    );
    localStorage.removeItem('currentSelectedMerchantId');
    localStorage.setItem('currentSelectedMerchantId', this.selectedMerchant);
    return currentMerchantIdParam;
  }
  onAccountModelClose() {
    this.selectedAccount = '';
    this.bsModalRef.hide();
    this.removeModalAttr();
  }
  removeModalAttr() {
    document.body.classList.remove('modal-open');
    document.body.removeAttribute('style');
  }
}
