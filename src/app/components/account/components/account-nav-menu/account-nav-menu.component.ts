import { Component, EventEmitter, Output } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Params } from '@angular/router';
import { faSearch } from '@fortawesome/free-solid-svg-icons';
import { TranslateService } from '@ngx-translate/core';
import { AuthService } from 'src/app/components/auth/auth.service';
import { ToastService } from 'src/app/services/toast-service/toast.service';
import { AccountFilter, AccountStatus } from '../../models/account.model';

@Component({
  selector: 'app-account-nav-menu',
  templateUrl: './account-nav-menu.component.html',
  styleUrls: ['./account-nav-menu.component.css']
})
export class AccountNavMenuComponent {

  searchIcon = faSearch;

  statusKeys: string[] = [];
  status = AccountStatus;

  filterForm!: FormGroup;

  accountId!: string
  selectedStatus!: number | null;
  constructor(
    private toastService: ToastService,
    private authService: AuthService,
    private translate: TranslateService,
    private route: ActivatedRoute) {
    this.statusKeys = Object.keys(this.status).filter(f => !isNaN(Number(f)));
  }
  ngAfterViewInit(): void {
    this.route.params.subscribe(
      (params: Params) => {
        if (this.accountId != params['accountId']) {
          this.accountId = params['accountId'];
          this.clearForm();
        }
      }
    );
  }

  ngOnInit(): void {
    
    this.accountId = this.route.snapshot.params['accountId'];
    this.filterForm = new FormGroup({
      'searchPattern': new FormControl(null),
      'status': new FormControl(null),
    });
  }

  @Output("filterAccounts") filterAccounts: EventEmitter<any> = new EventEmitter();

  onAccountSearch(keyElement: any) {
    let searchPattern = keyElement.target.value;
    if (searchPattern.length < 3) {
      const searchPatternLength = this.translate
        .instant('general.searchPatternLength');
      this.toastService.error(searchPatternLength);
      return;
    }
    this.filterAccounts.emit({ SearchPattern: searchPattern });
  }
  onStatusChange(status: number) {
    this.selectedStatus = status;
    const searchPattern = this.filterForm?.value.searchPattern
    const data: AccountFilter = {
      status: status
    }
    if (!!searchPattern) {
      data.searchPattern = searchPattern;
    }
    this.filterAccounts.emit(data)
  }
  clearForm() {
    this.selectedStatus = null;
    this.filterForm?.controls['searchPattern'].setValue('');
    this.filterForm?.controls['status'].setValue(null);
    this.filterAccounts.emit({});
  }



}
