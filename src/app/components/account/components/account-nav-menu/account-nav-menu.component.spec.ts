import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountNavMenuComponent } from './account-nav-menu.component';

describe('AccountNavMenuComponent', () => {
  let component: AccountNavMenuComponent;
  let fixture: ComponentFixture<AccountNavMenuComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [AccountNavMenuComponent]
    });
    fixture = TestBed.createComponent(AccountNavMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
