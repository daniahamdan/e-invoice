import { Component } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { faEllipsisV, faRotate, faTrash } from '@fortawesome/free-solid-svg-icons';
import { TranslateService } from '@ngx-translate/core';
import { BsModalService, ModalOptions, BsModalRef } from 'ngx-bootstrap/modal';
import { PageChangedEvent } from 'ngx-bootstrap/pagination';

import { ToastService } from 'src/app/services/toast-service/toast.service';
import { ConfirmationDialogComponent } from 'src/app/shared/components/confirmation-dialog/confirmation-dialog.component';
import { AccountService } from '../../account.service';
import { Account, AccountFilter, AccountStatus } from '../../models/account.model';
import { AccountViewComponent } from '../dialogs/account-view/account-view.component';

@Component({
  selector: 'app-account-list',
  templateUrl: './account-list.component.html',
  styleUrls: ['./account-list.component.css']
})
export class AccountListComponent {

  actionIcon = faEllipsisV;
  updateIcon = faRotate;
  deleteIcon = faTrash;
  accounts: Account[] = [];
  isUpdate: boolean = false;
  itemCount: number = 0;
  addsuccessMessage!: string;

  currentFilter: AccountFilter | null = null;

  paginatorPageSize: number = 10;
  paginatorPageNumber: number = 1;

  accountId: string | null = '';
  getScreenWidth!: number;
  getScreenHeight!: number;
  tableMaxSize: number=10;

  constructor(
    private accountService: AccountService,
    public router: Router,
    public route: ActivatedRoute,
    private modalService: BsModalService,
    private toastService: ToastService,
    private translate: TranslateService
  ) {}
  ngOnInit(): void {
    this.accountId = this.route.snapshot.params['accountId'];
    this.getScreenWidth = window.innerWidth;
    this.getScreenHeight = window.innerHeight;
    if(this.getScreenWidth < 1000){
      this.tableMaxSize = 3;
    }
  }

  ngAfterViewInit() {
    this.route.params.subscribe((params: Params) => {
      if (this.accountId != params['accountId']) {
        this.accountId = params['accountId'];
      }
      this.getAccountList();
    });
  }

  getAccount(id: string) {
    this.accountService.getAccount(id).subscribe((response) => {
      return response;
    });
  }

  openDeleteConfirmationModal(id: string): void {
    const config: ModalOptions = { class: 'modal-sm modal-dialog-centered' };
    const modalRef: BsModalRef = this.modalService.show(
      ConfirmationDialogComponent,
      config
    );
    modalRef.content.deleteConfirmed.subscribe(() => {
      this.confirmDelete(id);
    });
  }
  confirmDelete(id: string) {
    this.accountService.deleteAccount(id).subscribe(() => {
      const deletedSuccessMessage = this.translate.instant(
        'general.deletedSuccessMessage',
        { componentName: 'account' }
      );
      this.toastService.success(deletedSuccessMessage);
      this.reloadAccountTable(this.currentFilter);
    });
  }
  updateAccount(id: string) {
    this.router.navigate([
      '/account/account-add'
      , this.accountId
      , { isEdit: true, editId: id },
    ]);
    this.isUpdate = true;
  }
  getAccountList(accountData?: any) {
    const data = this.setListRequestData(accountData);
    this.accountService.getAccountList( data).subscribe((accountData) => {
      this.accounts = accountData.data;
      this.itemCount = accountData.totalCount;
      this.accounts.map((i) => {
        i.statusString = AccountStatus[i.status];
      });
    });
  }
  reloadAccountTable(filterData?: any) {
    this.getAccountList(filterData);
  }
  setListRequestData(accountData?: AccountFilter | null) {
    if (!!accountData) {
      accountData.pageSize = this.paginatorPageSize;
      accountData.pageNumber = this.paginatorPageNumber;
      return accountData;
    } else {
      return {
        PageSize: this.paginatorPageSize,
        PageNumber: this.paginatorPageNumber,
      };
    }
  }

  applyAccountFilter(filterData?: any) {
    this.paginatorPageNumber = 1;
    this.paginatorPageSize = 10;
    this.currentFilter = filterData;
    this.getAccountList(filterData);
  }

  viewAccount(rowData: any) {
    this.accountService.getAccount( rowData.id).subscribe((response) => {
      const config: ModalOptions = {
        backdrop: true,
        class: 'modal-xl modal-dialog-centered',
        initialState: { data: response },
      };
      const modalRef: BsModalRef = this.modalService.show(
        AccountViewComponent,
        config
      );
    });
  }
  preventCellClick($event: any) {
    $event.stopPropagation();
  }

  pageChanged(event: PageChangedEvent): void {
    this.paginatorPageNumber = event.page;
    this.paginatorPageSize = event.itemsPerPage;
    this.reloadAccountTable(this.currentFilter);
  }

}
