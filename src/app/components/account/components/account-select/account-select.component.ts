import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppTranslateService } from 'src/app/services/translate-service/translate.service';
import { environment } from 'src/environments/environment';
import { IdName } from 'src/app/shared/interfaces/idName.interface';
import { faGlobe } from '@fortawesome/free-solid-svg-icons';
import { AccountService } from '../../account.service';
import { Account } from '../../models/account.model';

@Component({
  selector: 'app-account-select',
  templateUrl: './account-select.component.html',
  styleUrls: ['./account-select.component.css']
})
export class AccountSelectComponent implements OnInit {
  globeIcon = faGlobe;
  brandName: string = environment.brandName;
  defaultLang: string = environment.defaultLang;

  accounts: IdName[] = [];

  selectedAccount: string = '';

  
  languages = [
    {
      name: 'english',
      value: 'en-US'


    },
    {
      name: 'arabic',
      value: 'ar-SA'
    }
  ]
  logoSrc: string = "../../../../assets/logo/sign_logo.png"

  constructor(private accountService: AccountService,
    private router: Router,
    private route: ActivatedRoute,
    private appTranslateService: AppTranslateService
  ) { }

  ngOnInit(): void {
    const currentLang = localStorage.getItem('currentLang');
    this.defaultLang = ((currentLang == null || currentLang == 'undefined') ? environment.defaultLang : currentLang);

  
    this.setAccounts();

    if (window.innerWidth <= 990) {
      this.logoSrc = '../../../../assets/logo/sign_logo_mobile.png';
    }
  }

  setAccounts() {
    this.accountService.getAccountList({ PageSize: 1000, PageNumber: 1 })
      .subscribe(response => {
        if (!!response) {
          response.data.forEach((i: Account) => {
            this.accounts.push({ name: `${i.name}`, id: i.id });
          });
        }
      });
  }

  onAccountSelect(accountId: string) {
    this.selectedAccount = accountId;
  }

  confirmSelectedAccount() {
    localStorage.setItem('currentSelectedAccountId', this.selectedAccount);
    if (!!this.selectedAccount) {
      //route accordingly
      this.router.navigate(['/merchants', this.selectedAccount]);
    }
  }
  onLangChange(lang: any) {
    lang = lang.value;
    this.appTranslateService.changeLanguage(lang);
  }

}
