import { Component, Input } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { faCopy } from '@fortawesome/free-solid-svg-icons';
import { ClipboardService } from 'ngx-clipboard';

import { AccountService } from '../../account.service';
import { AccountCreateUpdate, AccountStatus } from '../../models/account.model';

@Component({
  selector: 'app-account-add-update',
  templateUrl: './account-add-update.component.html',
  styleUrls: ['./account-add-update.component.css']
})
export class AccountAddUpdateComponent {
  accountForm!: FormGroup;
  statusKeys: string[] = [];
  status = AccountStatus;
  currdata: any;
  accountId!: string;
  submitted: boolean = false;
  accountEditId: string = '';
  isEditView: boolean = false;
  copyIcon = faCopy;

  clientId: string = "";
  clientSecret: string = "";
  // clientId!: string;




  constructor(private accountService: AccountService,
    private route: ActivatedRoute,
    private router: Router,
    private clipboardService: ClipboardService
  ) {
    this.statusKeys = Object.keys(this.status).filter(f => !isNaN(Number(f)));
  }

  ngOnInit(): void {

    this.accountId = this.route.snapshot.params['accountId'];
    this.accountEditId = this.route.snapshot.params['editId'];
    const isEdit = this.route.snapshot.params['isEdit'];
    this.isEditView = (isEdit === 'true' ? true : this.isEditView);
    if (this.isEditView) {
      this.getAccountById(this.accountEditId);
    }

    this.accountForm = new FormGroup({
      'name': new FormControl(null, [Validators.required, Validators.minLength(3), Validators.maxLength(255)]),
      //'clientId': new FormControl(null, [(!this.isEditView ? Validators.nullValidator : Validators.required), Validators.minLength(5), Validators.maxLength(255)]),
      // 'clientSecret': new FormControl(null, [(!this.isEditView ? Validators.nullValidator : Validators.required), Validators.minLength(5), Validators.maxLength(255)]),
      'adminPassword': new FormControl(null, [(this.isEditView ? Validators.nullValidator : Validators.required), Validators.minLength(5), Validators.maxLength(255)])
    });


  }
  getAccountById(id: string) {
    this.accountService
      .getAccount(id)
      .subscribe(response => {
        this.fillAccountForm(response);
        this.clientId = response.clientId
        this.clientSecret = response.clientSecret
      });
  }
  fillAccountForm(currentData: any) {

    this.accountForm.controls['name'].setValue(currentData.name);
    // this.accountForm.controls['clientId'].setValue(currentData.clientId);
    // this.accountForm.controls['clientSecret'].setValue(currentData.clientSecret);
  }

  onSave() {

    this.submitted = true;
    if (!this.accountForm.valid) {
      this.accountForm.markAllAsTouched();
      return;
    }

    var data: AccountCreateUpdate = this.accountForm.getRawValue();
    if (this.isEditView) {
      this.updateAccount(data);
    }
    else {
      this.addAccount(data);
    }
  }


  copyContent() {
    this.clipboardService.copyFromContent(this.clientId);
  }

  copyContentt() {
    this.clipboardService.copyFromContent(this.clientSecret);
  }

  addAccount(data: any) {
    this.accountService.postAccount(this.accountId, data)
      .subscribe((response: any) => {
        if (!!response) {
          this.router.navigate(['account/account-list', this.accountId]);
        }
      });
  }

  updateAccount(data: any) {
    this.accountService.updateAccount(this.accountEditId, data)
      .subscribe((response: any) => {
        if (!!response) {
          this.router.navigate(['account/account-list', this.accountId]);
        }
      });
  }
}
