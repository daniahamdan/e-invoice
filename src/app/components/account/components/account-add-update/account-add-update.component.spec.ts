import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountAddUpdateComponent } from './account-add-update.component';

describe('AccountAddUpdateComponent', () => {
  let component: AccountAddUpdateComponent;
  let fixture: ComponentFixture<AccountAddUpdateComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [AccountAddUpdateComponent]
    });
    fixture = TestBed.createComponent(AccountAddUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
