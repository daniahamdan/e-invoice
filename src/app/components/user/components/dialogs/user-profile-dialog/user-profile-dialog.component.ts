import { DOCUMENT } from '@angular/common';
import { Component, Inject, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { FormGroup } from '@angular/forms';
import { faAngleLeft, faAngleRight, faGlobe, faPowerOff } from '@fortawesome/free-solid-svg-icons';

import { BsModalRef, BsModalService, ModalOptions } from 'ngx-bootstrap/modal';
import { AuthService } from 'src/app/components/auth/auth.service';
import { Login } from 'src/app/components/auth/models/login.model';
import { ChangepasswordService } from 'src/app/components/changepassword/services/changepassword.service';
import { ViewChangepasswordComponent } from 'src/app/components/changepassword/view-changepassword/view-changepassword.component';
import { AppTranslateService } from 'src/app/services/translate-service/translate.service';
import { environment } from 'src/environments/environment';


@Component({
  selector: 'app-user-profile-dialog',
  templateUrl: './user-profile-dialog.component.html',
  styleUrls: ['./user-profile-dialog.component.css']
})
export class UserProfileDialogComponent implements OnInit {
  globeIcon = faGlobe;
  logoutIcon = faPowerOff;
  angleRightIcon = faAngleRight;
  angleLeftIcon = faAngleLeft;
  loginForm!: FormGroup;
  oldPassword!: FormControl;
  newPassword!: FormControl;
  cnewPassword!: FormControl;
 UpdatePasswordForm!: FormGroup;
  error: string | null = null;
  submitted: boolean = false;
  languages = [
    {
      name: 'english',
      value: 'en-US'


    },
    {
      name: 'arabic',
      value: 'ar-SA'
    }
  ]
  username: string = '';
  defaultLang: string = environment.defaultLang;
  year = new Date().getFullYear();
  accountId!: string;
  constructor(
    @Inject(DOCUMENT) document: Document,
    private authService: AuthService,
    private appTranslateService: AppTranslateService,
    private bsModalRef: BsModalRef,
    private modalService: BsModalService,
    private changepasswordService: ChangepasswordService,

  ) 
  
  {}

  
  ngOnInit(): void {
    const currentLang = localStorage.getItem('currentLang');
    this.defaultLang = ((currentLang == null || currentLang == 'undefined') ? environment.defaultLang : currentLang);

    const currentUser = (this.authService.getLoggedInUser()?.username);
    this.username = (currentUser == null ? '' : currentUser);
  }
  onLangChange(lang: any) {
    lang = lang.value;
    this.appTranslateService.changeLanguage(lang);
  }
  onLogout() {
    this.bsModalRef.hide();
    this.removeModalAttr();
    this.authService.logout();
  }



  removeModalAttr() {
    document.body.classList.remove('modal-open');
    document.body.removeAttribute('style');
  }

  onProfileModelClose() {
    this.bsModalRef.hide();
    this.removeModalAttr();
  }


  
  openProfileModal() {
    const config: ModalOptions = {
      backdrop: false,
      class: 'profile-modal border rounded-3'
    };
    const modalRef: BsModalRef = this.modalService.show(UserProfileDialogComponent, config);
  
}


  
  openPasswordModal() {
  
    const config: ModalOptions = {
      backdrop: false,
     class: 'profile-modal border rounded-3'
    };
    const modalRef: BsModalRef = this.modalService.show(ViewChangepasswordComponent, config);
  }
}

  // openPasswordModal() {
  //   const config: ModalOptions = {
  //     backdrop: false,
  //     class: 'password-modal border rounded-3'
  //   };
  //   const modalRef: BsModalRef = this.modalService.show(UserChangePasswordComponent, config);
  
