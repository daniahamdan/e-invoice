export class Monitor {
    id!: string;
    accountId!: string;
    traceId!: string;
    requestVerb!: string;
    requestPath!: string;
    requestTimestamp!: string;
    responseStatus!: string;
    ip!: string;
    requestTimeMilliseconds!: string;
    minRequestDuration!:string;
    type!: MonitorType;   
    monitorNumber!: string;
    typeString!: string;
    xml!: string;
    qrCode!: string;
    status!: MonitorStatus;
    statusString!: string;

}

export enum MonitorType
    {
        GeneralApi = 0,

        ZatcaApi = 1,

        InvoiceApi = 2,

        ZatcaSdkApi = 3,

    }

    export interface MonitorFilter {
    

        accountId?: string;
        monitorNumber?: number;
        sendDateFrom?: string;
        sendDateTo?: string;
        HttpVerb?: string;
        RequestPath?: string;
        responseStatus?: string;
        minRequestDuration?:string;
        type?: MonitorType;   
        PageSize?: number;
        PageNumber?: number;
        FromDate?: string;
        ToDate?: string;
    
      }


      export enum MonitorStatus {
        Active = 0,
      
        InActive = 1,
      }
