import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MonitorviewComponent } from './monitorview.component';

describe('MonitorviewComponent', () => {
  let component: MonitorviewComponent;
  let fixture: ComponentFixture<MonitorviewComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [MonitorviewComponent]
    });
    fixture = TestBed.createComponent(MonitorviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
