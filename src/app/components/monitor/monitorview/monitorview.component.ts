import { Component, Input } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { MonitorStatus } from '../models/monitor.model';

@Component({
  selector: 'app-monitorview',
  templateUrl: './monitorview.component.html',
  styleUrls: ['./monitorview.component.css']
})
export class MonitorviewComponent {
  statusKeys: string[] = [];
  status = MonitorStatus;
  currentData: any;
  

  @Input() data: any;
  constructor(public bsModalRef: BsModalRef) {
    this.statusKeys = Object.keys(this.status).filter((f) => !isNaN(Number(f)));

  }
  ngOnInit(): void {
    this.currentData = this.data;
    
  }


  
}
