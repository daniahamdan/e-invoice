import { DatePipe } from '@angular/common';
import { Component, EventEmitter, Output } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { ActivatedRoute, Params } from '@angular/router';
import { faCalendarDays, faSearch } from '@fortawesome/free-solid-svg-icons';``
import { BehaviorSubject } from 'rxjs';

import { MonitorFilter, MonitorType } from '../models/monitor.model';



@Component({
  selector: 'app-nav-monitor',
  templateUrl: './nav-monitor.component.html',
  styleUrls: ['./nav-monitor.component.css']
})
export class NavMonitorComponent {
  calendarIcon = faCalendarDays;
  searchIcon = faSearch;
  filterForm!: FormGroup;
  accountId!: string;
  merchantId!: string;
  responseStatus!: string;
  HttpVerb!: string;
  monitorTypeKeys: string[] = [];
  monitorType = MonitorType;
  fromDateString: string | null = null;
  toDateString: string | null = null;

  isCleared = new BehaviorSubject(false);
  $isCleared = this.isCleared.asObservable();

 

  @Output("filterMonitors") filterMonitors: EventEmitter<any> = new EventEmitter();
  translate: any;
  toastService: any;
  
responseStatuses = [
  {
    value: '400'
  
  },
  {
    value: '401'
  
  },
  {
    value: '500'
  
  },
  {
    value: '200'
  
  },
  {
    value: '202'
  
  },
  ];

  requestVerbs = [
    {
      value: 'Post'
    
    },
    {
      value: 'Get'
    
    },
    {
      value: 'Put'
    
    },
    {
      value: 'Delete'
    
    },
  
    ];
  

  

  constructor(private datePipe: DatePipe,
 
    private route: ActivatedRoute) {
      this.monitorTypeKeys = Object.keys(this.monitorType).filter(f => !isNaN(Number(f)));
  }
  ngAfterViewInit(): void {
    this.route.params.subscribe(
      (params: Params) => {
        if (this.accountId != params['accountId']) {
          this.accountId = params['accountId'];
        }
        if (this.merchantId != params['merchantId']) {
          this.merchantId = params['merchantId'];
          this.clearForm();
          this.isCleared.next(true);
        }
      }
    );
  }

  ngOnInit(): void {
    this.accountId = this.route.snapshot.params['accountId'];
    this.merchantId = this.route.snapshot.params['merchantId'];

    this.filterForm = new FormGroup({
      'type': new FormControl(),
      'HttpVerb': new FormControl(),
      'responseStatus': new FormControl(),
      'RequestPath': new FormControl(null), 
    });


   
  }


  onSave() {

  
    const type = this.filterForm.get('type')?.value;
    const responseStatus = this.filterForm.get('responseStatus')?.value;
    const HttpVerb = this.filterForm.get('HttpVerb')?.value;
    const data: MonitorFilter = { accountId: this.accountId};
   
  

    if (!!responseStatus) {
      data.responseStatus= responseStatus;
    }

   if (!!HttpVerb) {
      data.HttpVerb= HttpVerb;
    }

    if (!!this.fromDateString) {
      data.FromDate = this.fromDateString;
    }
    if (!!this.toDateString) {
      data.ToDate = this.toDateString;
    }
    if(!!type) {
      data.type = type;
    }
    this.filterMonitors.emit(data);
  }

  clearForm() {
    

    this.filterForm?.controls['type'].setValue(null);
    this.filterForm?.controls['HttpVerb'].setValue(null);
    this.filterForm?.controls['responseStatus'].setValue(null);
    this.filterForm?.controls['RequestPath'].setValue(null);
    this.fromDateString = null;
    this.toDateString = null;
  }



 

  onMonitorSearchbyRequestPath(keyElement: any) {
    let RequestPath = keyElement.target.value;
    if ( RequestPath.length < 3) {
      const  RequestPathLength = this.translate.instant(
        'general.RequestPathLength'
      );
      this.toastService.error(RequestPathLength);
      return;
    }
    this.filterMonitors.emit({ accountId: this.accountId,
      RequestPath:  RequestPath, 
      merchantId: this.merchantId
     });
  }
  
    emitClearFilter() {
      this.clearForm();
      this.isCleared.next(true);
      this.filterMonitors.emit({ accountId: this.accountId, merchantId: this.merchantId });
    }



    
    onDateChange($event: any) {
      if (!!$event)
        this.setRangeDate($event)
    }

    setRangeDate(date: string) {
      if (!!date) {
        const range = date.split(';');
        this.fromDateString = range[0];
        this.toDateString = range[1];
        this.onSave();
      }
    }
  
  }
