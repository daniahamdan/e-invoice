import { Injectable } from '@angular/core';
import { tap } from 'rxjs/internal/operators/tap';
import { HttpService } from 'src/app/services/http.service';

@Injectable({
  providedIn: 'root'
})
export class MonitorService {


  constructor(private httpService: HttpService) { }
  
  getMonitorList(accountId: string | null, data?: any) {
    const id = (accountId == null ? '' : accountId)

    const headers = { 'AccountId': id }
    return this.httpService.get("Monitor/List", data, headers).pipe(
      tap(response => {
      }))
  }


  getMonitor(id: string) {
    return this.httpService.getbyId("Monitor", id).pipe(
      tap(response => {
     }))
  }
  

}
