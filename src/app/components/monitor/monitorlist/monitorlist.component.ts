import { Component } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { faEllipsisV, faDownload, faFilePdf, faQrcode, faFile, faTrash } from '@fortawesome/free-solid-svg-icons';
import { BsModalRef, BsModalService, ModalOptions } from 'ngx-bootstrap/modal';
import { PageChangedEvent } from 'ngx-bootstrap/pagination';
import { Monitor, MonitorFilter,  MonitorStatus } from '../models/monitor.model';
import { MonitorType } from '../models/monitor.model';
import { MonitorviewComponent } from '../monitorview/monitorview.component';
import { MonitorService } from '../services/monitor.service';

@Component({
  selector: 'app-monitorlist',
  templateUrl: './monitorlist.component.html',
  styleUrls: ['./monitorlist.component.css']
})
export class MonitorlistComponent {
  actionIcon = faEllipsisV;
  
  deleteIcon = faTrash;
 
  monitors: Monitor[] = [];
  isUpdate: boolean = false;
  itemCount: number = 0;
  addsuccessMessage!: string;
  currentFilter: MonitorFilter | null = null;
  paginatorPageSize: number = 10;
  paginatorPageNumber: number = 1;
  accountId: string | null = '';
  merchantId: string | null = '';
  requestVerb!: string;
  getScreenWidth!: number;
  tableMaxSize: number=10;
  getScreenHeight!: number;


  constructor(
    private monitorService: MonitorService,
    public router: Router,
    public route: ActivatedRoute,
    private modalService: BsModalService
  ) { }
  ngOnInit(): void {
    this.accountId = this.route.snapshot.params['accountId'];
    this.merchantId = this.route.snapshot.params['merchantId'];
    this.getScreenWidth = window.innerWidth;
    this.getScreenHeight = window.innerHeight;
    if(this.getScreenWidth < 1000){
      this.tableMaxSize = 3;
    }

  
  
  }

  ngAfterViewInit() {
    this.route.params.subscribe((params: Params) => {
      if (this.accountId != params['accountId']) {
        this.accountId = params['accountId'];
      }
      if (this.merchantId != params['merchantId']) {
        this.merchantId = params['merchantId'];
      }
      this.getMonitorList();
    });
  }


  getMonitorList(monitorData?: any) {
     const data = this.setListRequestData(monitorData);
    this.monitorService
      .getMonitorList(this.accountId, data)
      .subscribe((monitorData: any) => {
        this.monitors = monitorData.data;
        this.itemCount = monitorData.totalCount;
        console.log(this.monitors);
        this.monitors.map((i) => {
          i.typeString = MonitorType[i.type];
          i.statusString = MonitorStatus[i.status];
        });
      });
  } 

  reloadMonitorTable(filterData?: any) {
    this.getMonitorList(filterData);
  }
  setListRequestData(monitorData?: MonitorFilter | null) {
    if (!!monitorData) {
      monitorData.PageSize = this.paginatorPageSize;
      monitorData.PageNumber = this.paginatorPageNumber;
      return monitorData;
    } else {
      
      return {
        AccountId: this.accountId,
        MerchantId: this.merchantId,
        PageSize: this.paginatorPageSize,
        PageNumber: this.paginatorPageNumber,
      
      };
    }
  }

  applyMonitorFilter(filterData?: any) {
    this.paginatorPageNumber = 1;
    this.paginatorPageSize = 10;
    this.currentFilter = filterData;
    this.getMonitorList(filterData);
  }

  preventCellClick($event: any) {
    $event.stopPropagation();
  }

  pageChanged(event: PageChangedEvent): void {
    this.paginatorPageNumber = event.page;
    this.paginatorPageSize = event.itemsPerPage;
    this.reloadMonitorTable(this.currentFilter);
  }

  viewMonitor(rowData: any) {
    this.monitorService.getMonitor(rowData.id).subscribe((response: any) => {
      const config: ModalOptions = {
        backdrop: true,
        class: 'modal-xl modal-dialog-centered',
        initialState: { data: response },
      };
      const modalRef: BsModalRef = this.modalService.show(
        MonitorviewComponent,
        config
      );
    });
  }







}
