import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MonitorlistComponent } from './monitorlist.component';

describe('MonitorlistComponent', () => {
  let component: MonitorlistComponent;
  let fixture: ComponentFixture<MonitorlistComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [MonitorlistComponent]
    });
    fixture = TestBed.createComponent(MonitorlistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
