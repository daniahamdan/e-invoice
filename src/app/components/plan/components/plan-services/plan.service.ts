import { HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, tap } from 'rxjs';
import { HttpService } from 'src/app/services/http.service';

@Injectable({
  providedIn: 'root'
})
export class PlanService {

  



  constructor(private httpService: HttpService) { }


  
  postPlan(accountId: string|null, data: any) {
    const id = (accountId == null ? '' : accountId)

    const headers = { 'AccountId': id }
    return this.httpService.post("Plan", data, headers).pipe(
      tap(() => {
       
      }));
  }
  getPlan(accountId: string|null, id: string) {
    const accId = (accountId == null ? '' : accountId)

    const headers = { 'AccountId': accId }
    return this.httpService.getbyId("Plan", id, headers).pipe(
      tap(response => {
      }))
  }
  updatePlan(id: string, data: any) {
    return this.httpService.update("Plan", id, data).pipe(
      tap(response => {
        
      }));
  }
  deletePlan(id: string) {
    const param = new HttpParams()
      .set('id', id);
    return this.httpService.delete("Plan", param).pipe(
      tap(response => {
        
      }));
  }

getPlanList(accountId: string|null, data?: any) {
  const id = (accountId == null ? '' : accountId)

    const headers = { 'AccountId': id }
    return this.httpService.get("Plan/List", data, headers).pipe(
      tap(response => { 
      }))
  }

  //move to plan service 
  getPlans(accountId: string|null, data?: any){
    const id = (accountId == null ? '' : accountId)

    const headers = { 'AccountId': id }
    return this.httpService.get("Plan/List", data, headers).pipe(
      tap(response => { 
      }))
  }
}


