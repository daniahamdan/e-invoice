export class Plan {

 
    id!: string;
    accountId!: string;
    expiryDate!: Date;
    price!: Float32Array;
    numberOfUser!: number;
    numberOfInvoiceB2C!: number;
    numberOfInvoiceB2B!: number;
    enableIntegrationAPI!: true;
    numberOfSubMerchant!: number;
    numberOfBranch!: number;
    numberOfTerminal!: number;
    status!: PlanStatus ;
    isManipulated!: true;
    name!: string;
  statusString: any;
   
}


export class PlanCreateUpdate {

 
  id!: string;
  accountId!: string;
  expiryDate!: Date;
  price!: Float32Array;
  numberOfUser!: number;
  numberOfInvoiceB2C!: number;
  numberOfInvoiceB2B!: number;
  enableIntegrationAPI!: true;
  numberOfSubMerchant!: number;
  numberOfBranch!: number;
  numberOfTerminal!: number;
  status!: PlanStatus ;
  isManipulated!: true;
  name!: string;
  statusString: any;

}


export interface PlanFilter {
    accountId?: string;
    pageSize?: number;
    pageNumber?: number;
    searchPattern?: string;
    status?: PlanStatus;
  }


  export enum PlanStatus {
    Active = 0,
  
    InActive = 1,
  }