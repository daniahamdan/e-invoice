import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NavPlanComponent } from './nav-plan.component';

describe('NavPlanComponent', () => {
  let component: NavPlanComponent;
  let fixture: ComponentFixture<NavPlanComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [NavPlanComponent]
    });
    fixture = TestBed.createComponent(NavPlanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
