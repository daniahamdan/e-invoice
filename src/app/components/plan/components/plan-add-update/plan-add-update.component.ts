import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { PlanCreateUpdate, PlanStatus } from '../models/plan.model';
import { PlanService } from '../plan-services/plan.service';

@Component({
  selector: 'app-plan-add-update',
  templateUrl: './plan-add-update.component.html',
  styleUrls: ['./plan-add-update.component.css']
})
export class PlanAddUpdateComponent {

  planForm!: FormGroup;

  statusKeys: string[] = [];
  status = PlanStatus;

  accountId!: string;
  submitted: boolean = false;

  planEditId: string = '';
  isEditView: boolean = false;

  

  constructor(private planService: PlanService,
    private route: ActivatedRoute,
    private router: Router
  ) {
    this.statusKeys = Object.keys(this.status).filter(f => !isNaN(Number(f)));
  }

  ngOnInit(): void {
    this.accountId = this.route.snapshot.params['accountId'];
    this.planEditId = this.route.snapshot.params['editId'];
    const isEdit = this.route.snapshot.params['isEdit'];

    this.isEditView = (isEdit === 'true' ? true : this.isEditView);
    if (this.isEditView) {
      this.getPlanById(this.planEditId);
    }

    this.planForm = new FormGroup({
   
      'name': new FormControl(null, [Validators.required, Validators.minLength(3), Validators.maxLength(255)]),
      'expiryDate': new FormControl(null, [Validators.required, Validators.minLength(3), Validators.maxLength(255)]),
     // 'nameEn': new FormControl(null, [Validators.required, Validators.minLength(5), Validators.maxLength(255)]),
      'price': new FormControl(null, [Validators.required]),
      'numberOfUser': new FormControl(null, [Validators.required, Validators.minLength(10), Validators.maxLength(32), Validators.pattern("^[0-9]*$")]),
      'numberOfInvoiceB2C': new FormControl(null, [Validators.required, Validators.minLength(10), Validators.maxLength(32), Validators.pattern("^[0-9]*$")]),
      'numberOfInvoiceB2B': new FormControl(null, [Validators.required, Validators.minLength(10), Validators.maxLength(32), Validators.pattern("^[0-9]*$")]),
      'numberOfSubMerchant': new FormControl(null, [Validators.required, Validators.minLength(10), Validators.maxLength(32), Validators.pattern("^[0-9]*$")]),
      'numberOfBranch': new FormControl(null, [Validators.required, Validators.minLength(10), Validators.maxLength(32), Validators.pattern("^[0-9]*$")]),
      'numberOfTerminal': new FormControl(null, [Validators.required, Validators.minLength(10), Validators.maxLength(32), Validators.pattern("^[0-9]*$")]),
     
      'status': new FormControl(null),

      
    });

  
  }
  getPlanById(id: string) {
    this.planService
      .getPlan(this.accountId, id)
      .subscribe(response => {
        this.fillPlanForm(response);
      });
  }
  fillPlanForm(currentData: any) {
    this.planForm.controls['name'].setValue(currentData.name);
    this.planForm.controls['expiryDate'].setValue(currentData.expiryDate);
    this.planForm.controls['price'].setValue(currentData.price);
    this.planForm.controls['numberOfUser'].setValue(currentData.numberOfUser);
    this.planForm.controls['status'].setValue(currentData.status);
    this.planForm.controls['numberOfInvoiceB2C'].setValue(currentData.numberOfInvoiceB2C);
    this.planForm.controls['numberOfInvoiceB2B'].setValue(currentData.numberOfInvoiceB2B);
    this.planForm.controls['numberOfSubMerchant'].setValue(currentData.numberOfSubMerchant);
    this.planForm.controls['numberOfBranch'].setValue(currentData.numberOfBranch);
    this.planForm.controls['numberOfTerminal'].setValue(currentData.numberOfTerminal);
  }
  onSave() {
    debugger
    this.submitted = true;
    if (!this.planForm.valid) {
      this.planForm.markAllAsTouched();
      return;
    }
    const status = +this.planForm?.value.status;
    var data: PlanCreateUpdate = this.planForm.getRawValue();
//    data.logo = "logo"
    if (this.isEditView) {
      data.status = status;
      data.accountId=this.accountId;
      this.updatePlan(data);
    }
    else {
      data.status = PlanStatus.Active;
      data.accountId=this.accountId;
      this.addPlan(data);
    }
  }

  addPlan(data: any) {
    this.planService
      .postPlan(this.accountId, data)
      .subscribe(response => {
        if (!!response) {
          this.router.navigate(['plan/plan-list', this.accountId]);
        }
      });
  }
  updatePlan(data: any) {
    this.planService.updatePlan(this.planEditId, data)
      .subscribe(response => {
        if (!!response) {
          this.router.navigate(['plan/plan-list', this.accountId]);
        }
      });
  }




}
