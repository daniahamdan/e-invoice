import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PlanAddUpdateComponent } from './plan-add-update.component';

describe('PlanAddUpdateComponent', () => {
  let component: PlanAddUpdateComponent;
  let fixture: ComponentFixture<PlanAddUpdateComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [PlanAddUpdateComponent]
    });
    fixture = TestBed.createComponent(PlanAddUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
