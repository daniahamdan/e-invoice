import { Component, Input } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { PlanStatus } from '../models/plan.model';

@Component({
  selector: 'app-plan-view',
  templateUrl: './plan-view.component.html',
  styleUrls: ['./plan-view.component.css']
})
export class PlanViewComponent {
  statusKeys: string[] = [];
  status = PlanStatus;
  currentData: any;

  @Input() data: any;
  constructor(public bsModalRef: BsModalRef) {
    this.statusKeys = Object.keys(this.status).filter((f) => !isNaN(Number(f)));
  }
  ngOnInit(): void {
    this.currentData = this.data;
  }
}
