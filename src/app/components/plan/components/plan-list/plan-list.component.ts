import { Component } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { faEllipsisV, faRotate, faTrash } from '@fortawesome/free-solid-svg-icons';
import { TranslateService } from '@ngx-translate/core';
import { BsModalRef, BsModalService, ModalOptions } from 'ngx-bootstrap/modal';
import { PageChangedEvent } from 'ngx-bootstrap/pagination';
import { ToastService } from 'src/app/services/toast-service/toast.service';
import { ConfirmationDialogComponent } from 'src/app/shared/components/confirmation-dialog/confirmation-dialog.component';
import { Plan, PlanFilter, PlanStatus } from '../models/plan.model';
import { PlanService } from '../plan-services/plan.service';
import { PlanViewComponent } from '../plan-view/plan-view.component';

@Component({
  selector: 'app-plan-list',
  templateUrl: './plan-list.component.html',
  styleUrls: ['./plan-list.component.css']
})
export class PlanListComponent {

  actionIcon = faEllipsisV;
  updateIcon = faRotate;
  deleteIcon = faTrash;
  plans: Plan[] = [];
  isUpdate: boolean = false;
  itemCount: number = 0;
  addsuccessMessage!: string;

  currentFilter: PlanFilter | null = null;

  paginatorPageSize: number = 10;
  paginatorPageNumber: number = 1;

  accountId: string | null = '';
  getScreenHeight!: number;
  getScreenWidth!: number;
  tableMaxSize: number=10;

  constructor(
    private planService: PlanService,
    public router: Router,
    public route: ActivatedRoute,
    private modalService: BsModalService,
    private toastService: ToastService,
    private translate: TranslateService
  ) {}
  ngOnInit(): void {
    this.accountId = this.route.snapshot.params['accountId'];
    this.getScreenWidth = window.innerWidth;
    this.getScreenHeight = window.innerHeight;
    if(this.getScreenWidth < 1000){
      this.tableMaxSize = 3;
    }

  }

  ngAfterViewInit() {
    this.route.params.subscribe((params: Params) => {
      if (this.accountId != params['accountId']) {
        this.accountId = params['accountId'];
      }
      this.getPlanList();
    });
  }

  getPlan(id: string) {
    this.planService.getPlan(this.accountId,id).subscribe((response: any) => {
      return response;
    });
  }

  openDeleteConfirmationModal(id: string): void {
    const config: ModalOptions = { class: 'modal-sm modal-dialog-centered' };
    const modalRef: BsModalRef = this.modalService.show(
      ConfirmationDialogComponent,
      config
    );
    modalRef.content.deleteConfirmed.subscribe(() => {
      this.confirmDelete(id);
    });
  }
  confirmDelete(id: string) {
    this.planService.deletePlan(id).subscribe(() => {
      const deletedSuccessMessage = this.translate.instant(
        'general.deletedSuccessMessage',
        { componentName: 'plan' }
      );
      this.toastService.success(deletedSuccessMessage);
      this.reloadPlanTable(this.currentFilter);
    });
  }
  updatePlan(id: string) {
    this.router.navigate([
      '/plan/plan-add'
      , this.accountId
      , { isEdit: true, editId: id },
    ]);
    this.isUpdate = true;
  }
  getPlanList(planData?: any) {
    const data = this.setListRequestData(planData);
    this.planService.getPlanList(this.accountId, data).subscribe((planData) => {
      this.plans = planData.data;
      this.itemCount = planData.totalCount;
      this.plans.map((i) => {
        i.statusString = PlanStatus[i.status];
      });
    });
  }
  reloadPlanTable(filterData?: any) {
    this.getPlanList(filterData);
  }
  setListRequestData(planData?: PlanFilter | null) {
    if (!!planData) {
      planData.pageSize = this.paginatorPageSize;
      planData.pageNumber = this.paginatorPageNumber;
      return planData;
    } else {
      return {
        PageSize: this.paginatorPageSize,
        PageNumber: this.paginatorPageNumber,
      };
    }
  }

  applyPlanFilter(filterData?: any) {
    this.paginatorPageNumber = 1;
    this.paginatorPageSize = 10;
    this.currentFilter = filterData;
    this.getPlanList(filterData);
  }

  viewPlan(rowData: any) {
    this.planService.getPlan(this.accountId, rowData.id).subscribe((response: any) => {
      const config: ModalOptions = {
        backdrop: true,
        class: 'modal-xl modal-dialog-centered',
        initialState: { data: response },
      };
      const modalRef: BsModalRef = this.modalService.show(
        PlanViewComponent,
        config
      );
    });
  }
  preventCellClick($event: any) {
    $event.stopPropagation();
  }

  pageChanged(event: PageChangedEvent): void {
    this.paginatorPageNumber = event.page;
    this.paginatorPageSize = event.itemsPerPage;
    this.reloadPlanTable(this.currentFilter);
  }
}
