import { Component, Input } from '@angular/core';


import { BsModalRef } from 'ngx-bootstrap/modal';
import { UserStatus } from '../models/user.model';

@Component({
  selector: 'app-user-view',
  templateUrl: './user-view.component.html',
  styleUrls: ['./user-view.component.css']
})
export class UserViewComponent {
  statusKeys: string[] = [];
  status = UserStatus;
  currentData: any;

  

  @Input() data: any;
  constructor(public bsModalRef: BsModalRef) {
    this.statusKeys = Object.keys(this.status).filter((f) => !isNaN(Number(f)));
  }
  ngOnInit(): void {
    this.currentData = this.data;
    console.log('hi');
  }
}
