export class UserModel {
  id!: string;
  merchantId!: string;
  branchId!: string;
  nameEn!: string;
  nameAr!: string;
  username!: string;
  email!: string;
  status!: UserStatus;
  type !: UserType;
  typeKeys!: number;
  typeString!: string;
  statusString!: string;
}

export interface UserCreateUpdate {
  accountId: string;
  id: string;
  merchantId: string;
  branchId: string;
  nameAr: string;
  nameEn: string;
  username: string;
  email: string;
  status: UserStatus;
  type: UserType;
  typeKeys: number;

}

export interface UserFilter {
  type?: UserType;
  merchantId?: string;
  branchId?: string;
  pageSize?: number;
  pageNumber?: number;
  searchPattern?: string;
  status?: UserStatus;
}

export enum UserStatus {
  Active = 0,

  InActive = 1,

  //Deleted = 2,
}


export enum UserType {
  SystemAdmin = 1,
  AccountAdmin = 2,
  MerchantAdmin = 3,
  BranchAdmin = 4,
  
}


