import { Component, EventEmitter, Output } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { ActivatedRoute, Params } from '@angular/router';
import { faSearch } from '@fortawesome/free-solid-svg-icons';
import { TranslateService } from '@ngx-translate/core';
import { AuthService } from 'src/app/components/auth/auth.service';
import { ToastService } from 'src/app/services/toast-service/toast.service';
import { UserFilter, UserStatus } from '../models/user.model';


@Component({
  selector: 'app-nav-user',
  templateUrl: './nav-user.component.html',
  styleUrls: ['./nav-user.component.css']
})
export class NavUserComponent {
  searchIcon = faSearch;

  statusKeys: string[] = [];
  status = UserStatus;

  filterForm!: FormGroup;

  accountId!: string
  selectedStatus!: number | null;
  merchantId: any;
  branchId: any;
  type: any;
  constructor(
    private toastService: ToastService,
    private authService: AuthService,
    private translate: TranslateService,
 

  private route: ActivatedRoute
  ) {
    this.statusKeys = Object.keys(this.status).filter((f) => !isNaN(Number(f)));}

  ngAfterViewInit(): void {
    this.route.params.subscribe(
      (params: Params) => {
        if (this.merchantId != params['merchantId']) {
          this.merchantId = params['merchantId'];
       
        }

        if (this.accountId != params['accountId']) {
          this.accountId = params['accountId'];
        }

        this.clearForm()
      }
      
    );
  } 

  ngOnInit(): void {
    
    
    this.accountId = this.route.snapshot.params['accountId'];
    this.merchantId = this.route.snapshot.params['merchantId'];

    this.filterForm = new FormGroup({
      'searchPattern': new FormControl(null),
      'status': new FormControl(null),
    });
  }

  @Output("filterUsers") filterUsers: EventEmitter<any> = new EventEmitter();

  onUserSearch(keyElement: any) {
    let searchPattern = keyElement.target.value;
    if (searchPattern.length < 3) {
      const searchPatternLength = this.translate
        .instant('general.searchPatternLength');
      this.toastService.error(searchPatternLength);
      return;
    }
    this.filterUsers.emit({ 
      SearchPattern: searchPattern, 
      MerchantId: this.merchantId
     });
  }
  onStatusChange(status: number) {
    this.selectedStatus = status;
    const searchPattern = this.filterForm?.value.searchPattern
    const data: UserFilter = {
      status: status,
      merchantId: this.merchantId,
      type:this.type
    }
    if (!!searchPattern) {
      data.searchPattern = searchPattern;
    }
    this.filterUsers.emit(data)
  }

  emitClearForm() {
    this.clearForm();
    this.filterUsers.emit();
  }
  clearForm() {
    this.selectedStatus = null;
    this.filterForm?.controls['searchPattern'].setValue('');
    this.filterForm?.controls['status'].setValue(null);
    
  }

}
