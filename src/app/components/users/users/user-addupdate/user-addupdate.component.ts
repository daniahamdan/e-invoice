import { Component } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { map, Observable, of } from 'rxjs';
import { AuthService } from 'src/app/components/auth/auth.service';
import { UserRole } from 'src/app/components/auth/models/user.model';
import { BranchService } from 'src/app/components/branch/components/branch.service';
import { IdName } from 'src/app/shared/interfaces/idName.interface';
import { UserCreateUpdate, UserStatus, UserType } from '../models/user.model';
import { UserService } from '../user-service/user.service';

@Component({
  selector: 'app-user-addupdate',
  templateUrl: './user-addupdate.component.html',
  styleUrls: ['./user-addupdate.component.css']
})
export class UserAddupdateComponent {
  [x: string]: any;
  userForm!: FormGroup;
  statusKeys: string[] = [];
  status = UserStatus;
  typeKeys: string[];
  type = UserType;
  submitted: boolean = false;
  branchId!: string;
  merchantId!: string;
  userEditId: string = '';
  isEditView: boolean = false;
  isradioValue: boolean = false;
  isAccountAdmin: boolean = false;
  isSystemAdmin: boolean = false;

  branches!: Observable<IdName[]>;
  selectedBranch: string = '';
  accountId!: string;
  newbranchId!: string;
  userRole!: UserRole;
  isHidden: boolean = true;
  isEditedUserBranchAdmin: boolean = false;

  // Event.target: EventTarget | null;

  constructor(private userService: UserService, private branchService: BranchService,
    private route: ActivatedRoute,
    private router: Router,
    private auth: AuthService,
    private authService: AuthService,
  ) {

    this.userRole = auth.getUserRole();
    this.statusKeys = Object.keys(this.status).filter(f => !isNaN(Number(f)));
    this.typeKeys = Object.keys(this.type).filter(f => !isNaN(Number(f)) && Number(f) >= this.userRole);

    //this['radioValue'] = Number(this.userForm.controls['type'].getRawValue());

  }





  ngOnInit(): void {
    this.isSystemAdmin = (this.authService.getLoggedInUser()?.userRole == 1);

    this.merchantId = this.route.snapshot.params['merchantId'];
    this.accountId = this.route.snapshot.params['accountId'];
    this.branchId = this.route.snapshot.params['branchId'];
    this.userEditId = this.route.snapshot.params['editId'];
    const isEdit = this.route.snapshot.params['isEdit'];
    this.isEditView = (isEdit === 'true' ? true : this.isEditView);
    // this.isradioValue=(this.isHidden==false ? true : this.isradioValue )
    // this.isradioValue=( this['radioValue'] ==4 ? true : this.isradioValue )
    //  this.isradioValue=( this.userRole==1 ? true : this.isradioValue )

    if (this.isEditView) {

      this.getUserById(this.userEditId);
    }


    this.userForm = new FormGroup({
      'branchId': new FormControl(null),
      'nameAr': new FormControl(null, [Validators.required, Validators.minLength(3), Validators.maxLength(255)]),
      'nameEn': new FormControl(null, [Validators.required, Validators.minLength(3), Validators.maxLength(255)]),
      'password': new FormControl(null, [(this.isEditView ? Validators.nullValidator : Validators.required), Validators.minLength(8), Validators.maxLength(255), Validators.pattern('^(?=(.*\\d){1})(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z\\d]).{8,}$')]),
      'status': new FormControl(null),
      'type': new FormControl(null),
      'email': new FormControl('', [
        Validators.required,
        Validators.email
      ]),
      'username': new FormControl(null, [(this.isEditView ? Validators.nullValidator : Validators.required), Validators.minLength(4), Validators.maxLength(20), Validators.pattern('^(?=.{4,20}$)(?![_.])(?!.*[_.]{2})[a-zA-Z0-9._]+(?<![_.])$')]),
    });

    this.getBranches();
  }


  getUserById(id: string) {
    this.userService
      .getUser(id)
      .subscribe(response => {
        this.fillUserForm(response);

      });
  }




  fillUserForm(currentData: any) {
    this.userForm.controls['nameAr'].setValue(currentData.nameAr);
    this.userForm.controls['nameEn'].setValue(currentData.nameEn);
    this.userForm.controls['email'].setValue(currentData.email);
    this.userForm.controls['password'].setValue(currentData.password);
    this.userForm.controls['status'].setValue(currentData.status);
    this.userForm.controls['username'].setValue(currentData.username);
    this.userForm.controls['branchId'].setValue(currentData.branchId);
    this.isEditedUserBranchAdmin = currentData.type == UserType.BranchAdmin;
  }






  radioButtonChanged() {

    const radioValue = Number(this.userForm.controls['type'].getRawValue());
    if (radioValue == 4) {
      this.userForm.get('branchId')?.setValidators(Validators.required)
      this.isHidden = false;
    }
    else {
      this.userForm.controls['branchId'].setValue(null);
      this.userForm.get('branchId')?.clearValidators()
      this.userForm.get('branchId')?.updateValueAndValidity()
      this.isHidden = true;
    }
  }



  radioButtonChanged1() {
    const radioValue = Number(this.userForm.controls['type'].getRawValue());
    if (radioValue == 4) {
      this.userForm.get('branchId')?.setValidators(Validators.required)
      this.isHidden = false;
    } else {
      this.userForm.controls['branchId'].setValue(null);
      this.userForm.get('branchId')?.clearValidators()
      this.userForm.get('branchId')?.updateValueAndValidity()
      this.isHidden = true;
    }
  }


  onSave() {

    this.submitted = true;

    if (!this.userForm.valid) {
      this.userForm.markAllAsTouched();
      return;
    }
    const status = +this.userForm?.value.status;
    const type = +this.userForm?.value.type;
    var data: UserCreateUpdate = this.userForm.getRawValue();

    if (this.isEditView) {
      //  this.userRole= this.userRole
      data.status = status;
      data.type = type;
      data.accountId = this.accountId
      data.merchantId = this.merchantId;

      this.updateUser(data);
    }
    else {
      data.status = UserStatus.Active;
      data.type = type;
      data.accountId = this.accountId;
      data.merchantId = this.merchantId;
      //this.userRole= this.userRole
      this.addUser(data);
    }








  }

  addUser(data: any) {
    this.userService
      .postUser(this.accountId, data)
      .subscribe(response => {
        if (!!response) {
          this.router.navigate(['user/user-list', this.accountId, this.merchantId]);
        }
      });
  }

  updateUser(data: any) {
    this.userService.updateUser(this.accountId, this.userEditId, data)
      .subscribe((response: any) => {

        if (!!response) {

          this.router.navigate(['user/user-list', this.accountId, this.merchantId]);
        }
      });
  }

  getBranches() {
    const data = {
      MerchantId: this.merchantId,
    }
    this.branchService.getBranchList(this.accountId, data)
      .subscribe((response: { data: any[]; }) => {
        if (!!response) {
          this.branches = of(response.data.map((item: any) => ({
            id: item.nameEn,
            name: item.id,
          }))).pipe();
        }
      });
  }
}
