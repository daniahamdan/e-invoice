import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UserAddupdateComponent } from './user-addupdate.component';

describe('UserAddupdateComponent', () => {
  let component: UserAddupdateComponent;
  let fixture: ComponentFixture<UserAddupdateComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [UserAddupdateComponent]
    });
    fixture = TestBed.createComponent(UserAddupdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
