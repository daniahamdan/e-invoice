import { Component } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { faEllipsisV, faRotate, faTrash } from '@fortawesome/free-solid-svg-icons';
import { TranslateService } from '@ngx-translate/core';
import { BsModalService, ModalOptions, BsModalRef } from 'ngx-bootstrap/modal';
import { PageChangedEvent } from 'ngx-bootstrap/pagination';
import { UserModel, UserFilter, UserStatus } from '../models/user.model';
import { UserService } from '../user-service/user.service';
import { UserViewComponent } from '../user-view/user-view.component';
import { ToastService } from 'src/app/services/toast-service/toast.service';
import { ConfirmationDialogComponent } from 'src/app/shared/components/confirmation-dialog/confirmation-dialog.component';
import { UserType } from 'src/app/components/branch/components/models/branch.model';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent {
  actionIcon = faEllipsisV;
  updateIcon = faRotate;
  deleteIcon = faTrash;
  users: UserModel[] = [];
  isUpdate: boolean = false;
  itemCount: number = 0;
  addsuccessMessage!: string;
  status: string | undefined
  currentFilter: UserFilter | null = null;

  paginatorPageSize: number = 10;
  paginatorPageNumber: number = 1;

  accountId!: string;
  branchId: string | undefined;
  merchantId!: string;
  terminalService: any;
  searchPattern!: string;
  pagination!: number;
  getScreenWidth!: number;
  tableMaxSize: number=10;
  getScreenHeight!: number;
  constructor(
    public router: Router,
    public userService: UserService,
    public route: ActivatedRoute,
    private modalService: BsModalService,
    private toastService: ToastService,
    private translate: TranslateService
  ) { }
  ngOnInit(): void {
    
    this.merchantId = this.route.snapshot.params['merchantId'];
    this.accountId = this.route.snapshot.params['accountId'];
    this.getScreenWidth = window.innerWidth;
    this.getScreenHeight = window.innerHeight;
    if(this.getScreenWidth < 1000){
      this.tableMaxSize = 3;
    }
 
    
    

  }

  ngAfterViewInit() {
    this.route.params.subscribe((params: Params) => {
      if (this.merchantId != params['merchantId']) {
        this.merchantId = params['merchantId'];
      }
      if (this.accountId != params['accountId']) {
        this.accountId = params['accountId'];
      }
      this.getUserList();
    });
   //  this.getScreenWidth = window.innerWidth;
  //this.getScreenHeight = window.innerHeight;
  }

  getUser(id: string) {
    this.userService.getUser(id).subscribe((response: any) => {
      return response;
    });
  }

  openDeleteConfirmationModal(id: string): void {
    const config: ModalOptions = { class: 'modal-sm modal-dialog-centered' };
    const modalRef: BsModalRef = this.modalService.show(
      ConfirmationDialogComponent,
      config
    );
    modalRef.content.deleteConfirmed.subscribe(() => {
      this.confirmDelete(id);
    });
  }
  confirmDelete(id: string) {
    this.userService.deleteUser(id).subscribe((res: any) => {
      const deletedSuccessMessage = this.translate.instant(
        'general.deletedSuccessMessage',
        { componentName: 'user' }
      );
      this.toastService.success(deletedSuccessMessage);
      this.reloadUserTable(this.currentFilter);
    });
  }

  updateUser(id: string) {
    this.router.navigate([
      '/user/user-add'
      , this.accountId, this.merchantId
      , { isEdit: true, editId: id },
    ]);
    this.isUpdate = true;
  }
  getUserList(userData?: any) {
    const data = this.setListRequestData(userData);
    this.userService.getUserList(this.accountId, data).subscribe((userData) => {

      this.userService
        .getUserList(this.accountId, data)
        .subscribe((userData) => {
          this.users = userData.data;
          this.itemCount = userData.totalCount;
          this.users.map((i) => {
            i.statusString = UserStatus[i.status];
            i.typeString = UserType[i.type];
          });

        });
    });
  }


  reloadUserTable(filterData?: any) {
    this.getUserList(filterData);
  }

  setListRequestData(userData?: UserFilter | null) {
    if (!!userData) {
      userData.pageSize = this.paginatorPageSize;
      this.searchPattern = "";
      userData.searchPattern = this.searchPattern;

      userData.merchantId = this.merchantId;
      userData.pageNumber = this.paginatorPageNumber;
      return userData;
    } else {
      this.searchPattern = "";
      return {
        PageSize: this.paginatorPageSize,
        PageNumber: this.paginatorPageNumber,
        SearchPattern: this.searchPattern,
        MerchantId: this.merchantId

      };
    }
  }

  applyUserFilter(filterData?: any) {
    this.paginatorPageNumber = 1;
    this.paginatorPageSize = 10;
    this.pagination=360;
    this.currentFilter = filterData;
    this.getUserList(filterData);
  }

  viewUser(rowData: any) {
    this.userService.getUser(rowData.id).subscribe((response: any) => {
      const config: ModalOptions = {
        backdrop: true,
        class: 'modal-xl modal-dialog-centered',
        initialState: { data: response },
      };
      const modalRef: BsModalRef = this.modalService.show(
        UserViewComponent,
        config
      );
    });
  }

  
  preventCellClick($event: any) {
    $event.stopPropagation();
  }

  pageChanged(event: PageChangedEvent): void {
    this.paginatorPageNumber = event.page;
    this.paginatorPageSize = event.itemsPerPage;
    this.reloadUserTable(this.currentFilter);
  }




}
