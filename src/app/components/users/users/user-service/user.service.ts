import { HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { tap } from 'rxjs';
import { HttpService } from 'src/app/services/http.service';
import { IdName } from 'src/app/shared/interfaces/idName.interface';

@Injectable({
  providedIn: 'root'
})
export class UserService {


  branchService: any;


  router: any;
  merchantId: any;
  
  branches: IdName[] = [];

  selectedBranch: string = '';
  accountId: string | null = '';
  

 
  constructor(private httpService: HttpService) { }
 
  
  postUser(accountId: string|null, data: any) {
    const id = (accountId == null ? '' : accountId)

    const headers = { 'AccountId': id }
    return this.httpService.post("User", data, headers).pipe(
      tap((response: any) => {
     
      }));
  }
  getUser(id: string) {
    return this.httpService.getbyId("User", id).pipe(
      tap((response: any) => {
      
      }))
  }
  

  updateUser(accountId: string|null, id: string, data: any) {
    const accId = (accountId == null ? '' : accountId)

    const headers = { 'AccountId': accId }
    return this.httpService.update("User", id, data, headers).pipe(
      tap((response: any) => {
        
      }));






  }
  deleteUser(id: string) {
    const param = new HttpParams()
      .set('id', id);
    return this.httpService.delete("User", param).pipe(
      tap((response: any) => {
      
      }));
  }

getUserList(accountId: string|null, data?: any) {
  const id = (accountId == null ? '' : accountId)

    const headers = { 'AccountId': id }
    return this.httpService.get("User/List", data, headers).pipe(
      tap((response: any) => { 
      }))
  }

}
