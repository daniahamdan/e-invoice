import { Component, OnInit } from "@angular/core";
import { faAngleDown } from "@fortawesome/free-solid-svg-icons";
import { environment } from "src/environments/environment";
import { ActivatedRoute } from "@angular/router";
import { BsModalRef, BsModalService, ModalOptions } from "ngx-bootstrap/modal";
import { UserProfileDialogComponent } from "../user/components/dialogs/user-profile-dialog/user-profile-dialog.component";
import { AuthService } from "../auth/auth.service";
import { AccountService } from "../account/account.service";
import { ChangeAccountDialogComponent } from "../account/components/dialogs/change-account-dialog/change-account-dialog.component";
import { IdName } from "src/app/shared/interfaces/idName.interface";
import { MerchantService } from "../merchant/merchant.service";
import { Login } from "../auth/models/login.model";

import { FormControl, FormGroup } from "@angular/forms";
import { ChangepasswordService } from "../changepassword/services/changepassword.service";


@Component({
  selector: 'app-nav-menu',
  templateUrl: './nav-menu.component.html',
  styleUrls: ['./nav-menu.component.css']
})
export class NavMenuComponent implements OnInit {

  downArrowIcon = faAngleDown;
  brandName = environment.brandName;
  accountName: string = '';
  merchantName: string = '';

  accountId: string | null = null;
  merchantId: string | null = null;
  currentAccountIdParam!: string;
  isSystemAdmin: boolean = false;
  isAccountAdmin: boolean = false;
  isMerchantAdmin: boolean = false;
  isBranchAdmin: boolean = false;
  selectedAccount!: string;
  submitted!: boolean;
  oldPassword!: FormControl;
  newPassword!: FormControl;
  cnewPassword!: FormControl;
  UpdatePasswordForm!: FormGroup;
  defaultLang: any;
  loginForm!: FormGroup;
  error: any;

  

  constructor(
    private route: ActivatedRoute,
    private modalService: BsModalService,
    private authService: AuthService,
    private accountService: AccountService,
    private merchantService: MerchantService,
    private changepasswordService: ChangepasswordService,
  ) { }

  ngOnInit(): void {


    this.accountId = this.route.snapshot.params['accountId'];

    this.isSystemAdmin = (this.authService.getLoggedInUser()?.userRole == 1)
    this.isAccountAdmin = (this.authService.getLoggedInUser()?.userRole == 2)
    this.isMerchantAdmin = (this.authService.getLoggedInUser()?.userRole == 3)


    this.getAccountId();
    this.getAccount();
    this.getMerchantId();
    this.getMerchant();
  }
  getAccountId() {
    const userAccountId = this.authService.getLoggedInUser()?.accountId;
    const selectedAccountId = localStorage.getItem('currentSelectedAccountId');
    if (!this.isSystemAdmin) {
      if (!!userAccountId) {
        this.accountId = userAccountId;
      }
    }
    else {
      this.accountId = selectedAccountId;
    }
  }


  getAccount() {
    const userRole = this.authService.getLoggedInUser()?.userRole;
    if (userRole != 3 && userRole != 4) {
      const selectedAccountId = localStorage.getItem('currentSelectedAccountId') == null ? this.accountId : localStorage.getItem('currentSelectedAccountId');
      if (!!selectedAccountId) {
        this.accountService.getAccount(selectedAccountId)
          .subscribe(response => {
            if (!!response) {
              this.accountName = response.name;
            }
          });
      }
    }
  }
  getMerchantId() {
    const userMerchantId = this.authService.getLoggedInUser()?.merchantId;
    const selectedMerchantId = localStorage.getItem('currentSelectedMerchantId');
    if (this.isMerchantAdmin) {
      if (!!userMerchantId) {
        this.merchantId = userMerchantId;
      }
    }
    else {
      this.merchantId = selectedMerchantId;
    }
  }
  getMerchant() {
    const selectedMerchantId = localStorage.getItem('currentSelectedMerchantId');
    if (!!selectedMerchantId) {
      this.merchantService.getMerchant(this.accountId, selectedMerchantId)
        .subscribe(response => {
          if (!!response) {
            this.merchantName = response.nameEn;
          }
        });
    }
  }


  openAccountModal() {
    const config: ModalOptions = {
      backdrop: false,
      class: 'account-modal border rounded-3'
    };
    const modalRef: BsModalRef = this.modalService.show(ChangeAccountDialogComponent, config);
    modalRef.content.selectedAccountEvent.subscribe((item: IdName) => {
      this.accountId = item.id;
      this.accountName = item.name;
      modalRef.hide();
    });
    modalRef.content.selectedMerchantEvent.subscribe((item: IdName) => {
      this.merchantId = item.id;
      this.merchantName = item.name;
      modalRef.hide();
    });
  }



  
  
  


  openProfileModal() {
    const config: ModalOptions = {
      backdrop: false,
      class: 'profile-modal border rounded-3'
    };
    const modalRef: BsModalRef = this.modalService.show(UserProfileDialogComponent, config);
  }
}



