import { HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, tap } from 'rxjs';
import { HttpService } from 'src/app/services/http.service';

@Injectable({
  providedIn: 'root'
})
export class MerchantService {

  private newMerchantData = new BehaviorSubject(false);

  newMerchantObservable = this.newMerchantData.asObservable();

  constructor(private httpService: HttpService) { }

 emitNewMerchantData() {
this.newMerchantData.next(true);
 }

  
  postMerchant(accountId: string|null, data: any) {
    const id = (accountId == null ? '' : accountId)

    const headers = { 'AccountId': id }
    return this.httpService.post("Merchant", data, headers).pipe(
      tap(response => {
       this.emitNewMerchantData();
      }));
  }
  getMerchant(accountId: string|null, id: string) {
    const accId = (accountId == null ? '' : accountId)

    const headers = { 'AccountId': accId }
    return this.httpService.getbyId("Merchant", id, headers).pipe(
      tap(response => {
      }))
  }
  updateMerchant(accountId: string|null, id: string, data: any) {
    const accId = (accountId == null ? '' : accountId)

    const headers = { 'AccountId': accId }
    return this.httpService.update("Merchant", id, data, headers).pipe(
      tap(response => {
        this.emitNewMerchantData();
      }));
  }
  deleteMerchant(id: string, accountId: string|null) {
    const param = new HttpParams()
      .set('id', id);
      const accId = (accountId == null ? '' : accountId)

    const headers = { 'AccountId': accId }
    return this.httpService.delete("Merchant", param, headers).pipe(
      tap(response => {
        this.emitNewMerchantData();
      }));
  }

getMerchantList(accountId: string|null, data?: any) {
  const id = (accountId == null ? '' : accountId)

    const headers = { 'AccountId': id }
    return this.httpService.get("Merchant/List", data, headers).pipe(
      tap(response => { 
      }))
  }

  //move to plan service 
  getPlans(accountId: string|null, data?: any){
    const id = (accountId == null ? '' : accountId)
    const headers = { 'AccountId': id }
    return this.httpService.get("Plan/List", data, headers).pipe(
      tap(response => { 
      }))
  }
  
  
}
