export interface Merchant {
  id: string;
  accountId: string;
  nameAr: string;
  nameEn: string;
  vatRegistrationNumber: string;
  commerialRegisterationNumber: string;
  businessCategoryAr: string;
  businessCategoryEn: string;
  status: MerchantStatus;
  statusString: string;
  plan: MerchantPlan;
  groupTIN:string;
  clientId:string;
  clientSecret:string;

}
export interface MerchantPlan {
  name:string;
}
export interface MerchantCreateUpdate {
  nameAr: string;
  nameEn: string;
  vatRegistrationNumber: string;
  commerialRegisterationNumber: string;
  status: MerchantStatus;
  businessCategoryAr: string;
  businessCategoryEn: string;
  email: string;
  planId: string;
  canIssueSimplifiedInvoice: boolean;
  canIssueStandardInvoice: boolean;
  logo?: string;
  groupTIN:string;
  createPdf:boolean;
  balanceEnabled:boolean;
}
export interface MerchantFilter {
  accountId?: string;
  pageSize?: number;
  pageNumber?: number;
  searchPattern?: string;
  status?: MerchantStatus;
}
export enum MerchantStatus {
  Active = 0,

  InActive = 1,
}
