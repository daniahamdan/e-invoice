import { AfterViewInit, Component, OnInit } from '@angular/core';
import {
  faEllipsisV,
  faRotate,
  faTrash,
} from '@fortawesome/free-solid-svg-icons';
import {
  Merchant,
  MerchantFilter,
  MerchantStatus,
} from '../../models/merchant.model';
import { MerchantService } from '../../merchant.service';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { BsModalRef, BsModalService, ModalOptions } from 'ngx-bootstrap/modal';
import { ToastService } from 'src/app/services/toast-service/toast.service';
import { TranslateService } from '@ngx-translate/core';
import { ConfirmationDialogComponent } from 'src/app/shared/components/confirmation-dialog/confirmation-dialog.component';
import { MerchantViewComponent } from '../dialogs/merchant-view/merchant-view.component';
import { PageChangedEvent } from 'ngx-bootstrap/pagination';

@Component({
  selector: 'app-merchant-list',
  templateUrl: './merchant-list.component.html',
  styleUrls: ['./merchant-list.component.css'],
})
export class MerchantListComponent implements AfterViewInit, OnInit {
  actionIcon = faEllipsisV;
  updateIcon = faRotate;
  deleteIcon = faTrash;
  merchants: Merchant[] = [];
  isUpdate: boolean = false;
  itemCount: number = 0;
  addsuccessMessage!: string;

  currentFilter: MerchantFilter | null = null;

  paginatorPageSize: number = 10;
  paginatorPageNumber: number = 1;

  accountId: string | null = '';
  getScreenWidth!: number;
  tableMaxSize: number=10;
  getScreenHeight!: number;

  constructor(
    private merchantService: MerchantService,
    public router: Router,
    public route: ActivatedRoute,
    private modalService: BsModalService,
    private toastService: ToastService,
    private translate: TranslateService
  ) {}
  ngOnInit(): void {
    this.accountId = this.route.snapshot.params['accountId'];
    this.getScreenWidth = window.innerWidth;
    this.getScreenHeight = window.innerHeight;
    if(this.getScreenWidth < 1000){
      this.tableMaxSize = 3;
    }
  }

  ngAfterViewInit() {
    this.route.params.subscribe((params: Params) => {
      if (this.accountId != params['accountId']) {
        this.accountId = params['accountId'];
      }
      this.getMerchantList();
    });
  }

  getMerchant(id: string) {
    this.merchantService.getMerchant(this.accountId,id).subscribe((response) => {
      return response;
    });
  }

  openDeleteConfirmationModal(id: string): void {
    const config: ModalOptions = { class: 'modal-sm modal-dialog-centered' };
    const modalRef: BsModalRef = this.modalService.show(
      ConfirmationDialogComponent,
      config
    );
    modalRef.content.deleteConfirmed.subscribe(() => {
      this.confirmDelete(id);
    });
  }
  confirmDelete(id: string) {
    this.merchantService.deleteMerchant(id, this.accountId).subscribe((res) => {
      const deletedSuccessMessage = this.translate.instant(
        'general.deletedSuccessMessage',
        { componentName: 'merchant' }
      );
      this.toastService.success(deletedSuccessMessage);
      this.reloadMerchantTable(this.currentFilter);
    });
  }
  updateMerchant(id: string) {
    this.router.navigate([
      '/merchant/merchant-add'
      , this.accountId
      , { isEdit: true, editId: id },
    ]);
    this.isUpdate = true;
  }
  getMerchantList(merchantData?: any) {
    const data = this.setListRequestData(merchantData);
    this.merchantService.getMerchantList(this.accountId, data).subscribe((merchantData) => {
      this.merchants = merchantData.data;
      this.itemCount = merchantData.totalCount;
      this.merchants.map((i) => {
        i.statusString = MerchantStatus[i.status];
      });
    });
  }
  reloadMerchantTable(filterData?: any) {
    this.getMerchantList(filterData);
  }
  setListRequestData(merchantData?: MerchantFilter | null) {
    if (!!merchantData) {
      merchantData.pageSize = this.paginatorPageSize;
      merchantData.pageNumber = this.paginatorPageNumber;
      return merchantData;
    } else {
      return {
        PageSize: this.paginatorPageSize,
        PageNumber: this.paginatorPageNumber,
        AccountId: this.accountId
      };
    }
  }

  applyMerchantFilter(filterData?: any) {
    this.paginatorPageNumber = 1;
    this.paginatorPageSize = 10;
    this.currentFilter = filterData;
    this.getMerchantList(filterData);
  }

  viewMerchant(rowData: any) {
    this.merchantService.getMerchant(this.accountId, rowData.id).subscribe((response) => {
      const config: ModalOptions = {
        backdrop: true,
        class: 'modal-xl modal-dialog-centered',
        initialState: { data: response },
      };
      const modalRef: BsModalRef = this.modalService.show(
        MerchantViewComponent,
        config
      );
    });
  }
  preventCellClick($event: any) {
    $event.stopPropagation();
  }

  pageChanged(event: PageChangedEvent): void {
    this.paginatorPageNumber = event.page;
    this.paginatorPageSize = event.itemsPerPage;
    this.reloadMerchantTable(this.currentFilter);
  }
}
