import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { AppTranslateService } from 'src/app/services/translate-service/translate.service';
import { environment } from 'src/environments/environment';
import { MerchantService } from '../../merchant.service';
import { IdName } from 'src/app/shared/interfaces/idName.interface';
import { Merchant } from '../../models/merchant.model';
import { faGlobe } from '@fortawesome/free-solid-svg-icons';
import { AuthService } from 'src/app/components/auth/auth.service';
import { UserRole } from 'src/app/components/auth/models/user.model';

@Component({
  selector: 'app-merchant-select',
  templateUrl: './merchant-select.component.html',
  styleUrls: ['./merchant-select.component.css']
})
export class MerchantSelectComponent implements OnInit {
  globeIcon = faGlobe;
  brandName: string = environment.brandName;
  defaultLang: string = environment.defaultLang;

  merchants: IdName[] = [];

  selectedMerchant: string = '';
  accountId: string | null = '';
  isSystemAdmin: boolean = false;
  
  languages = [
    {
      name: 'english',
      value: 'en-US'


    },
    {
      name: 'arabic',
      value: 'ar-SA'
    }
  ]
  logoSrc: string = "../../../../assets/logo/sign_logo.png"

  constructor(private merchantService: MerchantService,
    private router: Router,
    private route: ActivatedRoute,
    private appTranslateService: AppTranslateService,
    private authService: AuthService
  ) { }

  ngOnInit(): void {
    const currentLang = localStorage.getItem('currentLang');
    this.defaultLang = ((currentLang == null || currentLang == 'undefined') ? environment.defaultLang : currentLang);

    this.accountId = this.route.snapshot.params['accountId'];
    this.setMerchants();

    if (window.innerWidth <= 990) {
      this.logoSrc = '../../../../assets/logo/sign_logo_mobile.png';
    }
    this.isSystemAdmin = (this.authService.getLoggedInUser()?.userRole == UserRole.SystemAdmin);
  }

  setMerchants() {
    this.merchantService.getMerchantList(this.accountId, { PageSize: 1000, PageNumber: 1, AccountId: this.accountId })
      .subscribe(response => {
        if (!!response) {
          response.data.forEach((i: Merchant) => {
            this.merchants.push({ name: `${i.nameEn} | ${i.nameAr}`, id: i.id });
          });
        }
      });
  }

  onMerchentSelect(merchantId: string) {
    this.selectedMerchant = merchantId;
  }

  confirmSelectedMerchant() {
    localStorage.setItem('currentSelectedMerchantId', this.selectedMerchant);
    if (!!this.selectedMerchant) {
      this.router.navigate([
        '/branch/branch-list',
        this.accountId,
        this.selectedMerchant,
      ]);
    }
    if(this.isSystemAdmin && this.selectedMerchant === ""){
      this.router.navigate([
        '/merchant/merchant-list',
        this.accountId,
      ]);
    }
  }
  onLangChange(lang: any) {
    lang = lang.value;
    this.appTranslateService.changeLanguage(lang);
  }

}
