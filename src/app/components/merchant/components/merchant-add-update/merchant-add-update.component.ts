import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MerchantCreateUpdate, MerchantStatus } from '../../models/merchant.model';
import { Observable, of } from 'rxjs';
import { IdName } from 'src/app/shared/interfaces/idName.interface';
import { MerchantService } from '../../merchant.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ClipboardService } from 'ngx-clipboard';
import { faCopy } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-merchant-add-update',
  templateUrl: './merchant-add-update.component.html',
  styleUrls: ['./merchant-add-update.component.css']
})
export class MerchantAddUpdateComponent implements OnInit {
  merchantForm!: FormGroup;

  statusKeys: string[] = [];
  status = MerchantStatus;

  accountId!: string;
  submitted: boolean = false;
  copyIcon = faCopy;
  merchantEditId: string = '';
  isEditView: boolean = false;
  clientId: string="";
  clientSecret: string="";
  plans!: Observable<IdName[]>;

  constructor(private merchantService: MerchantService,
    private route: ActivatedRoute,
    private router: Router,
    private clipboardService: ClipboardService
  ) {
    this.statusKeys = Object.keys(this.status).filter(f => !isNaN(Number(f)));
  }

  ngOnInit(): void {
    this.accountId = this.route.snapshot.params['accountId'];
    this.merchantEditId = this.route.snapshot.params['editId'];
    const isEdit = this.route.snapshot.params['isEdit'];

    this.isEditView = (isEdit === 'true' ? true : this.isEditView);
    if (this.isEditView) {
      this.getMerchantById(this.merchantEditId);
    }

    this.merchantForm = new FormGroup({
      'nameAr': new FormControl(null, [Validators.required, Validators.minLength(3), Validators.maxLength(255)]),
      'nameEn': new FormControl(null, [Validators.required, Validators.minLength(3), Validators.maxLength(255)]),
      'groupTIN': new FormControl(null, [Validators.minLength(10), Validators.maxLength(10)]),
      'vatRegistrationNumber': new FormControl(null, [Validators.required, Validators.minLength(10), Validators.maxLength(32), Validators.pattern("^[0-9]*$")]),
      'businessCategoryAr': new FormControl(null, [Validators.required, Validators.minLength(3), Validators.maxLength(255)]),
      'businessCategoryEn': new FormControl(null, [Validators.required, Validators.minLength(3), Validators.maxLength(255)]),
      'commerialRegisterationNumber': new FormControl(null, [Validators.required, Validators.minLength(10), Validators.maxLength(32), Validators.pattern("^[0-9]*$")]),
      'email': new FormControl(null, [Validators.required]),
      'planId': new FormControl(null, [(this.isEditView ? Validators.nullValidator : Validators.required)]),
      'canIssueSimplifiedInvoice': new FormControl(false, []),
      'canIssueStandardInvoice': new FormControl(false, []),
      'status': new FormControl(null),
      'createPdf': new FormControl(false, []),
     
    
    });

    this.getPlans();
  }
  getMerchantById(id: string) {
    this.merchantService
      .getMerchant(this.accountId, id)
      .subscribe(response => {
        this.fillMerchantForm(response);
        this.clientId=response.clientId
        this.clientSecret=response.clientSecret
      });
  }
  fillMerchantForm(currentData: any) {
    this.merchantForm.controls['nameAr'].setValue(currentData.nameAr);
    this.merchantForm.controls['nameEn'].setValue(currentData.nameEn);
    this.merchantForm.controls['groupTIN'].setValue(currentData.groupTIN);
    this.merchantForm.controls['vatRegistrationNumber'].setValue(currentData.vatRegistrationNumber);
    this.merchantForm.controls['status'].setValue(currentData.status);
    this.merchantForm.controls['businessCategoryAr'].setValue(currentData.businessCategoryAr);
    this.merchantForm.controls['businessCategoryEn'].setValue(currentData.businessCategoryEn);
    this.merchantForm.controls['commerialRegisterationNumber'].setValue(currentData.commerialRegisterationNumber);
    this.merchantForm.controls['email'].setValue(currentData.email);
    this.merchantForm.controls['canIssueSimplifiedInvoice'].setValue(currentData.canIssueSimplifiedInvoice);
    this.merchantForm.controls['canIssueStandardInvoice'].setValue(currentData.canIssueStandardInvoice);
    this.merchantForm.controls['createPdf'].setValue(currentData.createPdf);
  }
  onSave() {
    this.submitted = true;
    if (!this.merchantForm.valid) {
      this.merchantForm.markAllAsTouched();
      return;
    }
   const status = +this.merchantForm?.value.status;
    var data: MerchantCreateUpdate = this.merchantForm.getRawValue();
//    data.logo = "logo"
    if (this.isEditView) {
      data.status =status  ;
      this.updateMerchant(data);
    }
    else {
      data.status = MerchantStatus.Active;
      this.addMerchant(data);
    }
    
  }

  addMerchant(data: any) {
    
    this.merchantService
      .postMerchant(this.accountId, data)
      .subscribe(response => {
        if (!!response) {
          this.router.navigate(['merchant/merchant-list', this.accountId]);
        }
      });
  }
  updateMerchant(data: any) {
    this.merchantService.updateMerchant(this.accountId, this.merchantEditId, data)
      .subscribe(response => {
        if (!!response) {
          this.router.navigate(['merchant/merchant-list', this.accountId]);
        }
      });
  }

    
  copyContent() {

     this.clipboardService.copyFromContent(this.clientId);
   
     }
   
     copyContentt() {

       this.clipboardService.copyFromContent(this.clientSecret);
     }
   

  getPlans() {
    this.merchantService.getPlans(this.accountId, { PageSize: 1000, PageNumber: 1 })
      .subscribe(response => {
        if (!!response) {
          this.plans = of(response.data.map((item: IdName) => ({
            id: item.name,
            name: item.id,
          }))).pipe();
        }
      });
  }


  
}
