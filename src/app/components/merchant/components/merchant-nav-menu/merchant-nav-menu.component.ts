import { Component, EventEmitter, Output } from '@angular/core';
import { faSearch } from '@fortawesome/free-solid-svg-icons';
import { MerchantFilter, MerchantStatus } from '../../models/merchant.model';
import { FormControl, FormGroup } from '@angular/forms';
import { ToastService } from 'src/app/services/toast-service/toast.service';
import { AuthService } from 'src/app/components/auth/auth.service';
import { TranslateService } from '@ngx-translate/core';
import { ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-merchant-nav-menu',
  templateUrl: './merchant-nav-menu.component.html',
  styleUrls: ['./merchant-nav-menu.component.css']
})
export class MerchantNavMenuComponent {
  searchIcon = faSearch;

  statusKeys: string[] = [];
  status = MerchantStatus;

  filterForm!: FormGroup;

  accountId!: string
  selectedStatus!: number | null;
  constructor(
    private toastService: ToastService,
    private authService: AuthService,
    private translate: TranslateService,
    private route: ActivatedRoute) {
    this.statusKeys = Object.keys(this.status).filter(f => !isNaN(Number(f)));
  }
  ngAfterViewInit(): void {
    this.route.params.subscribe(
      (params: Params) => {
        if (this.accountId != params['accountId']) {
          this.accountId = params['accountId'];
          this.clearForm();
        }
      }
    );
  }

  ngOnInit(): void {
    this.accountId = this.route.snapshot.params['accountId'];

    this.filterForm = new FormGroup({
      'searchPattern': new FormControl(null),
      'status': new FormControl(null),
    });
  }

  @Output("filterMerchants") filterMerchants: EventEmitter<any> = new EventEmitter();

  onMerchantSearch(keyElement: any) {
    let searchPattern = keyElement.target.value;
    if (searchPattern.length < 3) {
      const searchPatternLength = this.translate
        .instant('general.searchPatternLength');
      this.toastService.error(searchPatternLength);
      return;
    }
    this.filterMerchants.emit({ AccountId: this.accountId, 
      SearchPattern: searchPattern });
  }
  onStatusChange(status: number) {
    this.selectedStatus = status;
    const searchPattern = this.filterForm?.value.searchPattern
    const data: MerchantFilter = {
      accountId: this.accountId,
      status: status
    }
    if (!!searchPattern) {
      data.searchPattern = searchPattern;
    }
    this.filterMerchants.emit(data)
  }
  clearForm() {
    this.selectedStatus = null;
    this.filterForm?.controls['searchPattern'].setValue('');
    this.filterForm?.controls['status'].setValue(null);
    this.filterMerchants.emit({AccountId: this.accountId});
  }



  
}
