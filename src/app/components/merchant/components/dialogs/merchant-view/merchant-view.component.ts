import { Component, Input, OnInit } from '@angular/core';
import { MerchantStatus } from '../../../models/merchant.model';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { ClipboardService } from 'ngx-clipboard';
import { faCopy } from '@fortawesome/free-solid-svg-icons';


@Component({
  selector: 'app-merchant-view',
  templateUrl: './merchant-view.component.html',
  styleUrls: ['./merchant-view.component.css'],
})
export class MerchantViewComponent implements OnInit {
  statusKeys: string[] = [];
  status = MerchantStatus;
  currentData: any;
  copyIcon = faCopy;

  @Input() data: any;
  
  constructor(public bsModalRef: BsModalRef,private clipboardService: ClipboardService) {
    this.statusKeys = Object.keys(this.status).filter((f) => !isNaN(Number(f)));
  }
  ngOnInit(): void {
    this.currentData = this.data;
  }

  copyContent() {
     this.clipboardService.copyFromContent(this.currentData.clientId);
   }
 
   copyContentt() {
      this.clipboardService.copyFromContent(this.currentData.clientSecret);
    }
}
