import { Injectable } from '@angular/core';
import { tap } from 'rxjs';
import { HttpService } from 'src/app/services/http.service';

@Injectable({
  providedIn: 'root'
})
export class ChangepasswordService {

  constructor(private httpService: HttpService){ 

  }

  changePassword(userDetails:any){
       return this.httpService.post("User/ChangePassword",userDetails).pipe(
        tap((response: any) => {
       
        }));
  }


}
