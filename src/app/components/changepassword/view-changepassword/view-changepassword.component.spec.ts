import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewChangepasswordComponent } from './view-changepassword.component';

describe('ViewChangepasswordComponent', () => {
  let component: ViewChangepasswordComponent;
  let fixture: ComponentFixture<ViewChangepasswordComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ViewChangepasswordComponent]
    });
    fixture = TestBed.createComponent(ViewChangepasswordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
