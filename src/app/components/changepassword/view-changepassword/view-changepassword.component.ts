
import { Component } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { TranslateService } from '@ngx-translate/core';

import { BsModalRef } from 'ngx-bootstrap/modal';

import { AppTranslateService } from 'src/app/services/translate-service/translate.service';
import { AccountService } from '../../account/account.service';
import { AuthService } from '../../auth/auth.service';
import { ChangepasswordService } from '../services/changepassword.service';
import {
  AbstractControl,
  ValidationErrors,
  ValidatorFn,
} from '@angular/forms';
import { ToastService } from 'src/app/services/toast-service/toast.service';




@Component({
  selector: 'app-view-changepassword',
  templateUrl: './view-changepassword.component.html',
  styleUrls: ['./view-changepassword.component.css']
})
export class ViewChangepasswordComponent {

  confirmPasswordValidator: ValidatorFn = (
    control: AbstractControl
  ): ValidationErrors | null => {
    return control.value.password1 === control.value.password2
      ? null
      : { PasswordNoMatch: true };
  };

  oldPassword!: FormControl;
  newPassword!: FormControl;
  cnewPassword!: FormControl;
  Password!: FormControl;
  UpdatePasswordForm!: FormGroup;
  defaultLang: any;
  submitted!: boolean;

  loginForm: any;
  accountId!: string | null;

  currentAccountIdParam!: string;
  isSystemAdmin: boolean = false;
  isAccountAdmin: boolean = false;
  isMerchantAdmin: boolean = false;
  isBranchAdmin: boolean = false;
  //selectedAccount!: string;
  passwordsMatching = false;
  isConfirmPasswordDirty = false;
  confirmPasswordClass = 'form-control';
  accountName: string = '';

  error: any;
  username: string = '';
  message!: string;





  constructor(
    private changepasswordService: ChangepasswordService,
    private router: Router,
    private authService: AuthService,
    private translate: TranslateService,
    private appTranslateService: AppTranslateService,
    private fb: FormBuilder,
    private accountService: AccountService,
    private bsModalRef: BsModalRef,
    private route: ActivatedRoute,
    private toastService: ToastService,

  ) { }

  isRequired = (value: string) => value === '' ? false : true;


  ngOnInit(): void {



    this.UpdatePasswordForm = new FormGroup({
      oldPassword: new FormControl(null, [
        Validators.maxLength(255),
        Validators.required,
      ]),

      newPassword: new FormControl(null, [
        Validators.required,
      ]),
      confirmPassword: new FormControl(null, [
        Validators.maxLength(255),
        Validators.required,
      ]),
    },
      { validators: [this.checkNewPasswords, this.checkOldPassword] })
  }




  checkNewPasswords: ValidatorFn = (group: AbstractControl): ValidationErrors | null => {
    let pass = group.get('newPassword')?.value;
    let confirmPass = group.get('confirmPassword')?.value

    return pass === confirmPass ? null : { notSame: true }
  }

  checkOldPassword: ValidatorFn = (group: AbstractControl): ValidationErrors | null => {
    let pass = group.get('newPassword')?.value;
    let oldPassword = group.get('oldPassword')?.value

    return (pass !== oldPassword) ? null : { samePassword: true }
  }

  changesPassword() {
    if (!this.UpdatePasswordForm.valid) {
      return;
    };
    const request = {
      newPassword: this.UpdatePasswordForm.value.newPassword,
      oldPassword: this.UpdatePasswordForm.value.oldPassword
    };
    this.changepasswordService.changePassword(request)
    .subscribe((response) => {
      if (response.status == 500) {
        this.message = 'Error changing password';
        this.toastService.error(this.message);
      } else {
        this.message = 'Password updated successfully';
        this.toastService.success(this.message);
        return response;
      } 


    });
  }

  removeModalAttr() {
    document.body.classList.remove('modal-open');
    document.body.removeAttribute('style');
  }

  onProfileModelClose() {
    this.bsModalRef.hide();
    this.removeModalAttr();
  }
}
