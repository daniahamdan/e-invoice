import { Component, Input, OnInit } from '@angular/core';
import { PageChangedEvent } from 'ngx-bootstrap/pagination';
import { Invoice, InvoiceMessage } from './models/message.model';
import { InvoiceService } from '../../../invoice.service';
import { ActivatedRoute } from '@angular/router';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { faQrcode } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-invoice-messages',
  templateUrl: './invoice-messages.component.html',
  styleUrls: ['./invoice-messages.component.css']
})
export class InvoiceMessagesComponent implements OnInit {
  currentData: {totalCount:number, data: InvoiceMessage[]} | any;
  accountId: string | null = '';
  qrIcon = faQrcode;
  invoices: Invoice[] = [];
  paginatorPageSize: number = 5;
  paginatorPageNumber: number = 1;
  invdata!: string;
  myAngularxQrCode!: string;

  @Input() data: {totalCount:number, data: InvoiceMessage[]} | any;
  @Input() id: string = '';
  constructor(private invoiceService: InvoiceService,
    private route: ActivatedRoute,
    public bsModalRef: BsModalRef
    ) {
    
  }
  ngOnInit(): void {
    this.accountId = this.route.snapshot.params['accountId'];
    this.currentData = this.data;
  }
  reloadInvoiceTable(){
    const data = {
      Id: this.id,
      PageSize: this.paginatorPageSize,
      PageNumber: this.paginatorPageNumber
    }
    this.invoiceService.getMessages(data).subscribe((response) => {
      this.currentData = response;
    });
  }

  downloadFile(base64: any, type: string, filename: string) {
    const blob = new Blob([base64], { type: type });
    const url = window.URL.createObjectURL(blob);
    const link = document.createElement('a');
    link.href = url;
    link.download = filename;
    link.click();
  }

  openQrCode(id: string, number: string) {
    this.invoiceService.getInvoiceQrCode(this.accountId, id)
      .subscribe(response => {
        this.downloadFile(response, 'image/png', number + '-qr.png');
      });
      
  }

  pageChanged(event: PageChangedEvent): void {
    this.paginatorPageNumber = event.page;
    this.paginatorPageSize = event.itemsPerPage;
    this.reloadInvoiceTable();
  }
}
