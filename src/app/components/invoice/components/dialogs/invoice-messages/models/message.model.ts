export interface InvoiceMessage {
  type: InvoiceMessageType;
  code: string;
  category: string;
  message: string;
  status: string;
}

export enum InvoiceMessageType {
  Information = 1,

  Error = 2,

  Warning = 3,
}

export interface Invoice {
  branchName: string;
  terminalName: string;
  reportingStatusString: string;
  totalAmount: string;
  vatAmount: string;
  subTotalAmount: string;
  invoiceNumber: string;
  issueDateTime: string;
  typeString: string;
  xml: string;
  qrCode: string;
  id: string;
}