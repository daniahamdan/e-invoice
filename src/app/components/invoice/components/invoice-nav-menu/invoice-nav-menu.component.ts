import { Component, EventEmitter, Output } from '@angular/core';
import { InvoiceFilter, InvoiceType, ReportingStatus } from '../../models/invoice.model';
import { IdName } from 'src/app/shared/interfaces/idName.interface';
import { faCalendarDays, faFileExcel, faSearch } from '@fortawesome/free-solid-svg-icons';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { FormControl, FormGroup } from '@angular/forms';
import { DatePipe } from '@angular/common';
import { TerminalService } from 'src/app/components/terminal/components/services-terminal/terminal.service';
import { BranchService } from 'src/app/components/branch/components/branch.service';
import { ActivatedRoute, Params } from '@angular/router';
import { InvoiceService } from '../../invoice.service';


@Component({
  selector: 'app-invoice-nav-menu',
  templateUrl: './invoice-nav-menu.component.html',
  styleUrls: ['./invoice-nav-menu.component.css']
})
export class InvoiceNavMenuComponent {
  calendarIcon = faCalendarDays;
  searchIcon = faSearch;
  excelIcon = faFileExcel;

  branches!: Observable<IdName[]>;
  terminals!: Observable<IdName[]>;
  filterForm!: FormGroup;

  accountId!: string;
  merchantId!: string;

  invoiceTypeKeys: string[] = [];
  repoStatusKeys: string[] = [];
  invoiceType = InvoiceType;

  repoStatus = ReportingStatus;

  fromDateString: string | null = null;
  toDateString: string | null = null;
  isCleared = new BehaviorSubject(false);
  $isCleared = this.isCleared.asObservable();

  @Output("filterInvoices") filterInvoices: EventEmitter<any> = new EventEmitter();
  @Output("exportInvoices") exportInvoices: EventEmitter<any> = new EventEmitter();
  translate: any;
  toastService: any;
  filterMonitors: any;

  constructor(private datePipe: DatePipe,
    private terminalService: TerminalService,
    private branchService: BranchService,
    private invoiceService: InvoiceService,
    private route: ActivatedRoute) {
    this.invoiceTypeKeys = Object.keys(this.invoiceType).filter(f => !isNaN(Number(f)));
    this.repoStatusKeys = Object.keys(this.repoStatus).filter(f => !isNaN(Number(f)));
  }
  ngAfterViewInit(): void {
    this.route.params.subscribe(
      (params: Params) => {
        if (this.accountId != params['accountId']) {
          this.accountId = params['accountId'];
        }
        if (this.merchantId != params['merchantId']) {
          this.merchantId = params['merchantId'];
          this.clearForm();
          this.getBranches();
          this.getTerminals();
          this.isCleared.next(true);
        }
      }
    );
  }

  ngOnInit(): void {
    this.accountId = this.route.snapshot.params['accountId'];
    this.merchantId = this.route.snapshot.params['merchantId'];

    this.filterForm = new FormGroup({
      'selectedBranch': new FormControl(null),
      'selectedTerminal': new FormControl(null),
      'type': new FormControl(),
      'invoiceNumber': new FormControl(null),
      'referenceId': new FormControl(null),
      'reportingStatus': new FormControl(),
    });
    this.getBranches();
    this.getTerminals();
  }

  getBranches() {
    this.branchService.getBranchList(this.accountId, { PageSize: 1000, PageNumber: 1, MerchantId: this.merchantId })
      .subscribe(response => {
        if (!!response) {
          this.branches = of(response.data.map((item: any) => ({
            id: item.nameEn,
            name: item.id,
          }))).pipe();
        }
      });
  }
  getTerminals() {
    this.terminalService.getTerminalList(this.accountId, { PageSize: 1000, PageNumber: 1, MerchantId: this.merchantId })
      .subscribe(response => {
        if (!!response) {
          this.terminals = of(response.data.map((item: any) => ({
            id: item.id,
            name: item.nameEn,
          }))).pipe();
        }
      });
  }

  onInvoiceSearchbyReferenceId(keyElement: any) {
    let ReferenceId = keyElement.target.value;
    if (ReferenceId.length < 3) {
      const ReferenceIdLength = this.translate.instant(
        'general.ReferenceIdLength'
      );
      this.toastService.error(ReferenceIdLength);
      return;
    }
    this.filterInvoices.emit({
      AccountId: this.accountId,
      ReferenceId: ReferenceId,
      MerchantId: this.merchantId
    });
  }

  onSave() {
    
    const terminalId = this.filterForm.get('selectedTerminal')?.value;
    const branchId = this.filterForm.get('selectedBranch')?.value;
    const invoiceNumber = this.filterForm.get('invoiceNumber')?.value;
    const type = this.filterForm.get('type')?.value;
    const reportingStatus = this.filterForm.get('reportingStatus')?.value;

    const data: InvoiceFilter = { accountId: this.accountId };
    if (!!branchId) {
      data.branchId = branchId;
    }
    if (!!terminalId) {
      data.terminalId = terminalId;
    }
    if (!!invoiceNumber) {
      data.invoiceNumber = invoiceNumber;
    }
    if (!!this.fromDateString) {
      data.sendDateFrom = this.fromDateString;
    }
    if (!!this.toDateString) {
      data.sendDateTo = this.toDateString;
    }
    if (!!type) {
      data.invoiceType = type;
    }

    if (!!reportingStatus) {
      data.reportingStatus = reportingStatus;
    }

    this.filterInvoices.emit(data);
  }

  clearForm() {
    this.filterForm?.controls['selectedBranch'].setValue(null);
    this.filterForm?.controls['selectedTerminal'].setValue(null);
    this.filterForm?.controls['invoiceNumber'].setValue(null);
    this.filterForm?.controls['type'].setValue(null);
    this.filterForm?.controls['reportingStatus'].setValue(null);
    this.filterForm?.controls['referenceId'].setValue(null);
    this.fromDateString = null;
    this.toDateString = null;
  }
  emitClearFilter() {
    this.clearForm();
    this.isCleared.next(true);
    this.filterInvoices.emit({ accountId: this.accountId, merchantId: this.merchantId });
  }
  onDateChange($event: any) {
    if (!!$event)
      this.setRangeDate($event)
  }
  setRangeDate(date: string) {
    if (!!date) {
      const range = date.split(';');
      this.fromDateString = range[0];
      this.toDateString = range[1];
      this.onSave();
    }
  }

  getFilterData(){
    const terminalId = this.filterForm.get('selectedTerminal')?.value;
    const branchId = this.filterForm.get('selectedBranch')?.value;
    const referenceId = this.filterForm.get('referenceId')?.value;
    const invoiceNumber = this.filterForm.get('invoiceNumber')?.value;
    const type = this.filterForm.get('type')?.value;
    const reportingStatus = this.filterForm.get('reportingStatus')?.value;

    const data: InvoiceFilter = { accountId: this.accountId, merchantId: this.merchantId };
    if (!!branchId) {
      data.branchId = branchId;
    }
    if (!!terminalId) {
      data.terminalId = terminalId;
    }
    if (!!referenceId) {
      data.referenceId = referenceId;
    }
    if (!!invoiceNumber) {
      data.invoiceNumber = invoiceNumber;
    }
    if (!!this.fromDateString) {
      data.sendDateFrom = this.fromDateString;
    }
    if (!!this.toDateString) {
      data.sendDateTo = this.toDateString;
    }
    if (!!type) {
      data.invoiceType = type;
    }

    if (!!reportingStatus) {
      data.reportingStatus = reportingStatus;
    }
    return data;
  }

  exportExcel(){
    var request = this.getFilterData();
    this.exportInvoices.emit(request);
  }

}
