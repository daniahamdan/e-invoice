import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InvoiceQrcodeComponent } from './invoice-qrcode.component';

describe('InvoiceQrcodeComponent', () => {
  let component: InvoiceQrcodeComponent;
  let fixture: ComponentFixture<InvoiceQrcodeComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [InvoiceQrcodeComponent]
    });
    fixture = TestBed.createComponent(InvoiceQrcodeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
