import { Component } from '@angular/core';
import { faDownload, faEllipsisV, faFile, faFilePdf, faMessage, faQrcode, faRotate, faTrash } from '@fortawesome/free-solid-svg-icons';
import { InvoiceService } from './invoice.service';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { BsModalRef, BsModalService, ModalOptions } from 'ngx-bootstrap/modal';
import { PageChangedEvent } from 'ngx-bootstrap/pagination';
import { Invoice, InvoiceAttachment, InvoiceFilter, InvoiceType, ReportingStatus } from './models/invoice.model';
import { InvoiceMessagesComponent } from './components/dialogs/invoice-messages/invoice-messages.component';
import { InvoiceQrcodeComponent } from './components/invoice-qrcode/invoice-qrcode.component';
import { ExportedInvoicesFilter } from '../exported-invoices/models/exported-invoices.mode';
import { TranslateService } from '@ngx-translate/core';
import { ToastService } from 'src/app/services/toast-service/toast.service';







@Component({
  selector: 'app-invoice',
  templateUrl: './invoice.component.html',
  styleUrls: ['./invoice.component.css'],
})
export class InvoiceComponent {
  actionIcon = faEllipsisV;
  downloadIcon = faDownload;
  pdfIcon = faFilePdf;
  qrIcon = faQrcode;
  xmlIcon = faFile;
  messageIcon = faMessage;
  number!: string;
  //invoices: Invoice[] = [];
  isUpdate: boolean = false;

  itemCount: number = 0;
  addsuccessMessage!: string;
  currentFilter: InvoiceFilter | null = null;
  paginatorPageSize: number = 10;
  paginatorPageNumber: number = 1;
  accountId: string | null = '';
  merchantId: string | null = '';
  getScreenWidth!: number;
  getScreenHeight!: number;
  tableMaxSize: number = 10;
  invoiceNumber!: string;
  id!: string;

  // FileSaver = require('file-saver');

  constructor(
    private invoiceService: InvoiceService,
    public router: Router,
    public route: ActivatedRoute,
    private modalService: BsModalService,
    private translate: TranslateService,
    private toastService: ToastService
  ) {


  }


  invoices: Invoice[] = [];
  ngOnInit(): void {

    this.accountId = this.route.snapshot.params['accountId'];
    this.merchantId = this.route.snapshot.params['merchantId'];
    this.getScreenWidth = window.innerWidth;
    this.getScreenHeight = window.innerHeight;

    if (this.getScreenWidth < 1000) {
      this.tableMaxSize = 3;
    }
  }

  ngAfterViewInit() {
    this.route.params.subscribe((params: Params) => {
      if (this.accountId != params['accountId']) {
        this.accountId = params['accountId'];
      }
      if (this.merchantId != params['merchantId']) {
        this.merchantId = params['merchantId'];
      }
      this.getInvoiceList();
    });
  }

  getInvoiceList(invoiceData?: any) {
    const data = this.setListRequestData(invoiceData);
    this.invoiceService
      .getInvoiceList(this.accountId, data)
      .subscribe((invoiceData) => {
        this.invoices = invoiceData.data;
        this.itemCount = invoiceData.totalCount;
        console.log(this.invoices);
        this.invoices.map((i) => {
          i.typeString = InvoiceType[i.type];
        });
        this.invoices.map(i => {
          i.reportingStatusString = ReportingStatus[i.reportingStatus!]
        });
      });
  }


  reloadInvoiceTable(filterData?: any) {
    this.getInvoiceList(filterData);
  }
  setListRequestData(invoiceData?: InvoiceFilter | null) {
    if (!!invoiceData) {
      invoiceData.PageSize = this.paginatorPageSize;
      invoiceData.PageNumber = this.paginatorPageNumber;
      return invoiceData;
    } else {
      return {
        AccountId: this.accountId,
        MerchantId: this.merchantId,
        PageSize: this.paginatorPageSize,
        PageNumber: this.paginatorPageNumber,
      };
    }
  }

  applyInvoiceFilter(filterData?: any) {
    this.paginatorPageNumber = 1;
    this.paginatorPageSize = 10;
    this.currentFilter = filterData;
    this.getInvoiceList(filterData);
  }

  preventCellClick($event: any) {
    $event.stopPropagation();
  }

  pageChanged(event: PageChangedEvent): void {
    this.paginatorPageNumber = event.page;
    this.paginatorPageSize = event.itemsPerPage;
    this.reloadInvoiceTable(this.currentFilter);
  }

  getInvoicePdf(rowData: any) {
    const data: InvoiceAttachment = {
      id: rowData.id,
      accountId: this.accountId,
      merchantId: rowData.merchantId,
      terminalId: rowData.terminalId,
      referenceId: rowData.referenceId,
      createDate: rowData.createDate
    }
    this.invoiceService.getInvoicePdf(this.accountId, data)
      .subscribe(response => {
        this.downloadPdf(response);
      });
  }

  getInvoiceXml(rowData: any) {
    const data: InvoiceAttachment = {
      id: rowData.id,
      accountId: this.accountId,
      merchantId: rowData.merchantId,
      terminalId: rowData.terminalId,
      referenceId: rowData.referenceId,
      createDate: rowData.createDate
    }
    this.invoiceService.getInvoiceXml(this.accountId, data)
      .subscribe(response => {
        this.downloadXml(response, rowData.invoiceNumber + ".xml");
      });
  }

  downloadXml(xmlString: string, number: string) {

    this.downloadFile(xmlString, 'application/xml', number);
  }


  downloadPdf(base64: any) {
    this.downloadFile(base64, 'application/pdf', 'invoice.pdf');
  }

  openQrCode(id: string, number: string) {

    this.invoiceService.getInvoiceQrCode(this.accountId, id)
      .subscribe(response => {

        this.downloadFile(response, 'image/png', number + '-qr.png');
      });
  }



  downloadFile(base64: any, type: string, invoiceNumber: string) {

    // invoiceNumber=this.number;
    const blob = new Blob([base64], { type: type });
    const url = window.URL.createObjectURL(blob);
    const link = document.createElement('a');
    link.href = url;
    link.download = "Invoice-" + invoiceNumber;
    //saveAs(blob,"qr_{invoice number}.png");
    //saveAs(blob, 'imageFileName.png');
    // saveAs(blob, 'export.pdf')
    link.click();
  }



  viewQrCode(id: string, number: string) {

    this.invoiceService.getInvoiceQrCode(this.accountId, id).subscribe((response: any) => {

      const blob = new Blob([response], { type: 'image/png' });
      const url = window.URL.createObjectURL(blob);

      const config: ModalOptions = {
        backdrop: true,
        class: 'modal-md modal-dialog-centered',
        initialState: { qrcode: url, id: number },
      };
      const modalRef: BsModalRef = this.modalService.show(
        InvoiceQrcodeComponent,
        config
      );
    });

  }




  viewInvoiceMessages(id: string) {
    const data = {
      Id: id,
      PageSize: 5,
      PageNumber: 1
    }
    this.invoiceService.getMessages(data).subscribe((response) => {
      const config: ModalOptions = {
        backdrop: true,
        class: 'modal-xl modal-dialog-centered',
        initialState: { data: response, id: id },
      };
      const modalRef: BsModalRef = this.modalService.show(
        InvoiceMessagesComponent,
        config
      );
    });
  }

  exportInvoices(request: InvoiceFilter){
    request.PageNumber = this.paginatorPageNumber;
    request.PageSize = this.paginatorPageSize;

    this.invoiceService.exportToExcel(this.accountId, request)
    .subscribe((response: any) =>{
      const deletedSuccessMessage = this.translate.instant(
        'invoice.excelMsgProcess'
      );
      this.toastService.success(deletedSuccessMessage);
    });
  }

}

