import { Injectable } from '@angular/core';
import { tap } from 'rxjs';
import { HttpService } from 'src/app/services/http.service';

@Injectable({
  providedIn: 'root'
})
export class InvoiceService {

  constructor(private httpService: HttpService) { }

  getInvoiceList(accountId: string | null, data?: any) {
    const id = (accountId == null ? '' : accountId)

    const headers = { 'AccountId': id }
    return this.httpService.get("Invoice/List", data, headers).pipe(
      tap(response => {
      }))
  }
  getInvoicePdf(accountId: string | null, data?: any) {
    const id = (accountId == null ? '' : accountId)
    const headers = { 'AccountId': id }
    return this.httpService.get("Invoice/GetPdf", data, headers, 'blob' as 'json').pipe(
      tap(response => {
      }))

      
  }
  
  getInvoiceQrCode(accountId: string | null,invoiceId: string) {  
      const headers = { 'AccountId': accountId }
      return this.httpService.getbyId("Invoice/GetQr", invoiceId, headers, 'blob' as 'json').pipe(
        tap(response => { 
        }))
        
    }

    getMessages(data: any) {  
      return this.httpService.get("Invoice/GetInvoiceMessages", data).pipe(
        tap(response => { 
        }))
    }

    getInvoiceXml(accountId: string | null, data?: any) {
      const id = (accountId == null ? '' : accountId)
      const headers = { 'AccountId': id }
      return this.httpService.get("Invoice/GetXml", data, headers, 'blob' as 'json').pipe(
        tap(response => {
        }))
    }

    exportToExcel(accountId: string | null, data?: any) {
      const id = (accountId == null ? '' : accountId)
      const headers = { 'AccountId': id }
      return this.httpService.post("Invoice/ExportToExcel", data, headers).pipe(
        tap(response => {
        }));
    }
}
