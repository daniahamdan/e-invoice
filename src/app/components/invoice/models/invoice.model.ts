export interface Invoice {
  ReportingStatus: any;
  branchName: string;
  terminalName: string;
  type: InvoiceType;
  reportingStatus:ReportingStatus;
  reportingStatusString: string;
  totalAmount: string;
  vatAmount: string;
  subTotalAmount: string;
  invoiceNumber: string;
  issueDateTime: string;
  typeString: string;
  xml: string;
  qrCode: string;
  id: string;
  downloadPdf: boolean;
  referenceId:string;
}
export interface InvoiceFilter {
  accountId?: string;
  merchantId?: string;
  terminalId?: string;
  branchId?: string;
  referenceId?: string;
  invoiceNumber?: number;
  sendDateFrom?: string;
  sendDateTo?: string;
  invoiceType?: InvoiceType;
  reportingStatus?:ReportingStatus;
  PageSize?: number;
  PageNumber?: number;
}
export interface InvoiceAttachment {
  id: string;
  accountId: string | null;
  merchantId: string;
  terminalId: string;
  referenceId: string;
  createDate: string;
}
export enum InvoiceType {
  StandardInvoice = 1,

  StandardDebitNote = 2,

  StandardCreditNote = 3,

  SimplifiedInvoice = 4,

  SimplifiedCreditNote = 5,

  SimplifiedDebitNote = 6,
  
}
export enum ReportingStatus
{
    REJECTED = 3,
    PASS = 1,
    WARNING = 2,
    Unidentified = 0,
}