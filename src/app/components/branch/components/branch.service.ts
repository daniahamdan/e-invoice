import { HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, tap } from 'rxjs';
import { HttpService } from 'src/app/services/http.service';

@Injectable({
  providedIn: 'root'
})
export class BranchService {


 branchId!: string;




 constructor(private httpService: HttpService) { }




  
  postBranch(accountId: string|null, data: any) {
    const id = (accountId == null ? '' : accountId)

    const headers = { 'AccountId': id }
    return this.httpService.post("Branch", data, headers).pipe(
      tap(response => {
        
      }));
  }


  getBranchList(accountId: string|null, data?: any) {
    const id = (accountId == null ? '' : accountId)
  
      const headers = { 'AccountId': id }
      return this.httpService.get("Branch/List", data, headers).pipe(
        tap(response => { 
          
        }))
    }

   

    getBranch(id: string) {
      localStorage.setItem('currentSelectedBranchId', this.branchId);
      this.branchId= id;
      return this.httpService.getbyId("Branch", id).pipe(
        
        tap(response => {
        }))
    }

    updateBranch(accountId: string|null, id: string, data: any) {
      const accId = (accountId == null ? '' : accountId)

      const headers = { 'AccountId': accId }
      return this.httpService.update("Branch", id, data, headers).pipe(
        tap(response => {
          
        }));
    }
    deleteBranch(id: string) {
      const param = new HttpParams()
        .set('id', id);
      return this.httpService.delete("Branch", param).pipe(
        tap(response => {

        }));
    }

   
  


  

}





















function user(user: any): string {
  throw new Error('Function not implemented.');
}

