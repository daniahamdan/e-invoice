import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BranchviewComponent } from './branchview.component';

describe('BranchviewComponent', () => {
  let component: BranchviewComponent;
  let fixture: ComponentFixture<BranchviewComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [BranchviewComponent]
    });
    fixture = TestBed.createComponent(BranchviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
