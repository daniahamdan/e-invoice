import { Component, Input } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { BranchStatus } from '../../models/branch.model';

@Component({
  selector: 'app-branchview',
  templateUrl: './branchview.component.html',
  styleUrls: ['./branchview.component.css']
})
export class BranchviewComponent {

  statusKeys: string[] = [];
  status = BranchStatus;
  currentData: any;

  @Input() data: any;
  constructor(public bsModalRef: BsModalRef) {
 this.statusKeys = Object.keys(this.status).filter((f) => !isNaN(Number(f)));
  }
  ngOnInit(): void {
    this.currentData = this.data;
  }
}
