import { AfterViewInit, Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import {
  faEllipsisV,
  faRotate,
  faTrash,
} from '@fortawesome/free-solid-svg-icons';
import { TranslateService } from '@ngx-translate/core';
import { BsModalRef, BsModalService, ModalOptions } from 'ngx-bootstrap/modal';
import { PageChangedEvent } from 'ngx-bootstrap/pagination';
import { UserType } from 'src/app/components/users/users/models/user.model';
import { ToastService } from 'src/app/services/toast-service/toast.service';
import { ConfirmationDialogComponent } from 'src/app/shared/components/confirmation-dialog/confirmation-dialog.component';
import { BranchService } from '../branch.service';
import { BranchviewComponent } from '../dialogs/branchview/branchview.component';
import { Branch, BranchFilter, BranchStatus } from '../models/branch.model';

@Component({
  selector: 'app-branch-list',
  templateUrl: './branch-list.component.html',
  styleUrls: ['./branch-list.component.css'],
})
export class BranchListComponent implements AfterViewInit, OnInit {
  [x: string]: any;

  currentFilter: BranchFilter | null = null;

  isUpdate: boolean = false;
  paginatorPageSize: number = 10;
  paginatorPageNumber: number = 1;
  actionIcon = faEllipsisV;
  updateIcon = faRotate;
  deleteIcon = faTrash;
  getScreenWidth!: number;
  getScreenHeight!: number;
  tableMaxSize: number=10;
  merchantId: string | null = '';
  accountId: string | null = '';
  Branches: Branch[] = [];
  itemCount: any;
  constructor(
    public router: Router,
    public route: ActivatedRoute,
    private modalService: BsModalService,
    private branchService: BranchService,
    private toastService: ToastService,
    private translate: TranslateService
  ) {}

  ngAfterViewInit() {
    this.route.params.subscribe((params: Params) => {
      if (this.merchantId != params['merchantId']) {
        this.merchantId = params['merchantId'];
      }
      if (this.accountId != params['accountId']) {
        this.accountId = params['accountId'];
      }
      this.getBranchList();
    });
  }

  ngOnInit(): void {
    this.merchantId = this.route.snapshot.params['merchantId'];
    this.accountId = this.route.snapshot.params['accountId'];
    this.getScreenWidth = window.innerWidth;
    this.getScreenHeight = window.innerHeight;
    if(this.getScreenWidth < 1000){
      this.tableMaxSize = 3;
    }
  }

  getBranchList(branchData?: any) {
    
    const data = this.setListRequestData(branchData);

    this.branchService
      .getBranchList(this.accountId, data)
      .subscribe((branchData) => {
        
        this.Branches = branchData.data;
        this.itemCount = branchData.totalCount;
        this.Branches.map((i) => {

          i.statusString = BranchStatus[i.status];
      
        });
      });
  }

  applyBranchFilter(filterData?: any) {
    this.paginatorPageNumber = 1;
    this.paginatorPageSize = 10;
    this.currentFilter = filterData;
    this.getBranchList(filterData);
  }

  setListRequestData(branchData?: BranchFilter | null) {
    if (!!branchData) {
      branchData.pageSize = this.paginatorPageSize;

      branchData.pageNumber = this.paginatorPageNumber;
      return branchData;
    } else {
      return {
        PageSize: this.paginatorPageSize,
        PageNumber: this.paginatorPageNumber,
        MerchantId: this.merchantId
      };
    }
  }

  getBranch(id: string) {
    this.branchService.getBranch(id).subscribe((response: any) => {
      return response;
    });
  }

  viewBranch(rowData: any) {
    this.branchService.getBranch(rowData.id).subscribe((response: any) => {
      const config: ModalOptions = {
        backdrop: true,
        class: 'modal-xl modal-dialog-centered',
        initialState: { data: response },
      };
      const modalRef: BsModalRef = this.modalService.show(
        BranchviewComponent,
        config
      );
    });
  }

  updateBranch(id: string) {
    this.router.navigate([
      '/branch/branch-add',
      this.accountId,
      this.merchantId,
      { isEdit: true, editId: id },
    ]);
    this.isUpdate = true;
  }

  preventCellClick($event: any) {
    $event.stopPropagation();
  }

  reloadBranchTable(filterData?: any) {
    this.getBranchList(filterData);
  }

  pageChanged(event: PageChangedEvent): void {
    this.paginatorPageNumber = event.page;
    this.paginatorPageSize = event.itemsPerPage;
    this.reloadBranchTable(this.currentFilter);
  }

  confirmDelete(id: string) {
    this.branchService.deleteBranch(id).subscribe((res: any) => {
      const deletedSuccessMessage = this.translate.instant(
        'general.deletedSuccessMessage',
        { componentName: 'branch' }
      );
      this.toastService.success(deletedSuccessMessage);
      this.reloadBranchTable(this.currentFilter);
    });
  }

  openDeleteConfirmationModal(id: string): void {
    const config: ModalOptions = { class: 'modal-sm modal-dialog-centered' };
    const modalRef: BsModalRef = this.modalService.show(
      ConfirmationDialogComponent,
      config
    );
    modalRef.content.deleteConfirmed.subscribe(() => {
      this.confirmDelete(id);
    });
  }
}
