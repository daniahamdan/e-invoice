import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BranchAddUpdateComponent } from './branch-add-update.component';

describe('BranchAddUpdateComponent', () => {
  let component: BranchAddUpdateComponent;
  let fixture: ComponentFixture<BranchAddUpdateComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [BranchAddUpdateComponent]
    });
    fixture = TestBed.createComponent(BranchAddUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
