import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { BranchService } from '../branch.service';
import { BranchCreateUpdate, BranchStatus } from '../models/branch.model'
@Component({
  selector: 'app-branch-add-update',
  templateUrl: './branch-add-update.component.html',
  styleUrls: ['./branch-add-update.component.css']
})
export class BranchAddUpdateComponent {
  branchForm!: FormGroup;

  statusKeys: string[] = [];
  status = BranchStatus;

 
  accountId!: string;
  submitted: boolean = false;

  branchEditId: string = '';
  isEditView: boolean = false;
  merchantId!: string;
  branchId!: string;

  

  constructor(private branchService: BranchService,
    private route: ActivatedRoute,
    private router: Router
  ) {
    this.statusKeys = Object.keys(this.status).filter(f => !isNaN(Number(f)));
  }

  ngOnInit(): void {
  
 
    this.merchantId = this.route.snapshot.params['merchantId'];
    this.accountId = this.route.snapshot.params['accountId'];
    this.branchEditId = this.route.snapshot.params['editId'];
    const isEdit = this.route.snapshot.params['isEdit'];

    this.isEditView = (isEdit === 'true' ? true : this.isEditView);
    if (this.isEditView) {
      this.getBranchById(this.branchEditId);
     // localStorage.setItem('currentSelectedBranchId', this.branchEditId);
    }
    this.branchForm = new FormGroup({
      'nameAr': new FormControl(null, [Validators.required, Validators.minLength(3), Validators.maxLength(255)]),
      'nameEn': new FormControl(null, [Validators.required, Validators.minLength(3), Validators.maxLength(255)]),
      'reference': new FormControl(null, [Validators.minLength(1), Validators.maxLength(32), Validators.pattern("^[0-9]*$")]),
      'status': new FormControl(null),
      'number': new FormControl(null, [Validators.required, Validators.minLength(3), Validators.maxLength(255)]),
      'zipCode': new FormControl(null, [Validators.required, Validators.minLength(5), Validators.maxLength(5)]),
      'streetName': new FormControl(null, [Validators.required, Validators.minLength(5), Validators.maxLength(255)]),
      'buildingNumber': new FormControl(null, [Validators.required, Validators.minLength(4), Validators.maxLength(4)]),
      'buildingName': new FormControl(null, [Validators.required, Validators.minLength(5), Validators.maxLength(255)]),
      'city': new FormControl(null, [Validators.required]),
      'additionalNumber': new FormControl(null, [Validators.required, Validators.minLength(4), Validators.maxLength(4)]),
      'saudiNationalAddress': new FormControl(null, [Validators.required, Validators.minLength(3), Validators.maxLength(255)]),
      'citySubDivision': new FormControl(null, [Validators.required]),
      'countrySubentity': new FormControl(null, [Validators.required]),
    
    });

    
  }
  getBranchById(id: string) {
    this.branchService
      .getBranch(id)
      .subscribe(response => {
        this.fillBranchForm(response);
    
      });
  }
  fillBranchForm(currentData: any) {
    
    this.branchForm.controls['nameAr'].setValue(currentData.nameAr);
    this.branchForm.controls['nameEn'].setValue(currentData.nameEn);
    this.branchForm.controls['reference'].setValue(currentData.reference);
    this.branchForm.controls['city'].setValue(currentData.city);
    this.branchForm.controls['number'].setValue(currentData.number);
    this.branchForm.controls['status'].setValue(currentData.status);
    this.branchForm.controls['streetName'].setValue(currentData.streetName);
    this.branchForm.controls['buildingNumber'].setValue(currentData.buildingNumber);
    this.branchForm.controls['buildingName'].setValue(currentData.buildingName);
    this.branchForm.controls['zipCode'].setValue(currentData.zipCode);
    this.branchForm.controls['additionalNumber'].setValue(currentData.additionalNumber);
    this.branchForm.controls['citySubDivision'].setValue(currentData.citySubDivision);
    this.branchForm.controls['countrySubentity'].setValue(currentData.countrySubentity);
    this.branchForm.controls['saudiNationalAddress'].setValue(currentData.saudiNationalAddress);
  }
  onSave() {
  
    this.submitted = true;
    if (!this.branchForm.valid) {
      this.branchForm.markAllAsTouched();
      return;
    }
    const status = +this.branchForm?.value.status;
    var data: BranchCreateUpdate = this.branchForm.getRawValue();

     data.zipCode = +data.zipCode;
    if (this.isEditView) {
      data.status = status;
      data.merchantId=this.merchantId;
  
      this.updateBranch(data);
    }
    else {
      data.status = BranchStatus.Active;
      data.merchantId=this.merchantId;

      this.addBranch(data);
    }
  }

  addBranch(data: any) {
    this.branchService
      .postBranch(this.accountId, data)
      .subscribe(response => {
        if (!!response) {
          this.router.navigate(['branch/branch-list', this.accountId,this.merchantId]);
        }
      });
  }


  

  updateBranch(data: any) {
    this.branchService.updateBranch(this.accountId, this.branchEditId, data)
      .subscribe((response: any) => {
      
        if (!!response) {
       
          this.router.navigate(['branch/branch-list', this.accountId, this.merchantId]);
        }
      });
  }


}
