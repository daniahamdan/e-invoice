import { Component, EventEmitter, Output } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Params } from '@angular/router';
import { faSearch } from '@fortawesome/free-solid-svg-icons';
import { TranslateService } from '@ngx-translate/core';
import { AuthService } from '../../../auth/auth.service';
import { BranchFilter, BranchStatus } from '../models/branch.model';
import { ToastService } from '../../../../services/toast-service/toast.service';

@Component({
  selector: 'app-nav-branch',
  templateUrl: './nav-branch.component.html',
  styleUrls: ['./nav-branch.component.css'],
})
export class NavBranchComponent {
  searchIcon = faSearch;

  statusKeys: string[] = [];
  status = BranchStatus;

  filterForm!: FormGroup;

  merchantId!: string;

  accountId!: string;
  selectedStatus!: number | null;
  constructor(
    private toastService: ToastService,
    private authService: AuthService,
    private translate: TranslateService,
    private route: ActivatedRoute
  ) {
    this.statusKeys = Object.keys(this.status).filter((f) => !isNaN(Number(f)));
  }
  ngAfterViewInit(): void {
    this.route.params.subscribe((params: Params) => {
      if (this.merchantId != params['merchantId']) {
        this.merchantId = params['merchantId'];
      }
      if (this.accountId != params['accountId']) {
        this.accountId = params['accountId'];
      }
      this.clearForm();
    });
  }

  ngOnInit(): void {
    this.merchantId = this.route.snapshot.params['merchantId'];
    this.accountId = this.route.snapshot.params['accountId'];

    this.filterForm = new FormGroup({
      searchPattern: new FormControl(null),
      status: new FormControl(null),
    });
  }

  @Output('filterBranches') filterBranches: EventEmitter<any> =
    new EventEmitter();

  onBranchSearch(keyElement: any) {
    let searchPattern = keyElement.target.value;
    if (searchPattern.length < 3) {
      const searchPatternLength = this.translate.instant(
        'general.searchPatternLength'
      );
      this.toastService.error(searchPatternLength);
      return;
    }
    this.filterBranches.emit({ 
      SearchPattern: searchPattern, 
      MerchantId: this.merchantId
     });
  }
  onStatusChange(status: number) {
    this.selectedStatus = status;
    const searchPattern = this.filterForm?.value.searchPattern;
    const data: BranchFilter = {
      status: status,
      merchantId: this.merchantId
    };
    if (!!searchPattern) {
      data.searchPattern = searchPattern;
    }
    this.filterBranches.emit(data);
  }
  emitClearForm() {
    this.clearForm();
    this.filterBranches.emit();
  }
  clearForm() {
    this.selectedStatus = null;
    this.filterForm?.controls['searchPattern'].setValue('');
    this.filterForm?.controls['status'].setValue(null);
  }
}
