

export interface Branch {
 
    id: string;
    merchantId: string;
    nameAr: string;
    nameEn: string;
    reference:string;
    number: string;
    city:string;
    streetName:string;
    buildingNumber:string;
    buildingName:string;
    citySubDivision:string;
    additionalNumber:string;
    status: BranchStatus;
    statusString: string;
    zipCode:number;
    countrySubentity:string;
    type: UserType;   
    typeString: string;
    saudiNationalAddress:string

   
  }



  export interface BranchFilter {
    merchantId?: string | null;
    pageSize?: number;
    pageNumber?: number;
    searchPattern?: string;
    status?: BranchStatus;
  }
  export interface BranchCreateUpdate {
    id: string;
    merchantId: string;
    nameAr: string;
    nameEn: string;
    reference:string;
    number: string;
    city:string;
    streetName:string;
    buildingNumber:string;
    buildingName:string;
    citySubDivision:string;
    additionalNumber:string;
    status: BranchStatus;
    statusString: string;
    zipCode:number;
    countrySubentity:string
  
  }
  

  export enum BranchStatus {
    Active = 0,
  
    InActive = 1,
  
    //Deleted = 2,
  }

  export enum UserType {
    SystemAdmin = 1,
    AccountAdmin = 2,
    MerchantAdmin = 3,
    BranchAdmin = 4,
    
  }
  