export class User {
  constructor(public username: string,
    public accountId: string,
    public userId: string,
    public merchantId: string,
    public userRole: UserRole,
    private _tokenType: string,
    private _expiresIn: number,
    private _expiresInMS: number,
    private _token: string) { }

  get token() {
    return this._token;
  }
  get tokenType() {
    return this._tokenType;
  }
  get expiresIn() {
    return this._expiresIn;
  }
  get expiresInMS() {
    return this._expiresInMS;
  }


}

export enum UserRole {
  SystemAdmin = 1,
  AccountAdmin = 2,
  MerchantAdmin = 3,
  BranchAdmin = 4
}
