import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthService } from './auth.service';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { environment } from 'src/environments/environment';
import { AppTranslateService } from 'src/app/services/translate-service/translate.service';
import { Login } from './models/login.model';
import { faGlobe } from '@fortawesome/free-solid-svg-icons';
import { UserRole } from './models/user.model';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css'],
})
export class AuthComponent implements OnInit {
  globeIcon = faGlobe;
  brandName: string = environment.brandName;
  defaultLang: string = environment.defaultLang;

  loginForm!: FormGroup;
  oldPassword!: FormControl;
  newPassword!: FormControl;
  cnewPassword!: FormControl;
     UpdatePasswordForm!: FormGroup;
  error: string | null = null;
  submitted: boolean = false;

  languages = [
    {
      name: 'english',
      value: 'en-US',
    },
    {
      name: 'arabic',
      value: 'ar-SA',
    },
  ];

  constructor(
    private authService: AuthService,
    private router: Router,
    private translate: TranslateService,
    private appTranslateService: AppTranslateService,
    private fb:FormBuilder
  ) { }

  ngOnInit(): void {
    const currentLang = localStorage.getItem('currentLang');
    this.defaultLang =
      currentLang == null || currentLang == 'undefined'
        ? environment.defaultLang
        : currentLang;
    this.loginForm = new FormGroup({
      username: new FormControl(null, [
        Validators.maxLength(255),
        Validators.required,
      ]),
      password: new FormControl(null, [
        Validators.maxLength(255),
        Validators.required,
      ]),
    });

    this.oldPassword= new FormControl(null, [
      Validators.required,
    ]);

   // this.newPassword= new FormControl(null, [
    //  Validators.maxLength(255),
    //  Validators.required,
//]);
    //this.loginForm=this.fb.group({
    //  oldPassword : this.oldPassword,
    //  newPassword : this.newPassword

  //  })
  }

  login() {
    this.submitted = true;
    if (!this.loginForm.valid) {
      return;
    }
    const request: Login = {
      username: this.loginForm?.value.username,
      password: this.loginForm?.value.password,
    };
    this.authService.login(request).subscribe(
      (response) => {
        //route accordining to user role
        this.routeWithUserRole(response.userRole);
        if(response.username){

          let userDetails=this.loginForm.value;
          userDetails.email=response.email;
        }
      },
      (errorMessage) => {
        const loginErrorMessage = this.translate.instant('login.errorMessage');
        this.error = loginErrorMessage;
      }
    );



  }


  onLangChange(lang: any) {
    lang = lang.value;
    this.appTranslateService.changeLanguage(lang);
  }

  routeWithUserRole(role: UserRole) {
    const accountId = this.authService.getLoggedInUser()?.accountId;
    const merchantId = this.authService.getLoggedInUser()?.merchantId;
    
    switch (role) {

      case 1:
        this.router.navigate(['/accounts']);
        break;
      case 2:
        this.router.navigate(['/merchants', accountId]);
        break;
      case 3:
        
        this.router.navigate(['/branch/branch-list', accountId, merchantId]);
        break;
    }
  }
}
