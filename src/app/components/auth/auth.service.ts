import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject, catchError, tap, throwError } from 'rxjs';
import { User, UserRole } from './models/user.model';
import { HttpService } from 'src/app/services/http.service';
import { Login } from './models/login.model';

export interface AuthResponseData {
  token: string,
  exipresIn: 0,
  tokenType: string,
  accountId: string,
  userId: string,
  userRole: UserRole
}

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  user = new BehaviorSubject<User | null>(null);
  private tokenExpirationTimer: any;

  constructor(private httpService: HttpService, private router: Router) {
  }

  login(loginRequest: Login) {
    return this.httpService.post<AuthResponseData>(
      "User/GetToken",
      loginRequest
    ).pipe(
      catchError(errorRes => {
        let errorMessage = ""
        if (errorRes.status === 401) {
          errorMessage = "Unauthorized";
        }
        return throwError(errorMessage);
      }),
      tap(
        response => {
          const exipresInMs = this.setExpiresInMS(response.exipresIn);
          const user = new User(
            loginRequest.username,
            response.accountId,
            response.userId,
            response.merchantId,
            response.userRole,
            response.tokenType,
            response.exipresIn,
            exipresInMs,
            response.token);
          this.user.next(user);

          localStorage.setItem('userData', JSON.stringify(user));
          this.checkTokenExpiration(exipresInMs);
        }));
  }

  logout() {
    this.user.next(null);
    this.router.navigate(['/login']);
    localStorage.removeItem('userData');
    if (this.tokenExpirationTimer) {
      clearTimeout(this.tokenExpirationTimer);
    }
    this.tokenExpirationTimer = null;
    localStorage.clear();
  }

  getLoggedInUser() {

    if (!!this.user.value) {
      return this.user.value
    }
    else {
      let storedData = localStorage.getItem('userData');
      if (!!storedData) {
        const userData: {
          username: string,
          accountId: string,
          userId: string,
          merchantId: string,
          userRole: UserRole,
          _tokenType: string,
          _expiresIn: number,
          _expiresInMS: number,
          _token: string
        } = JSON.parse(storedData);
        if (!!userData) {
          this.checkTokenExpiration(userData._expiresInMS)
          return userData
        }
      }
      return null;
    }
  }

  autoLogin() {
    const userData: {
      username: string,
      accountId: string,
      userId: string,
      merchantId: string,
      userRole: UserRole,
      _tokenType: string,
      _expiresIn: number,
      _expiresInMS: number,
      _token: string
    } = JSON.parse(localStorage.getItem('userData') || '{}');
    if (!userData) {
      return;
    }

    const loadedUser = new User(
      userData.username,
      userData.accountId,
      userData.userId,
      userData.merchantId,
      userData.userRole,
      userData._tokenType,
      userData._expiresIn,
      userData._expiresInMS,
      userData._token);

    if (loadedUser.token) {
      this.user.next(loadedUser);
    }
  }

  getAuthToken() {
    let storedData = localStorage.getItem('userData');
    if (!!storedData) {
      const userData: {
        username: string,
        accountId: string,
        userId: string,
        merchantId: string,
        userRole: UserRole,
        _tokenType: string,
        _expiresIn: number,
        _expiresInMS: number,
        _token: string
      } = JSON.parse(storedData);
      if (!!userData) {
        this.checkTokenExpiration(userData._expiresInMS)
        return userData._token
      }
    }
    return null;
  }

  autoLogout() {
    this.user.next(null);
    let storedData = localStorage.getItem('userData');
    if (!!storedData) {
      const userData: {
        _expiresInMS: number,
        _expiresIn: number,
        _token: string
      } = JSON.parse(storedData);
      this.checkTokenExpiration(userData._expiresInMS)
    }
  }

  checkTokenExpiration(tokenExpirationTime: number) {
    const currentTime = Date.now();
    if (currentTime >= tokenExpirationTime) {
      this.logout();
    }
  }
  setExpiresInMS(exipresIn: any) {
    var today = new Date();
    const expiredMS = today.setSeconds(exipresIn);
    return expiredMS;
  }

  
  getUserRole() {
    let storedData = localStorage.getItem('userData');
    if (!!storedData) {
      const userData: {
        username: string,
        accountId: string,
        userId: string,
        merchantId: string,
        userRole: UserRole,
        _tokenType: string,
        _expiresIn: number,
        _expiresInMS: number,
        _token: string
      } = JSON.parse(storedData);
      if (!!userData) {
        this.checkTokenExpiration(userData._expiresInMS)
        return userData.userRole
      }
    }
    return UserRole.MerchantAdmin;
  }


  changePassword(accountId: string| null,userDetails:any){
    const headers = { 'AccountId': accountId }
    const resetPassword={
      OldPassword:userDetails.oldPassword,
      Password:userDetails.newPassword,
      UserName:userDetails.username,
    }

      return this.httpService.post("User/ChangePassword",resetPassword, headers).pipe(
        tap((response: any) => {
       
        }));
  }

  postTerminal(accountId: string|null, data: any) {
    const id = (accountId == null ? '' : accountId)

    const headers = { 'AccountId': id }
    return this.httpService.post("Terminal", data, headers).pipe(
      tap((response: any) => {
     
      }));
  }


  getInvoiceQrCode(accountId: string | null,invoiceId: string) {  
    const headers = { 'AccountId': accountId }
    return this.httpService.getbyId("Invoice/GetQr", invoiceId, headers, 'blob' as 'json').pipe(
      tap(response => { 
      }))
      
  }

}
