import { HttpHandler, HttpHeaders, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { exhaustMap, take } from 'rxjs/operators';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthInterceptorService {
  constructor(private authService: AuthService) { }

  intercept(request: HttpRequest<any>, next: HttpHandler) {
    let headers = new HttpHeaders();
    headers = request.headers;

    headers = headers.set('Authorization', `Bearer ${this.authService.getAuthToken()}`);
    request = request.clone({ headers: headers });

    return next.handle(request);
  }
}
