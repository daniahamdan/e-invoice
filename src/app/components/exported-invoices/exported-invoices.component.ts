import { AfterViewInit, Component, OnInit } from '@angular/core';
import { ExportedInvoices, ExportedInvoicesFilter, ExportedInvoicesStatus } from './models/exported-invoices.mode';
import { ExportedInvoicesService } from './exported-invoices.service';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { PageChangedEvent } from 'ngx-bootstrap/pagination';
import { faFileExcel } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-exported-invoices',
  templateUrl: './exported-invoices.component.html',
  styleUrls: ['./exported-invoices.component.css']
})
export class ExportedInvoicesComponent implements OnInit, AfterViewInit {
  excelIcon = faFileExcel;

  itemCount: number = 0;
  currentFilter: ExportedInvoicesFilter | null = null;
  paginatorPageSize: number = 10;
  paginatorPageNumber: number = 1;
  accountId: string | null = '';
  merchantId: string | null = '';

  exportedInvoices: ExportedInvoices[] = [];

  constructor(
    private exportedService: ExportedInvoicesService,
    public router: Router,
    public route: ActivatedRoute
  ) {}


  ngOnInit(): void {

    this.accountId = this.route.snapshot.params['accountId'];
    this.merchantId = this.route.snapshot.params['merchantId'];
  }

  ngAfterViewInit() {
    this.route.params.subscribe((params: Params) => {
      if (this.accountId != params['accountId']) {
        this.accountId = params['accountId'];
      }
      if (this.merchantId != params['merchantId']) {
        this.merchantId = params['merchantId'];
      }
      this.getList();
    });
  }

  getList(filter?: any) {
    const data = this.setListRequestData(filter);
    this.exportedService
      .getList(this.accountId, data)
      .subscribe((response) => {
        this.exportedInvoices = response.data;
        this.itemCount = response.totalCount;
        this.exportedInvoices.map(i => {
          i.statusString = ExportedInvoicesStatus[i.status].toLowerCase();
        });
      });
  }


  reloadTable(filterData?: any) {
    this.getList(filterData);
  }
  setListRequestData(filter?: ExportedInvoicesFilter | null) {
    if (!!filter) {
      filter.pageSize = this.paginatorPageSize;
      filter.pageNumber = this.paginatorPageNumber;
      return filter;
    } else {
      return {
        MerchantId: this.merchantId,
        PageSize: this.paginatorPageSize,
        PageNumber: this.paginatorPageNumber,
      };
    }
  }

  applyFilter(filterData?: any) {
    this.paginatorPageNumber = 1;
    this.paginatorPageSize = 10;
    this.currentFilter = filterData;
    this.getList(filterData);
  }

  preventCellClick($event: any) {
    $event.stopPropagation();
  }

  pageChanged(event: PageChangedEvent): void {
    this.paginatorPageNumber = event.page;
    this.paginatorPageSize = event.itemsPerPage;
    this.reloadTable(this.currentFilter);
  }

  downloadExcel(id: string){
    this.exportedService
    .GetInvoiceExcel(this.accountId, {Id: id, MerchantId: this.merchantId})
    .subscribe((base64) => {
      const blob = new Blob([base64], { type: 'application/vnd.ms-excel' });
      const url = window.URL.createObjectURL(blob);
      const link = document.createElement('a');
      link.href = url;
      link.download = `Invoice-${new Date().toLocaleDateString()}.xlsx`;
      link.click();
    });
  }
}
