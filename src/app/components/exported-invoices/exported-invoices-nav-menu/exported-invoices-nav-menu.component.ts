import { Component, EventEmitter, Output } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { faCalendarDays, faFileExcel } from '@fortawesome/free-solid-svg-icons';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { IdName } from 'src/app/shared/interfaces/idName.interface';
import { InvoiceType } from '../../invoice/models/invoice.model';
import { DatePipe } from '@angular/common';
import { TerminalService } from '../../terminal/components/services-terminal/terminal.service';
import { BranchService } from '../../branch/components/branch.service';
import { ActivatedRoute, Params } from '@angular/router';
import { ExportedInvoicesFilter, ExportedInvoicesStatus } from '../models/exported-invoices.mode';

@Component({
  selector: 'app-exported-invoices-nav-menu',
  templateUrl: './exported-invoices-nav-menu.component.html',
  styleUrls: ['./exported-invoices-nav-menu.component.css']
})
export class ExportedInvoicesNavMenuComponent {
  calendarIcon = faCalendarDays;

  branches!: Observable<IdName[]>;
  terminals!: Observable<IdName[]>;
  filterForm!: FormGroup;

  accountId!: string;
  merchantId!: string;

  statusKeys: string[] = [];
  status = ExportedInvoicesStatus;

  fromDateString: string | null = null;
  toDateString: string | null = null;
  isCleared = new BehaviorSubject(false);
  $isCleared = this.isCleared.asObservable();


  @Output("filterExportedInvoices") filterExportedInvoices: EventEmitter<any> = new EventEmitter();

  constructor(private datePipe: DatePipe,
    private terminalService: TerminalService,
    private branchService: BranchService,
    private route: ActivatedRoute) {
     
    this.statusKeys = Object.keys(this.status).filter(f => !isNaN(Number(f)));
  }
  ngAfterViewInit(): void {
    this.route.params.subscribe(
      (params: Params) => {
        if (this.accountId != params['accountId']) {
          this.accountId = params['accountId'];
        }
        if (this.merchantId != params['merchantId']) {
          this.merchantId = params['merchantId'];
          this.clearForm();
          this.getBranches();
          this.getTerminals();
          this.isCleared.next(true);
        }
      }
    );
  }

  ngOnInit(): void {
    this.accountId = this.route.snapshot.params['accountId'];
    this.merchantId = this.route.snapshot.params['merchantId'];

    this.filterForm = new FormGroup({
      'branchId': new FormControl(null),
      'terminalId': new FormControl(null),
      'status': new FormControl(null),
    });
    this.getBranches();
    this.getTerminals();
  }

  getBranches() {
    this.branchService.getBranchList(this.accountId, { PageSize: 1000, PageNumber: 1, MerchantId: this.merchantId })
      .subscribe(response => {
        if (!!response) {
          this.branches = of(response.data.map((item: any) => ({
            id: item.nameEn,
            name: item.id,
          }))).pipe();
        }
      });
  }
  getTerminals() {
    this.terminalService.getTerminalList(this.accountId, { PageSize: 1000, PageNumber: 1, MerchantId: this.merchantId })
      .subscribe(response => {
        if (!!response) {
          this.terminals = of(response.data.map((item: any) => ({
            id: item.nameEn,
            name: item.id,
          }))).pipe();
        }
      });
  }

  onSave() {
  
    const terminalId = this.filterForm.get('terminalId')?.value;
    const branchId = this.filterForm.get('branchId')?.value;
    const status = this.filterForm.get('status')?.value;
    
    const data: ExportedInvoicesFilter = { merchantId:this.merchantId};
    if (!!branchId) {
      data.branchId = branchId;
    }
    if (!!terminalId) {
      data.terminalId = terminalId;
    }

    if (!!this.fromDateString) {
      data.fromDate = this.fromDateString;
    }
    if (!!this.toDateString) {
      data.toDate = this.toDateString;
    }
    if (!!this.status) {
      data.status = status;
    }
   
    this.filterExportedInvoices.emit(data);
  }

  clearForm() {
    this.filterForm?.controls['terminalId'].setValue(null);
    this.filterForm?.controls['branchId'].setValue(null);
    this.filterForm?.controls['status'].setValue(null);
    this.fromDateString = null;
    this.toDateString = null;
  }
  emitClearFilter() {
    this.clearForm();
    this.isCleared.next(true);
    this.filterExportedInvoices.emit({ accountId: this.accountId, merchantId: this.merchantId });
  }
  onDateChange($event: any) {
    if (!!$event)
      this.setRangeDate($event)
  }
  setRangeDate(date: string) {
    if (!!date) {
      const range = date.split(';');
      this.fromDateString = range[0];
      this.toDateString = range[1];
      this.onSave();
    }
  }

}
