export interface ExportedInvoices {
    id:string;
    branchName: string;
    terminalName: string;
    createDate: string;
    status: ExportedInvoicesStatus;
    statusString: string;
    username: string;
}

export interface ExportedInvoicesFilter {
    merchantId: string;
    branchId?: string;
    terminalId?: string;
    fromDate?: string;
    toDate?: string;
    pageSize?: number;
    pageNumber?: number;
    status?: ExportedInvoicesStatus;
}
export enum ExportedInvoicesStatus {
    Pending = 0,
 
    Ready = 1,
    
    Failed = 2,
}