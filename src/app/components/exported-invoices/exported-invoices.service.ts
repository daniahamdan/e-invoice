import { Injectable } from '@angular/core';
import { tap } from 'rxjs';
import { HttpService } from 'src/app/services/http.service';

@Injectable({
  providedIn: 'root'
})
export class ExportedInvoicesService {

  constructor(private httpService: HttpService) { }

  getList(accountId: string | null, data?: any) {
    const id = (accountId == null ? '' : accountId)

    const headers = { 'AccountId': id }
    return this.httpService.get("ExportedInvoices/List", data, headers).pipe(
      tap(response => {
      }))
  }

  GetInvoiceExcel(accountId: string | null, data?: any) {
    const id = (accountId == null ? '' : accountId)

    const headers = { 'AccountId': id }
    return this.httpService.get("ExportedInvoices/GetInvoiceExcel", data, headers, 'blob' as 'json').pipe(
      tap(response => {
      }))
  }
  
}
