import { Component, EventEmitter, Output } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Params } from '@angular/router';
import { faSearch } from '@fortawesome/free-solid-svg-icons';
import { TranslateService } from '@ngx-translate/core';
import { Observable, of } from 'rxjs';
import { AuthService } from 'src/app/components/auth/auth.service';
import { BranchService } from 'src/app/components/branch/components/branch.service';
import { ToastService } from 'src/app/services/toast-service/toast.service';
import { IdName } from 'src/app/shared/interfaces/idName.interface';
import { TerminalFilter, TerminalStatus } from '../models/terminal.model';

@Component({
  selector: 'app-nav-terminal',
  templateUrl: './nav-terminal.component.html',
  styleUrls: ['./nav-terminal.component.css']
})
export class NavTerminalComponent {
  [x: string]: any;
  searchIcon = faSearch;

  statusKeys: string[] = [];
  status = TerminalStatus;

  filterForm!: FormGroup;
  branches!: Observable<IdName[]>
  accountId!: string
  selectedStatus!: number | null;
  merchantId: any;
  
  @Output("filterTerminals") filterTerminals: EventEmitter<any> = new EventEmitter();
  constructor(
    private toastService: ToastService,
    private authService: AuthService,
    private translate: TranslateService,
    private branchService: BranchService,
  private route: ActivatedRoute
  ) {
    this.statusKeys = Object.keys(this.status).filter((f) => !isNaN(Number(f)));}

  ngAfterViewInit(): void {
    this.route.params.subscribe(
      (params: Params) => {
        if (this.merchantId != params['merchantId']) {
          this.merchantId = params['merchantId'];
       
        }

        if (this.accountId != params['accountId']) {
          this.accountId = params['accountId'];
        }

        this.clearForm()
      }
      
    );
  } 

  ngOnInit(): void {
    
    
    this.accountId = this.route.snapshot.params['accountId'];
    this.merchantId = this.route.snapshot.params['merchantId'];

    this.filterForm = new FormGroup({
      'searchPattern': new FormControl(null),
      'status': new FormControl(null),
      'BranchId':new FormControl(null),
    });

    this.getBranches();
  }




  onSave() {

  
    const BranchId = this.filterForm.get('BranchId')?.value;
   
    const data: TerminalFilter = { MerchantId: this.merchantId};

    if (!!BranchId) {
      data.BranchId= BranchId;
    }

    this.filterTerminals.emit(data);

  }


  onTerminalSearch(keyElement: any) {
    let searchPattern = keyElement.target.value;
    if (searchPattern.length < 3) {
      const searchPatternLength = this.translate
        .instant('general.searchPatternLength');
      this.toastService.error(searchPatternLength);
      return;
    }
    this.filterTerminals.emit({ 
      SearchPattern: searchPattern, 
      MerchantId: this.merchantId
     });
  }
  onStatusChange(status: number) {
    this.selectedStatus = status;
    const searchPattern = this.filterForm?.value.searchPattern
    const data: TerminalFilter = {
      status: status,
      MerchantId: this.merchantId
    }
    if (!!searchPattern) {
      data.searchPattern = searchPattern;
    }
    this.filterTerminals.emit(data)
  }

  emitClearForm() {
    this.clearForm();
    this.filterTerminals.emit();
  }
  clearForm() {
    this.selectedStatus = null;
    this.filterForm?.controls['searchPattern'].setValue('');
    this.filterForm?.controls['status'].setValue(null);
    this.filterForm?.controls['BranchId'].setValue(null);
    
  }
  


  getBranches() {
    const data = {
      MerchantId: this.merchantId,
    }
    this.branchService.getBranchList(this.accountId, data)
      .subscribe((response: { data: any[]; }) => {
        if (!!response) {
          this.branches = of(response.data.map((item: any) => ({
            id: item.nameEn,
            name: item.id,
          }))).pipe();

        }
      });
  }



}


