import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NavTerminalComponent } from './nav-terminal.component';

describe('NavTerminalComponent', () => {
  let component: NavTerminalComponent;
  let fixture: ComponentFixture<NavTerminalComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [NavTerminalComponent]
    });
    fixture = TestBed.createComponent(NavTerminalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
