import { Component, Input } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { TerminalStatus } from '../models/terminal.model';

@Component({
  selector: 'app-terminal-view',
  templateUrl: './terminal-view.component.html',
  styleUrls: ['./terminal-view.component.css']
})
export class TerminalViewComponent {
  statusKeys: string[] = [];
  status = TerminalStatus;
  currentData: any;

  @Input() data: any;
  constructor(public bsModalRef: BsModalRef) {
    this.statusKeys = Object.keys(this.status).filter((f) => !isNaN(Number(f)));
  }
  ngOnInit(): void {
    this.currentData = this.data;
  }
}
