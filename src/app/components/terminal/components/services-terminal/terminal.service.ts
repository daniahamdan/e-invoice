import { HttpClient, HttpEvent, HttpParams, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs';
import { BehaviorSubject, Observable, tap } from 'rxjs';
import { HttpService } from 'src/app/services/http.service';
import { IdName } from 'src/app/shared/interfaces/idName.interface';


@Injectable({
  providedIn: 'root'
})
export class TerminalService {



   //filename = basename(__filename);



  branchService: any;


  router: any;
  merchantId: any;
  
  branches: IdName[] = [];

  selectedBranch: string = '';
  accountId: string | null = '';
  private baseUrl = 'http://localhost:8080';
  serverUrl: string = "InvoiceBulkUpload";
   
 
 
  constructor(private httpService: HttpService,private http: HttpClient) { }
 

 
  
  
  postTerminal(accountId: string|null, data: any) {
    const id = (accountId == null ? '' : accountId)

    const headers = { 'AccountId': id }
    return this.httpService.post("Terminal", data, headers).pipe(
      tap((response: any) => {
     
      }));
  }


  getTerminal(id: string) {
    return this.httpService.getbyId("Terminal", id).pipe(
      tap((response: any) => {
      
      }))
  }
  

  updateTerminal(accountId: string|null, id: string, data: any) {
    const accId = (accountId == null ? '' : accountId)

    const headers = { 'AccountId': accId }
    return this.httpService.update("Terminal", id, data, headers).pipe(
      tap((response: any) => {
        
      }));

  }





  getFiles(): Observable<any> {
    return this.http.get(`InvoiceBulkUpload/files`);
  }


  public sendFormData(formData: any) {
    return this.http.post<any>(this.serverUrl, formData, {
      reportProgress: true,
      observe: 'events'
    });
}

   
  postInvoiceBulkUpload(accountId: string|null, data: any) {

   
   // const reqBody: FormData = new FormData();
//reqBody.append('file',File)
//  reqBody.append('json', new Blob([jsonObjAlreadyStringified],{ type: 'application/json' }));
   // const param = new HttpParams()
   // const reqBody: FormData = new FormData();
    //reqBody.append('file',File)
    const id = (accountId == null ? '' : accountId)
    const headers = { 'AccountId': id }
    return this.httpService.post("InvoiceBulkUpload",data,headers).pipe(
      catchError(errorRes => {
        let errorMessage = ""
        if (errorRes.status === 500) {
          errorMessage = "Unable to store file";
          console.log("Error ")
        }
        return throwError(errorMessage);
      }),
      tap((response: any) => {
     
        
      }));
  }

  deleteTerminal(id: string) {
    const param = new HttpParams()
      .set('id', id);
    return this.httpService.delete("Terminal", param).pipe(
      tap((response: any) => {
      
      }));
  }

getTerminalList(accountId: string|null, data?: any) {
  const id = (accountId == null ? '' : accountId)

    const headers = { 'AccountId': id }
    return this.httpService.get("Terminal/List", data, headers).pipe(
      tap((response: any) => { 
      }))
  }



 


}
