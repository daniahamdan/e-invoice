import { Component } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import {
  faDownload,
  faEllipsisV,
  faFile,
  faRotate,
  faTrash,
  faUpload,
} from '@fortawesome/free-solid-svg-icons';
import { TranslateService } from '@ngx-translate/core';
import { BsModalService, ModalOptions, BsModalRef } from 'ngx-bootstrap/modal';
import { PageChangedEvent } from 'ngx-bootstrap/pagination';
import { debounce } from 'rxjs';
import { MerchantViewComponent } from 'src/app/components/merchant/components/dialogs/merchant-view/merchant-view.component';
import {
  MerchantFilter,
  MerchantStatus,
} from 'src/app/components/merchant/models/merchant.model';
import { ToastService } from 'src/app/services/toast-service/toast.service';
import { ConfirmationDialogComponent } from 'src/app/shared/components/confirmation-dialog/confirmation-dialog.component';

import {
  Terminal,
  
  TerminalExcel,
  
  TerminalFilter,
  TerminalStatus,
} from '../models/terminal.model';
import { TerminalService } from '../services-terminal/terminal.service';
import { TerminalViewComponent } from '../terminal-view/terminal-view.component';
import { Observable } from 'rxjs/internal/Observable';
import { ElementRef } from '@angular/core';

@Component({
  selector: 'app-terminal-list',
  templateUrl: './terminal-list.component.html',
  styleUrls: ['./terminal-list.component.css'],
})
export class TerminalListComponent {
  // @ViewChild('fileUpload', { static: false })
  fileUpload!: ElementRef;
  files = [];
  actionIcon = faEllipsisV;
  updateIcon = faRotate;
  downloadIcon = faDownload;
  deleteIcon = faTrash;
  uploadIcon = faUpload;
  xmlIcon = faFile;
  id!: string;
  terminals: Terminal[] = [];
  isUpdate: boolean = false;
  itemCount: number = 0;
  addsuccessMessage!: string;
  status: string | undefined;
  currentFilter: TerminalFilter | null = null;

  paginatorPageSize: number = 10;
  paginatorPageNumber: number = 1;
  File!: File | Blob;
  //file?: string;
  accountId!: string;
  branchId: string | undefined;
  merchantId!: string;
  terminalId!: string;
  headerService: any;
  httpClient: any;

  currentFile!: string;
  base64Output!: string;
  message = '';
  isVisible: boolean = true;

  isB2C!: boolean;
  base64: any;
  getScreenWidth!: number;
  tableMaxSize: number=10;
  getScreenHeight!: number;
  // base64: string;
  constructor(
    public router: Router,
    public terminalService: TerminalService,
    public route: ActivatedRoute,
    private modalService: BsModalService,
    private toastService: ToastService,
    private translate: TranslateService,
    private bsModalRef: BsModalRef,
  ) {}
  ngOnInit(): void {
    this.merchantId = this.route.snapshot.params['merchantId'];
    this.accountId = this.route.snapshot.params['accountId'];
    this.getScreenWidth = window.innerWidth;
    this.getScreenHeight = window.innerHeight;
    if(this.getScreenWidth < 1000){
      this.tableMaxSize = 3;
    }
  }

  ngAfterViewInit() {
    this.route.params.subscribe((params: Params) => {
      if (this.merchantId != params['merchantId']) {
        this.merchantId = params['merchantId'];
      }
      if (this.accountId != params['accountId']) {
        this.accountId = params['accountId'];
      }
      this.getTerminalList();
    });
  }

  getTerminal(id: string) {
    this.terminalService.getTerminal(id).subscribe((response: any) => {
      return response;
    });
  }

  openDeleteConfirmationModal(id: string): void {
    const config: ModalOptions = { class: 'modal-sm modal-dialog-centered' };
    const modalRef: BsModalRef = this.modalService.show(
      ConfirmationDialogComponent,
      config
    );
    modalRef.content.deleteConfirmed.subscribe(() => {
      this.confirmDelete(id);
    });
  }
  confirmDelete(id: string) {
    this.terminalService.deleteTerminal(id).subscribe((res: any) => {
      const deletedSuccessMessage = this.translate.instant(
        'general.deletedSuccessMessage',
        { componentName: 'terminal' }
      );
      this.toastService.success(deletedSuccessMessage);
      this.reloadTerminalTable(this.currentFilter);
    });
  }
  updateTerminal(id: string) {
    this.router.navigate([
      '/terminal/terminal-add',
      this.accountId,
      this.merchantId,
      { isEdit: true, editId: id },
    ]);
    this.isUpdate = true;
  }
  getTerminalList(terminalData?: any) {
    const data = this.setListRequestData(terminalData);
    // this.terminalService.getTerminalList(this.accountId, data).subscribe((terminalData) => {

    this.terminalService
      .getTerminalList(this.accountId, data)
      .subscribe((terminalData) => {
        this.terminals = terminalData.data;
        this.itemCount = terminalData.totalCount;
        this.terminals.map((i) => {
          i.statusString = TerminalStatus[i.status];
        });
      });
    // });
  }

  reloadTerminalTable(filterData?: any) {
    this.getTerminalList(filterData);
  }

  setListRequestData(terminalData?: TerminalFilter | null) {
    if (!!terminalData) {
      terminalData.PageSize = this.paginatorPageSize;
      terminalData.MerchantId = this.merchantId;
      terminalData.PageNumber = this.paginatorPageNumber;
      return terminalData;
    } else {
      return {
        PageSize: this.paginatorPageSize,
        PageNumber: this.paginatorPageNumber,

        MerchantId: this.merchantId,
      };
    }
  }

  setListRequestDataa(id: string, fileBase64: string) {
    const terminaldata: TerminalExcel = {
      terminalId: id,
      merchantId: this.merchantId,
      isB2C: false,
      file: fileBase64,
    };

    return terminaldata;
  }

  onProfileModelClose() {
    this.bsModalRef.hide();
    this.removeModalAttr();
  }

  removeModalAttr() {
    document.body.classList.remove('modal-open');
    document.body.removeAttribute('style');
  }

  converFile(event: any, id: string) {
    //   let toConvert = 'UEsDBBQAAAgIABSJA..'//supply your full plain string here
    //const file = `data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64,${toConvert}`;
    let file = event.target.files.item(0);
    const reader = new FileReader();
    reader.readAsDataURL(file);

    reader.onload = () => {
      // debugger;
      // let toConvert = 'UEsDBBQAAAgIABSJA..'
      // let base64 = reader.result as `data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64,${typeof toConvert}`;
      //supply your full plain string here
      let base64 = reader.result as string;
      // this.base64 = `data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64,${toConvert}`;
      //TODO: call upload
      this.upload(base64, id);

      // const re = "data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64,";
      //  var str = base64;
      // var newstr = str.replace(re, "");
      //  console.log(newstr)
    };
  }


  hideDiv(){
    this.isVisible = false;  //  Hide the div
  }


  upload(file: string, id: string): void {
    const re =
      'data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64,';
    var str = file;
    var newstr = str.replace(re, '');
    //console.log(newstr)
    const data = this.setListRequestDataa(id, newstr);

    this.terminalService.postInvoiceBulkUpload(this.accountId, data)

      .subscribe((response) => {
        if (response.status == 500) {
          this.message = 'Unable to store file';
          this.toastService.error(this.message);
        } else {
          this.message = 'Uploaded successfully';
          this.toastService.success(this.message);
          return response;
        } 

  
      });
    
      this.getTerminalList();
  }

  applyTerminalFilter(filterData?: any) {
    this.paginatorPageNumber = 1;
    this.paginatorPageSize = 10;
    this.currentFilter = filterData;
    this.getTerminalList(filterData);
  }

  viewTerminal(rowData: any) {
    this.terminalService.getTerminal(rowData.id).subscribe((response: any) => {
      const config: ModalOptions = {
        backdrop: true,
        class: 'modal-xl modal-dialog-centered',
        initialState: { data: response },
      };
      const modalRef: BsModalRef = this.modalService.show(
        TerminalViewComponent,
        config
      );
    });
  }

  preventCellClick($event: any) {
    $event.stopPropagation();
  }

  pageChanged(event: PageChangedEvent): void {
    this.paginatorPageNumber = event.page;
    this.paginatorPageSize = event.itemsPerPage;
    this.reloadTerminalTable(this.currentFilter);
  }
}
