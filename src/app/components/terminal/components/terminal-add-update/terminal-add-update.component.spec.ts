import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TerminalAddUpdateComponent } from './terminal-add-update.component';

describe('TerminalAddUpdateComponent', () => {
  let component: TerminalAddUpdateComponent;
  let fixture: ComponentFixture<TerminalAddUpdateComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [TerminalAddUpdateComponent]
    });
    fixture = TestBed.createComponent(TerminalAddUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
