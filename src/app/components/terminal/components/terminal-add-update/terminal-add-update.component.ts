import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, of } from 'rxjs';
import { BranchService } from 'src/app/components/branch/components/branch.service';
import { BranchFilter } from 'src/app/components/branch/components/models/branch.model';
import { IdName } from 'src/app/shared/interfaces/idName.interface';
import { TerminalCreateUpdate, TerminalStatus } from '../models/terminal.model';
import { TerminalService } from '../services-terminal/terminal.service';

@Component({
  selector: 'app-terminal-add-update',
  templateUrl: './terminal-add-update.component.html',
  styleUrls: ['./terminal-add-update.component.css']
})
export class TerminalAddUpdateComponent {
  [x: string]: any;
  terminalForm!: FormGroup;
  statusKeys: string[] = [];
  status = TerminalStatus;
  branchData!: BranchFilter;
  submitted: boolean = false;
  branchId!: string;
  merchantId!: string;
  terminalEditId: string = '';
  isEditView: boolean = false;
  branches!: Observable<IdName[]>;
  selectedBranch: string = '';
  accountId!: string;
  newbranchId!: string;

  constructor(private terminalService: TerminalService, private branchService: BranchService,
    private route: ActivatedRoute,
    private router: Router
  ) {
    this.statusKeys = Object.keys(this.status).filter(f => !isNaN(Number(f)));
  }

  ngOnInit(): void {
    this.merchantId = this.route.snapshot.params['merchantId'];
    this.accountId = this.route.snapshot.params['accountId'];
    this.branchId = this.route.snapshot.params['branchId'];
    this.terminalEditId = this.route.snapshot.params['editId'];
    const isEdit = this.route.snapshot.params['isEdit'];

    this.isEditView = (isEdit === 'true' ? true : this.isEditView);
    if (this.isEditView) {
      this.getTerminalById(this.terminalEditId);
    }
    this.terminalForm = new FormGroup({
      'branchId': new FormControl(null, [(Validators.required)]),
      'nameAr': new FormControl(null, [Validators.required, Validators.minLength(3), Validators.maxLength(255)]),
      'nameEn': new FormControl(null, [Validators.required, Validators.minLength(3), Validators.maxLength(255)]),
      'reference': new FormControl(null, [ Validators.minLength(1), Validators.maxLength(32), Validators.pattern("^[0-9]*$")]),
      'status': new FormControl(null),
      'number': new FormControl(null, [Validators.required, Validators.minLength(3), Validators.maxLength(255)])
    });

    this.getBranches();

  }
  getTerminalById(id: string) {
    this.terminalService
      .getTerminal(id)
      .subscribe(response => {
        this.fillTerminalForm(response);
      });
  }


  fillTerminalForm(currentData: any) {

    this.terminalForm.controls['nameAr'].setValue(currentData.nameAr);
    this.terminalForm.controls['branchId'].setValue(currentData.branchId);
    this.terminalForm.controls['nameEn'].setValue(currentData.nameEn);
    this.terminalForm.controls['reference'].setValue(currentData.reference);
    this.terminalForm.controls['number'].setValue(currentData.number);
    this.terminalForm.controls['status'].setValue(currentData.status);
  }
  onSave() {

    this.submitted = true;
    if (!this.terminalForm.valid) {
      this.terminalForm.markAllAsTouched();
      return;
    }


    const status = +this.terminalForm?.value.status;
    var data: TerminalCreateUpdate = this.terminalForm.getRawValue();

    if (this.isEditView) {
      data.status = status;
      data.merchantId = this.merchantId;

      this.updateTerminal(data);
    }
    else {
      data.status = TerminalStatus.Active;

      data.merchantId = this.merchantId;

      this.addTerminal(data);
    }
  }

  addTerminal(data: any) {
    this.terminalService
      .postTerminal(this.accountId, data)
      .subscribe(response => {
        if (!!response) {
          this.router.navigate(['terminal/terminal-list', this.accountId, this.merchantId]);
        }
      });
  }



  updateTerminal(data: any) {
    this.terminalService.updateTerminal(this.accountId, this.terminalEditId, data)
      .subscribe((response: any) => {

        if (!!response) {
          this.router.navigate(['terminal/terminal-list', this.accountId, this.merchantId]);
        }
      });
  }

  getBranches() {
    const data = {
      MerchantId: this.merchantId,
    }
    this.branchService.getBranchList(this.accountId, data)
      .subscribe((response) => {
        if (!!response) {
          this.branches = of(response.data.map((item: any) => ({

            id: item.nameEn,
            name: item.id,
          }))).pipe();

        }
      });
  }

}



