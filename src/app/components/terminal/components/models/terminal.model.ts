export class Terminal {
    id!: string;
  
    merchantId!: string;
    branchId!: string;
    nameEn!: string;
    nameAr!: string;
    number!: string;
    reference!: string;
    status!: TerminalStatus;
    statusString!: string;
    branchName!: string;
    xml!: string;
    exel!: string;
    file!: File;


}

export interface TerminalCreateUpdate {
  
  id: string;
  merchantId: string;
    branchId: string;
    nameAr: string;
    nameEn: string;
    reference:string;
    number:string;
    status: TerminalStatus; 
     
  }

  export interface TerminalFilter {
  //  accountId: string;

    MerchantId?: string;
    BranchId?: string;
    PageSize?: number;
    PageNumber?: number;
    searchPattern?: string;
    status?: TerminalStatus;
  }


  export interface TerminalExcel {
    terminalId: string;
    
      merchantId: string;
      isB2C:boolean;
       file:string;
    }

  export enum TerminalStatus {
    Active = 0,
  
    InActive = 1,
  
    //Deleted = 2,
  }
  
