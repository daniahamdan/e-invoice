import { AfterViewInit, Component, EventEmitter, Input, OnDestroy, Output } from "@angular/core";
import { faCalendar } from "@fortawesome/free-solid-svg-icons";
import { NgbCalendar, NgbDate, NgbDateParserFormatter, NgbDateStruct } from "@ng-bootstrap/ng-bootstrap";
import { Observable, Subscription } from "rxjs";


@Component({
  selector: 'app-date-picker',
  templateUrl: './date-picker.component.html',
  styleUrls: ['./date-picker.component.css']
})
export class DatePickerComponent implements AfterViewInit, OnDestroy {
  calendarIcon = faCalendar;

  date: NgbDateStruct;
  dateString: string = '';

  private eventsSubscription!: Subscription;
  @Input() isCleared!: Observable<boolean>;
  @Output() dateChange = new EventEmitter<string>();

  constructor(private calendar: NgbCalendar, public formatter: NgbDateParserFormatter) {
    this.date = calendar.getToday();
  }
  ngOnDestroy(): void {
    this.eventsSubscription.unsubscribe();
  }
  ngAfterViewInit(): void {
    this.getToday();
    this.eventsSubscription = this.isCleared.subscribe(value => {
      if (value) {
        this.getToday();
      }
    })
  }
  getToday() {
    this.date = this.calendar.getToday();
    this.dateString = this.formatter.format(this.date);
  }
  onDateSelection(date: NgbDate) {
    this.date = date;
    this.dateString = this.formatter.format(date);
    this.emitDateChanges();
  }
  emitDateChanges() {
    this.dateChange.emit(this.dateString);
  }

}
