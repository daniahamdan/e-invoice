import { AfterViewInit, Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { faCalendar } from '@fortawesome/free-solid-svg-icons';
import { NgbCalendar, NgbDate, NgbDateParserFormatter, NgbDateStruct, NgbTimeAdapter } from '@ng-bootstrap/ng-bootstrap';
import { Observable, Subscription } from 'rxjs';
import { NgbTimeStringAdapter } from '../../services/date-time.service';

@Component({
  selector: 'app-date-time-picker',
  templateUrl: './date-time-picker.component.html',
  styleUrls: ['./date-time-picker.component.css'],
  providers: [
    {
      provide: NgbTimeAdapter,
      useClass: NgbTimeStringAdapter
    },
  ]
})
export class DateTimePickerComponent implements OnInit, AfterViewInit, OnDestroy {
  calendarIcon = faCalendar;
  hoveredDate: NgbDate | null = null;

  fromDate: NgbDate | null = null;
  toDate: NgbDate | null = null;

  fromDateString: string | null = null;
  toDateString: string | null = null;

  model!: NgbDateStruct;
  date!: { year: number; month: number; };

  timeFrom = '00:00:00';
  timeTo = '00:00:00';

  private cleanEventSub!: Subscription;
  @Input() isCleared!: Observable<boolean>;

  private accountEventSub!: Subscription;
  @Input() isAccountChanged: Observable<boolean> | undefined;
  @Output() dateChange = new EventEmitter<string>();

  @Input() startDate: string | null = null;
  @Input() endDate: string | null = null;
  constructor(private calendar: NgbCalendar, public formatter: NgbDateParserFormatter) {
  }
  ngOnInit(): void {
    this.setDefaultDate();
  }
  ngOnDestroy(): void {
    this.cleanEventSub.unsubscribe();
    this.accountEventSub?.unsubscribe();
  }

  ngAfterViewInit(): void {
    this.cleanEventSub = this.isCleared.subscribe(value => {
      if (value) {
        this.clearDateSelection();
      }
    });
    if (this.isAccountChanged != undefined) {
      this.accountEventSub = this.isAccountChanged.subscribe(value => {
        if (value) {
          this.setDefaultDate();
        }
      });
    }
  }

  setDefaultDate() {
    if (!!this.startDate && !!this.endDate) {
      //const from = this.formatter.parse(this.startDate)!;
      //const to = this.formatter.parse(this.endDate)!;
      //this.fromDate = new NgbDate(from.year, from.month, from.day);
      //this.toDate = new NgbDate(to.year, to.month, to.day);
      this.setDefaultWeek();
      this.fromDateString = this.formatter.format(this.fromDate);
      this.toDateString = this.formatter.format(this.toDate);
      this.emitDateChanges();
    }
  }

  clearDateSelection() {
    this.fromDateString = null;
    this.toDateString = null;
    this.fromDate = this.getToday();
    this.toDate = null;
  }

  getToday() {
    return this.calendar.getToday();
  }

  setDefaultWeek() {
    this.fromDate = this.calendar.getPrev(this.calendar.getToday(), 'd', 6);
    this.toDate = this.calendar.getToday();
    this.timeFrom = '00:00:00';
    this.timeTo = '00:00:00';
  }

  emitDateChanges() {
    this.dateChange.emit(this.formatDate());
  }

  onDateSelection(date: NgbDate) {
    if (!this.fromDate && !this.toDate) {
      this.fromDate = date;
      this.fromDateString = this.formatter.format(this.fromDate);
    }
    else if (this.fromDate && !this.toDate && date && date.after(this.fromDate)) {
      this.toDate = date;
      this.toDateString = this.formatter.format(this.toDate);
    }
    else {
      this.toDate = date;
      this.toDateString = this.formatter.format(this.toDate);
      this.fromDate = date;
      this.fromDateString = this.formatter.format(this.fromDate);
    }
  }

  isHovered(date: NgbDate) {
    return (
      this.fromDate && !this.toDate && this.hoveredDate && date.after(this.fromDate) && date.before(this.hoveredDate)
    );
  }

  isInside(date: NgbDate) {
    return this.toDate && date.after(this.fromDate) && date.before(this.toDate);
  }

  isRange(date: NgbDate) {
    return (
      date.equals(this.fromDate) ||
      (this.toDate && date.equals(this.toDate)) ||
      this.isInside(date) ||
      this.isHovered(date)
    );
  }

  applyDatetime($event: any) {
    $event.click();
    if (!!this.fromDate) {
      this.fromDateString = this.formatter.format(this.fromDate);
      this.toDateString = this.formatter.format(this.toDate);
      this.emitDateChanges();
    }
  }

  formatDate() {
    const fromDatetime = `${this.fromDateString} ${this.timeFrom}`;
    const toDatetime = `${this.toDateString} ${this.timeTo}`
    return `${fromDatetime};${toDatetime}`;
  }
}
