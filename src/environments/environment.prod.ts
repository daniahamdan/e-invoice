export const environment = {
  production: true,
  apiUrl: 'https://invoicewebapidev.cashin.sa/api/',
  defaultLang: 'en-US', //lang code - country code
  brandName: 'Cashin Invo',
};
